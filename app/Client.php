<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $connection = 'client';
    protected $table = 'brg_sscustomers';
}
