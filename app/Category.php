<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = 'mysql';

    function getUrlAttribute() {
        return route('category', [$this->type, $this->id]);
    }

    function getCountNewsAttribute() {
        return News::whereCategoryId($this->id)->count();
    }

    function subCategories() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    function projects() {
        return $this->hasMany(Project::class);
    }

    function getImageAttribute() {
        return route('thumb', [ '100', '100','category', $this->file]);
    }

    function getImageUrlAttribute() {
        return route('image', [ 'category', $this->file]);
    }
}
