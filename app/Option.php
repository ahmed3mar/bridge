<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $connection = 'mysql';
    public $timestamps = false;
}
