<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'client';
    protected $table = 'brg_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**permissions
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client() {
        return $this->hasOne(Client::class, 'userid');
    }

    public function isAdmin()
    {
        return $this->is_admin == "1";
    }

    public function isCompany()
    {
        return !$this->is_admin;
    }

    public function isBlocked()
    {
        return $this->status === 'blocked';
    }

    public function isActive()
    {
        return $this->status === 'active';
    }

    function permissions() {
        return $this->belongsToMany('App\Permission', 'permission_user');
    }

    public function getUrlAttribute() {
        return url('user/' . $this->id);
    }

    public function getThumbAttribute() {
        if(!$this->file) return url('/img/user.png');
        return route('user_image', [150,150, $this->file]);
    }

    var $permissionsData;
    var $permissionsArray;
    function getPermissions() {
        if (!$this->permissionsData) {
            $this->permissionsData = $this->permissions;
        }
        return $this->permissionsData;
    }

    function all_permissions() {
        if (!$this->permissionsArray) {
            $_permissions = $this->getPermissions();
            $this->permissionsArray = [];
            foreach ($_permissions as $p) {
                $this->permissionsArray[] = $p->name;
            }
        }
        return $this->permissionsArray;
    }

    function can( $permission, $a = [] ) {

        if (gettype($permission) === 'string') {
            $permission = [$permission];
        } else if (gettype($permission) !== 'array') {
            return false;
        }

        $permissions = $this->all_permissions();
        foreach ($permission as $p) {
            if ( in_array($p, $permissions ) ) return true;
        }
        return false;

    }
}
