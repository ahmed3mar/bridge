<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $connection = 'mysql';
    protected $table = 'gallery';

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function getImageUrlAttribute() {
        if($this->type == 'videos') {
            $id = youtube_id_from_url($this->url);
            return 'https://img.youtube.com/vi/'. $id .'/0.jpg';
        } else {
            return route('image', ['gallery', $this->url]);
        }
    }
}
