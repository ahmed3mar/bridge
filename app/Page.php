<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $connection = 'mysql';

    function getUrlAttribute() {
        $types = ['corporate', 'branding', 'partners', 'safety', 'about', 'terms'];
        if ( in_array($this->type, $types) ) {
            return route($this->type);
        }
        return route('page', $this->{_trans('slug')});
    }

    function getCoverUrlAttribute() {
        return route('image', ['cover', $this->cover]);
    }

    function getImageUrlAttribute() {
        return route('image', ['dits', $this->file]);
    }
}
