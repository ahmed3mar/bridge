<?php
/**
 * Created by PhpStorm.
 * User: ahmed3mar
 * Date: 7/18/18
 * Time: 8:08 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $connection = 'mysql';

    public function category() {
        return $this->belongsTo(Category::class);
    }

    function getUrlAttribute() {
        return route('project', $this->id);
    }

    function getImageUrlAttribute() {

        $images = json_decode($this->images);
        if(sizeof($images) == 0) return '';

        return route('image', ['project', $images[0]->image]);
    }

    function getImagesJsonAttribute() {

        $images = json_decode($this->images);
        $return = [];
        foreach ($images as $image) {
            $return[] = [
                'image_url' => route('image', ['project', $image->image]),
                'thumb_url' => route('thumb', ['100', '100', 'project', $image->image]),
            ];
        }

        return $return;
    }
}
