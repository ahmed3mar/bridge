<?php namespace App\Http\Controllers;

use App\Category;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = \App\Service::all();

        $projects = [];#\App\Project::all();
        $main_categories = Category::whereType('projects')->whereParentId(0)->get();


        $gallery = \App\Gallery::all();
        $news = \App\News::limit(9)->get();
        $current = 'home';
        $home = true;
        return view('home', compact('home','current', 'services', 'news', 'main_categories', 'projects', 'gallery'));
    }

}
