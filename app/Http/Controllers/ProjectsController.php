<?php namespace App\Http\Controllers;

use App\Category;
use App\Project;

class ProjectsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = new Category();
        $category->title = trans('projects.projects');
        $category->title_ar = trans('projects.projects');
        $category->subCategories = Category::whereType('projects')->whereParentId(0)->get();
        #$category = Category::whereType('projects')->whereParentId(0)->get();
        return view('projects.category', compact('category'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function projects( $id )
    {
        $category = Category::whereType('projects')->findOrFail($id);
        return view('projects.category', compact('category'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function project( $id )
    {
        $project = Project::findOrFail($id);
        $related_projects = Project::where('id', '!=', $id)->limit(3)->get();
        return view('projects.project', compact('project', 'related_projects'));
    }

}
