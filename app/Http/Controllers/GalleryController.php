<?php namespace App\Http\Controllers;

use App\Gallery;

class GalleryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::whereIn('type', ['images', 'videos'])->paginate(12);
        return view('gallery', compact('gallery'));
    }

}
