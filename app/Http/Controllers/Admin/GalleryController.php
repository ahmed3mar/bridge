<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class GalleryController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-gallery', 'edit-gallery', 'delete-gallery'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $pagesData = $this->data($type)->getData();
        $gallery = $pagesData->data;
        $recordsTotal = $pagesData->recordsTotal;

        $types = [
            'images'    => 'Images',
            'videos'      => 'Videos',
        ];
        $text = $types[$type];

        \View::share('current', $type);

        return view('admin.gallery', compact('gallery', 'recordsTotal', 'type', 'text'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data($type)
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Gallery::query()->whereType($type);
        else $query = Gallery::query()->whereType($type)->orderBy('id','desc');#->limit(10);

        return Datatables::of($query)
            ->addColumn('image_url', function (Gallery $project) {
                if($project->type == 'videos') return 'VIDEO';
                return img(route('image', ['gallery', $project->url]), '', " style='width: 75px' ");
            })
            ->addColumn('options', function (Gallery $gallery) use ($type) {
                $back = "";

                if (can(['edit-gallery', 'delete-gallery'])) {
                    if ( can('edit-gallery') ) $back .= data_edit_btn($gallery);
                    if ( can('delete-gallery') ) $back .= data_delete_btn($gallery);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image_url'])
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($type, $id)
    {
        try {
            $gallery = Gallery::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $gallery
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        if (!auth()->user()->can('add-gallery')) return no_permission();

        $this->validate($request, [
            'title'         => 'required|max:255',

            'category_id'       => 'required',
        ]);

        $gallery = new Gallery();

        $gallery->title = $request->title;
        $gallery->type = $request->type;
        $gallery->category_id = $request->category_id;
        $gallery->url = $request->url;
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('gallery');
                if( $path ) {
                    $gallery->url = str_replace('gallery/', '', $path);
                }
            }
        } else if( $gallery->type == 'images') {
            return response()->json([
                'errors' => [
                    'file' => 'Please choose the image',
                ]
            ], 422);
        }

        $gallery->save();

        return $gallery;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type, $id)
    {
        if (!auth()->user()->can('edit-gallery')) return no_permission();

        $this->validate($request, [
            'title'             => 'required|max:255',
            'category_id'       => 'required',
        ]);

        $gallery = Gallery::find($id);
        $gallery->title = $request->title;
        $gallery->category_id = $request->category_id;
        $gallery->type = $request->type;
        $gallery->url = $gallery->type == 'images' ? $gallery->url : $request->url;
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('gallery');
                if( $path ) {
                    $gallery->url = str_replace('gallery/', '', $path);
                }
            }
        }
        $gallery->save();

        return $gallery;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, $id)
    {

        if (!auth()->user()->can('delete-gallery')) return no_permission();

        try{
            $gallery = Gallery::findOrFail($id);
            $gallery->delete();

            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => "Gallery not found"
            ], 500);
        }
    }
}
