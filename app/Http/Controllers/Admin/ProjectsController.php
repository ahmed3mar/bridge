<?php

namespace App\Http\Controllers\Admin;

use App\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProjectsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-project', 'edit-project', 'delete-project'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'projects');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectsData = $this->data()->getData();
        $projects = $projectsData->data;
        $recordsTotal = $projectsData->recordsTotal;
        return view('admin.projects', compact('projects', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Project::query();
        else $query = Project::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('image_url', function (Project $project) {
                $images = json_decode($project->images);
                if(sizeof($images) == 0) return '';
                return img(route('image', ['project', $images[0]->image]), '', " style='width: 75px' ");
            })
            ->addColumn('options', function (Project $project) {

                $back = "";

                if (can(['edit-project', 'delete-project'])) {
                    if ( can('edit-project') ) $back .= data_edit_btn($project);
                    if ( can('delete-project') ) $back .= data_delete_btn($project);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image_url', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-project')) return no_permission();

        $this->validate($request, [
            'title'         => 'required|max:255',
            'category_id'       => 'required',
            'content'   => 'required',
        ]);

        $project = new Project();
        $project->title = $request->title;
        $project->category_id = $request->category_id;
        $project->content = $request->get('content');
        $project->content_ar = $request->get('content_ar');
        $project->title_ar = $request->get('title_ar');


        $images = $request->has('images') ? $request->get('images') : [];

        if( $request->hasFile('projectImages'))
            foreach ($request->file('projectImages') as $file) {
                if ($file->isValid()) {
                    $path = $file->storePublicly('projects');
                    if( $path ) {
                        $images[] = str_replace('projects/', '', $path);
                    }
                }
            }

        $values = [];
        foreach ($images as $image) {
            $values[] = [
                'image' => $image,
            ];
        }

        $project->images = json_encode($values);

//        if ($request->hasFile('file')) {
//            if ($request->file('file')->isValid()) {
//                $path = $request->file->storePublicly('projects');
//                if( $path ) {
//                    $project->image = str_replace('projects/', '', $path);
//                }
//            }
//        } else {
//            return response()->json([
//                'errors' => [
//                    'file' => 'Please choose the image',
//                ]
//            ], 422);
//        }

        $project->save();

        return $project;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $project = Project::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $project
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-project')) return no_permission();

        $this->validate($request, [
            'title'             => 'required|max:255',
            'category_id'       => 'required',
            'content'       => 'required',
        ]);

        $project = Project::find($id);
        $project->title = $request->title;
        $project->category_id = $request->category_id;
        $project->content = $request->get('content');
        $project->content_ar = $request->get('content_ar');
        $project->title_ar = $request->get('title_ar');
        $images = $request->has('images') ? $request->get('images') : [];

        if( $request->hasFile('projectImages'))
            foreach ($request->file('projectImages') as $file) {
                if ($file->isValid()) {
                    $path = $file->storePublicly('projects');
                    if( $path ) {
                        $images[] = str_replace('projects/', '', $path);
                    }
                }
            }

        $values = [];
        foreach ($images as $image) {
            $values[] = [
                'image' => $image,
            ];
        }

        $project->images = json_encode($values);

        $project->save();

        return $project;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-project')) return no_permission();

        try{
            $project = Project::findOrFail($id);
            $project->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
