<?php namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('current', 'home');
        $this->middleware('auth.admin');
    }

    public function index()
    {

        /*$ads = Ad::all();
        foreach ($ads as $ad) {
            $model = $ad->car_model;
            $production_years = $model->production_years;
            $ad->instalment = false;
            foreach ($production_years as $year) {
                if( $year->year == $ad->year ) {
                    $ad->instalment = $year->instalment;
                }
            }
            $ad->save();
        }*/


        $count = [
//            'ads' => [
//                'total' => \App\Ad::count(),
////                'today' => \App\Ad::where('ad_date', '>=', Carbon::today())->count(),
//                'pending' => \App\Ad::where('status', 'pending')->count(),
//            ],
//            'users' => [
//                'total' => \App\User::count(),
//                'today' => \App\User::where('created_at', '>=', Carbon::today())->count(),
//            ],
//            'reports'   => [
//                'total'     => \App\Report::count(),
//                'today'     => \App\Report::where('created_at', '>=', Carbon::today())->count(),
////                'pending'   => \App\Report::where('status', 'pending')->count(),
//            ],
//            'orders'   => [
//                'total'     => \App\Order::count(),
//                'today'     => \App\Order::where('created_at', '>=', Carbon::today())->count(),
//                'pending'   => \App\Order::where('status', 'pending')->count(),
//                'completed' => \App\Order::where('status', 'completed')->count(),
//            ],
//            'views'   => [
//                'total'     => \DB::table('ads')->selectRaw('SUM(views) as sum')->first()->sum,
//            ],
//            'phone'   => [
//                'total'     => \App\PhoneClick::count(),
//                'today'     => \App\PhoneClick::where('created_at', '>=', Carbon::today())->count(),
//            ],
        ];

//        $ordersChart = $reportsChart = $phoneClicks = [];

//        for($i = 0 ; $i <= 11; $i++) {
//            $current_date = Carbon::now()->subMonth($i);
//            $other_date = Carbon::now()->subMonth($i-1);
//            $ordersChart[ $current_date->format('M') . ' ' . $current_date->format('Y') ] =
//                \App\Order::whereBetween('created_at', [$current_date->toDateString(), $other_date->toDateString()])->count();
//
//            $phoneClicks[ $current_date->format('M') . ' ' . $current_date->format('Y') ] =
//                \App\PhoneClick::whereBetween('created_at', [$current_date->toDateString(), $other_date->toDateString()])->count();
//
////            $ordersChart[] = [
////                'name'  => $current_date->format('M') . ' ' . $current_date->format('Y'),
////                'value' => \App\Order::whereBetween('created_at', [$current_date->toDateString(), $other_date->toDateString()])->count(),
////            ];
////            $reportsChart[] = [
////                'name'  => $current_date->format('M') . ' ' . $current_date->format('Y'),
////                'value' => \App\Report::whereBetween('created_at', [$current_date->toDateString(), $other_date->toDateString()])->count(),
////            ];
////            $phoneClicks[] = [
////                'name'  => $current_date->format('M') . ' ' . $current_date->format('Y'),
////                'value' => \App\PhoneClick::whereBetween('created_at', [$current_date->toDateString(), $other_date->toDateString()])->count(),
////            ];
//        }

        return view('admin.home', compact('count', 'phoneClicks', 'reportsChart', 'ordersChart'));
    }

}