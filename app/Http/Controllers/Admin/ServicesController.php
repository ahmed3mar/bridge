<?php

namespace App\Http\Controllers\Admin;

use App\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ServicesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-service', 'edit-service', 'delete-service'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'services');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $icons_file = file_get_contents(public_path('../../backend/css/font-awesome.min.css'));
        preg_match_all('#\.fa\-(.*?)\:before#i', $icons_file, $_icons);
        unset($_icons[1][0]);
        $icons = $_icons[1];

        $servicesData = $this->data()->getData();
        $services = $servicesData->data;
        $recordsTotal = $servicesData->recordsTotal;
        return view('admin.services', compact('services', 'recordsTotal', 'icons'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Service::query();
        else $query = Service::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('icon', function (Service $service) {
                return '<i class="fa fa-'.$service->icon.'"></i>';
            })
            ->addColumn('options', function (Service $service) {

                $back = "";

                if (can(['edit-service', 'delete-service'])) {
                    if ( can('edit-service') ) $back .= data_edit_btn($service);
                    if ( can('delete-service') ) $back .= data_delete_btn($service);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'icon', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-service')) return no_permission();

        $this->validate($request, [
            'title'         => 'required|max:255',
            'description'   => 'required',
            'icon'   => 'required',
        ]);

        $service = new Service();
        $service->title = $request->title;
        $service->description = $request->description;
        $service->title_ar = $request->title_ar;
        $service->description_ar = $request->description_ar;
        $service->icon = $request->icon;

        $service->save();

        return $service;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $service = Service::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $service
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-service')) return no_permission();

        $this->validate($request, [
            'title'             => 'required|max:255',
            'description'       => 'required',
            'icon'       => 'required',
        ]);

        $service = Service::find($id);
        $service->title = $request->title;
        $service->description = $request->description;
        $service->title_ar = $request->title_ar;
        $service->description_ar = $request->description_ar;
        $service->icon = $request->icon;
        $service->save();

        return $service;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-service')) return no_permission();

        try{
            $service = Service::findOrFail($id);
            $service->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
