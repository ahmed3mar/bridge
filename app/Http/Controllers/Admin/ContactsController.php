<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ContactsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['view-contacts', 'edit-contact'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'contacts');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactsData = $this->data()->getData();
        $contacts = $contactsData->data;
        $recordsTotal = $contactsData->recordsTotal;

        $ad_id = (int) \request()->get('ad');
        $ad = null;
        if ($ad_id) $ad = Ad::find($ad_id);

        return view('admin.contacts', compact('contacts', 'recordsTotal', 'ad_id', 'ad'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isContact = \request()->get('contact');

        if ($isContact) $query = Contact::query();
        else $query = Contact::query()->orderBy('id','desc');#->limit(10);

        $date_from = \request()->get('date_from');
        if ($date_from) $query = $query->whereDate('created_at', '>=', Carbon::createFromFormat('F d, Y', $date_from)->toDateString());

        $date_to = \request()->get('date_to');
        if ($date_to) $query = $query->whereDate('created_at', '<=', Carbon::createFromFormat('F d, Y', $date_to)->toDateString());


        return Datatables::of($query)

            ->editColumn('name', function(Contact $contact) {
                return $contact->name;
            })

            ->editColumn('comment', function(Contact $contact) {
                return '<a href="#" class="edit-comment" data-id="'.$contact->id.'">'. ($contact->comment ? str_limit($contact->comment, 20) : 'no comment' ). '</a>';
            })

            ->editColumn('message', function(Contact $contact) {
                return ($contact->message ? str_limit($contact->message, 20) : '' );
            })
            ->addColumn('options', function (Contact $contact) {

                $back = "";

                $back .= data_btn($contact, 'View Contact', 'view-contact', '<i class="fa fa-1-8x fa-eye"></i>');

                if (can('delete-contact'))
                    $back .= data_btn($contact, 'Delete Contact', 'delete-contact', '<i class="fa fa-1-8x fa-trash text-danger"></i>');

                return $back;
            })
            ->rawColumns(['options', 'name', 'comment', 'email'])
            ->make(true);
    }

    public function export() {

        $query = Contact::query();
        $orders = $query->get();

        $export = \Excel::create('Drive Yalla - Contact us', function($excel) use ($orders) {

            // Set the title
            $excel->setTitle('Drive Yalla Contact us');

            // Chain the setters
            $excel->setCreator('Drive Yalla')
                ->setCompany('360 Codes');

            // Call them separately
//            $excel->setDescription('A demonstration to change the file properties');

            $excel->sheet('Contact us', function($sheet) use ($orders)
            {

                $orders_ar = [];
                foreach ($orders as $order) {
                    $orders_ar[] = [
                        $order->id, $order->name, $order->email, $order->phone, $order->message,
                        $order->comment,
                    ];
                }

                $sheet->fromArray($orders_ar, null, 'A1', true);
                $sheet->cells('A1', function($cell) { $cell->setValue('ID'); });
                $sheet->cells('B1', function($cell) { $cell->setValue('Name'); });
                $sheet->cells('C1', function($cell) { $cell->setValue('E-Mail'); });
                $sheet->cells('D1', function($cell) { $cell->setValue('Phone'); });
                $sheet->cells('E1', function($cell) { $cell->setValue('Message'); });
                $sheet->cells('F1', function($cell) { $cell->setValue('Comment'); });
                $sheet->cells('A1:F1', function($cells) {
                    $cells->setBackground('#000000');
                    $cells->setFontColor('#ffffff');
                    // Set font weight to bold
                    #$cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });


            })->export(\request()->get('type'));

        });

        // work on the export
        return $export;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contact = Contact::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $contact
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!can('edit-contact')) return no_permission();

        $type = $request->get('type');
        $contact = Contact::find($id);

        if($type=='comment') {
            $contact->comment = $request->comment;
            $contact->save();
        }

//        if ($type == 'cancel') {
//            $contact->ad->increment('cancelled_contacts_count');
//            $contact->status = 'cancelled';
//            $contact->save();
//        } else if ( $type == 'accept') {
////            $ad = $contact->ad;
////            $ad->status = 'sold';
////            $ad->save();
//            $contact->status = 'completed';
//            $contact->save();
//        }

        return $contact;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!can('delete-contact')) return no_permission();

        try{
            $contact = Contact::findOrFail($id);
            $contact->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

}
