<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Waavi\Translation\Models\Translation;
use Yajra\DataTables\Facades\DataTables;

class TranslationsController extends Controller
{

    function __construct()
    {
//        $this->middleware(function ($request, $next) {
//            if (!can(['add-translation', 'edit-translation', 'delete-translation'])) {
//                return redirect(route('admin.main'));
//            }
//            return $next($request);
//        });

        \View::share('current', 'translations');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translationsData = $this->data()->getData();
        $translations = $translationsData->data;
        $recordsTotal = $translationsData->recordsTotal;
        return view('admin.translations', compact('translations', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {

        $isOrder = \request()->get('order');

        if ($isOrder) $query = Translation::query();
        else $query = Translation::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('options', function (Translation $translation) {

                $back = "";

                if (can(['edit-translation', 'delete-translation'])) {
                    if ( can('edit-translation') ) $back .= data_edit_btn($translation);
                    if ( can('delete-translation') ) $back .= data_delete_btn($translation);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!can('add-translation')) return no_permission();

        $this->validate($request, [
            'locale'    => 'required|max:2',
            'group'     => 'required|max:50',
            'item'      => 'required|max:255',
            'text'      => 'required',
        ]);

        $translation = new Translation();
        $translation->locale = $request->get('locale');
        $translation->group = $request->group;
        $translation->item = $request->item;
        $translation->text = $request->text;
        $translation->save();

        return $translation;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = Translation::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $type
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!can('edit-translation')) return no_permission();

        $this->validate($request, [
            'locale'    => 'required|max:2',
            'group'     => 'required|max:50',
            'item'      => 'required|max:255',
            'text'      => 'required',
        ]);

        $translation = Translation::find($id);
        $translation->locale = $request->get('locale');
        $translation->group = $request->group;
        $translation->item = $request->item;
        $translation->text = $request->text;
        $translation->save();

        return $translation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!can('delete-translation')) return no_permission();

        try{
            $translation = Translation::findOrFail($id);
            $translation->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
