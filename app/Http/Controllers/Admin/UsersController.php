<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-user', 'edit-user', 'delete-user'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });
        \View::share('current', 'users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersData = $this->data()->getData();
        $users = $usersData->data;
        $recordsTotal = $usersData->recordsTotal;

        $permissionsGroups = list_permissions();

        return view('admin.users', compact('users', 'recordsTotal', 'permissionsGroups'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = User::query();
        else $query = User::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('is_admin', function (User $user) {
                return $user->isAdmin() ? '<span class="label label-danger">ADMIN</span>' : '<span class="label label-info">CLIENT</span>';
            })
//            ->editColumn('status', function (User $user) {
//                if ($user->isBlocked()) {
//                    return '<span class="label label-danger">BLOCKED</span>';
//                } else if ($user->isActive()) {
//                    return '<span class="label label-success">ACTIVE</span>';
//                }
//                return '<span class="label label-warning">REQUIRE ACTIVATION</span>';
//            })
            ->addColumn('options', function (User $user) {

                $back = "";

                if (can(['edit-user', 'delete-user'])) {
                    if ( can('edit-user') ) $back .= data_edit_btn($user);
                    if ( can('delete-user') ) $back .= data_delete_btn($user);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'is_admin', 'status'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!can('add-user')) return no_permission();

        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6',
            #'phone'     => 'required|numeric',
            #'status'    => 'required',
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        #$user->phone = $request->phone;
        #$user->status = $request->status;
        $user->password = bcrypt($request->password);

        $isAdmin = in_array('is_admin', array_keys($request->all()));

//        if ($request->hasFile('image')) {
//            if ($request->file('image')->isValid()) {
//                $path = $request->image->storePublicly('user');
//                if( $path ) {
//                    $user->image_file = str_replace('user/', '', $path);
//                }
//            }
//        }

        # Permissions
        if ($isAdmin) {
            $permission_ids = [];
            foreach ($request->permissions as $permission => $ids) {
                $permission_id = array_keys($ids)[0];
                $permission_ids[] = $permission_id;
            }
            if (sizeof($permission_ids) > 0)
                $user->permissions()->attach($permission_ids);
        }

        $user->is_admin = $isAdmin;

        $user->save();

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->image_url = $user->thumb;
            $user->all_permissions = $user->all_permissions();
            return response()->json([
                "success"   => true,
                "data"      => $user
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!can('edit-user')) return no_permission();

        \Validator::extend('new_password', function ($attribute, $value, $parameters, $validator) {
            if( !$value ) return true;
            if( strlen($value) < 6 ) return false;
            return true;
        });

        $this->validate($request, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users,email,' . $id,
            'password'  => 'new_password',
            #'phone'     => 'required|numeric',
            #'status'    => 'required',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
//        $user->phone = $request->phone;
        #$user->status = $request->status;
//
//        if ($user->status === 'active') {
//            $user->activation_code = null;
//        } else if ($user->status === 'blocked') {
//            foreach ($user->ads as $ad) {
//                $ad->status = 'spam';
//                $ad->save();
//            }
//        }

//        if ($request->hasFile('image')) {
//            if ($request->file('image')->isValid()) {
//                $path = $request->image->storePublicly('user');
//                if( $path ) {
//                    $user->image_file = str_replace('user/', '', $path);
//                }
//            }
//        }

        $isAdmin = in_array('is_admin', array_keys($request->all()));

        # Permissions
        $user->permissions()->detach();
        if ($isAdmin) {
            $permission_ids = [];
            foreach ($request->permissions as $permission => $ids) {
                $permission_id = array_keys($ids)[0];
                $permission_ids[] = $permission_id;
            }
            if (sizeof($permission_ids) > 0)
                $user->permissions()->attach($permission_ids);
        }

        if( $request->password ) {
            $user->password = bcrypt($request->password);
        }
        $user->is_admin = $isAdmin;
        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!can('delete-user')) return no_permission();

        try{
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
