<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class PagesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-page', 'edit-page', 'delete-page'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'pages');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagesData = $this->data()->getData();
        $pages = $pagesData->data;
        $recordsTotal = $pagesData->recordsTotal;
        return view('admin.pages', compact('pages', 'recordsTotal'));
    }

    public function edit($slugOrId) {
        if (is_numeric($slugOrId)) {
            $page = Page::findOrFail($slugOrId);
        } else {
            $page = Page::whereSlug($slugOrId)->firstOrFail();
        }

        $isHidden = in_array($page->slug, ['corporate', 'branding', 'partners', 'safety', 'about', 'mena', 'terms']);
        \View::share('sub', $slugOrId);

        return view('admin.pages.edit', compact('page', 'isHidden'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {

        $isOrder = \request()->get('order');

        if ($isOrder) $query = Page::query();
        else $query = Page::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('options', function (Page $page) {

                $back = "";

                if (can(['edit-page', 'delete-page'])) {
                    if ( can('edit-page') ) $back .= data_edit_btn($page);
                    if ( can('delete-page') ) $back .= data_delete_btn($page);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!can('add-page')) return no_permission();

        $this->validate($request, [
            'title'     => 'required|max:255',
            'slug'      => 'required|max:255|unique:pages',
            'content'   => 'required',

            'seo_title'             => 'required|max:60',
            'seo_title_ar'          => 'required|max:60',
            'seo_description'       => 'required|max:156',
            'seo_description_ar'    => 'required|max:156',
        ]);

        $page = new Page;
        $page->title        = $request->title;
        $page->title_ar     = $request->title_ar;
        $page->slug         = $request->slug;
        $page->content      = $request->get('content');
        $page->content_ar   = $request->content_ar;

        $page->seo_title           = $request->seo_title;
        $page->seo_title_ar        = $request->seo_title_ar;
        $page->seo_description     = $request->seo_description;
        $page->seo_description_ar  = $request->seo_description_ar;
        $page->save();

        return $page;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = Page::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $type
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!can('edit-page')) return no_permission();

        $this->validate($request, [
            'title'     => 'required|max:255',
            'slug'      => 'required|max:255|unique:pages,slug,' . $id,
            'content'   => 'required',

            'seo_title'             => 'required|max:60',
            'seo_title_ar'          => 'required|max:60',
            'seo_description'       => 'required|max:156',
            'seo_description_ar'    => 'required|max:156',
        ]);

        $page = Page::find($id);
        $page->title        = $request->title;
        $page->title_ar     = $request->title_ar;
//        $page->sub_title        = $request->sub_title;
//        $page->sub_title_ar     = $request->sub_title_ar;
        $page->description        = $request->description;
        $page->description_ar     = $request->description_ar;
        $page->slug         = $request->slug;
        $page->content      = $request->get('content');
        $page->content_ar   = $request->content_ar;

        $page->seo_title           = $request->seo_title;
        $page->seo_title_ar        = $request->seo_title_ar;
        $page->seo_description     = $request->seo_description;
        $page->seo_description_ar  = $request->seo_description_ar;

        $page->save();

        return $page;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!can('delete-page')) return no_permission();

        try{
            $page = Page::findOrFail($id);
            $page->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
