<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class OptionsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can('edit-options')) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'options');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        parseOptions();
        option('site_title');
        return view('admin.options', compact('options'));
    }

    public function update(Request $request) {

        $option_type = $request->get('type');

        if ($option_type == 'global') {
            $this->validate($request, [
                'site_title' => 'required|max:255',
                'home_title' => 'required|max:255',
                'site_description' => 'required',
                'site_keywords' => 'required',
            ]);

            $data = [
                'site_title', 'site_title_ar',
                'home_title', 'home_title_ar',
                'site_description', 'site_description_ar',
                'site_keywords', 'site_keywords_ar',
            ];

            foreach ($data as $item) {
                Option::where(['key' => $item])->update(['value' => $request->get($item)]);
            }
        }

        if ($option_type == 'slider') {
//            $this->validate($request, [
//                'slider_top'     => 'required',
//                'slider_bottom'  => 'required',
//            ]);
//
//            $data = [
//                'slider_top', 'slider_bottom',
//            ];

            $images = $request->has('images') ? $request->get('images') : [];

            if( $request->hasFile('sliderImages'))
                foreach ($request->file('sliderImages') as $file) {
                    if ($file->isValid()) {
                        $path = $file->storePublicly('slider');
                        if( $path ) {
                            $images[] = str_replace('slider/', '', $path);
                        }
                    }
                }

            $values = [];
            $x = 0;
            foreach ($images as $image) {
                $links[] = $request->get('links')[$x];

                $values[] = [
                    'image' => $image,
                    'link'  => $request->get('links')[$x]
                ];

                $x++;
            }


//            foreach ($data as $item) {
//                Option::where(['key' => $item])->update(['value' => $request->get($item)]);
//            }

            Option::where(['key' => 'slider_links'])->update(['value' => json_encode($values)]);
        }
        if ($option_type == 'about') {
//            $this->validate($request, [
//                'slider_top'     => 'required',
//                'slider_bottom'  => 'required',
//            ]);
//
//            $data = [
//                'slider_top', 'slider_bottom',
//            ];


            save_option('about_title', $request->get('about_title'));
            save_option('about_title_ar', $request->get('about_title_ar'));
            save_option('about_content', $request->get('about_content'));
            save_option('about_content_ar', $request->get('about_content_ar'));
            save_option('about_title', $request->get('about_title'));
            if($request->hasFile('about_file')) {
                if ($request->about_file->isValid()) {
                    $path = $request->about_file->storePublicly('images');
                    if( $path )
                        save_option('about_image', str_replace('images/', '', $path));
                }
            }
        }

        if ($option_type == 'map') {
            save_option('map_lat', $request->get('map_lat'));
            save_option('map_lng', $request->get('map_lng'));
        }

        if ($option_type == 'social') {
            save_option('social_facebook', $request->get('social_facebook'));
            save_option('social_google_plus', $request->get('social_google_plus'));
            save_option('social_linkedin', $request->get('social_linkedin'));
            save_option('social_twitter', $request->get('social_twitter'));
            save_option('social_youtube', $request->get('social_youtube'));
            save_option('social_instagram', $request->get('social_instagram'));
        }

        if ($option_type == 'close') {
            Option::where(['key' => 'site_close'])->update(['value' => $request->get('site_close')]);
            Option::where(['key' => 'site_close_message'])->update(['value' => $request->get('site_close_message')]);
            Option::where(['key' => 'site_close_message_ar'])->update(['value' => $request->get('site_close_message_ar')]);
        }

    }


}
