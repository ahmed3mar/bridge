<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoriesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            #if (!can(['add-category', 'edit-category', 'delete-category'])) {
            #    return redirect(route('admin.main'));
            #}
            return $next($request);
        });


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $types = [
            'projects'    => 'Projects',
            'images'      => 'Images',
            'videos'      => 'Videos',
            'news'      => 'News',
        ];
        $text = $types[$type];

        \View::share('current', $type);
        return view('admin.categories', compact('type', 'text'));
    }

    function sort() {
        $categories = \request()->get('categories');
        $this->saveOrder($categories, 0);
    }

    function saveOrder($categories, $parent) {
        $sort = 0;
        foreach ($categories as $category) {

            Category::whereId($category['id'])
                ->update([
                    'parent_id' => $parent,
                    'order'  => $sort,
                ]);

            if(isset($category['children'])) {
                $this->saveOrder($category['children'], $category['id']);
            }
            $sort++;
        }
    }

    function organise_categories($categories) {
        $_categories = [];
        foreach ($categories as $category) {
            $_categories[] = [
                'id'        => $category->id,
                'title'     => $category->title,
                'image'     => $category->image,
                'subs'      => $this->organise_categories($category->subCategories),
            ];
        }
        return $_categories;
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data($type)
    {
        return $this->organise_categories(Category::whereType($type)->whereParentId(0)->orderBy('order')->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($type, $id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->image_url = $category->image_url;
            return response()->json([
                "success"   => true,
                "data"      => $category
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        if (!auth()->user()->can('add-'.$type.'-category')) return no_permission();

        $this->validate($request, [
            'title'         => 'required|max:255',
        ]);

        $category = new Category();

        $category->title = $request->title;
        $category->title_ar = $request->title_ar;
        $category->type = $request->type;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('categories');
                if( $path ) {
                    $category->file = str_replace('categories/', '', $path);
                }
            }
        }

        $category->save();

        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $type, $id)
    {
        if (!auth()->user()->can('edit-'.$type.'-category')) return no_permission();

        $this->validate($request, [
            'title'             => 'required|max:255',
        ]);

        $category = Category::find($id);
        $category->title = $request->title;
        $category->title_ar = $request->title_ar;
        $category->type = $request->type;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('categories');
                if( $path ) {
                    $category->file = str_replace('categories/', '', $path);
                }
            }
        }
        $category->save();

        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, $id)
    {

        if (!auth()->user()->can('delete-'.$type.'-category')) return no_permission();

        try{
            $category = Category::findOrFail($id);
            $category->delete();

            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => "Category not found"
            ], 500);
        }
    }
}
