<?php

namespace App\Http\Controllers\Admin;

use App\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class NewsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-news', 'edit-news', 'delete-news'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'news');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsData = $this->data()->getData();
        $news = $newsData->data;
        $recordsTotal = $newsData->recordsTotal;
        return view('admin.news', compact('news', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = News::query();
        else $query = News::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('image_url', function (News $news) {
                $images = json_decode($news->images);
                if(sizeof($images) == 0) return '';
                return img(route('image', ['news', $images[0]->image]), '', " style='width: 75px' ");
            })
            ->addColumn('options', function (News $news) {

                $back = "";

                if (can(['edit-news', 'delete-news'])) {
                    if ( can('edit-news') ) $back .= data_edit_btn($news);
                    if ( can('delete-news') ) $back .= data_delete_btn($news);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image_url', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-news')) return no_permission();

        $this->validate($request, [
            'title'         => 'required|max:255',
            'category_id'       => 'required',
            'content'   => 'required',
        ]);

        $news = new News();
        $news->title = $request->title;
        $news->category_id = $request->category_id;
        $news->content = $request->get('content');
        $news->content_ar = $request->get('content_ar');
        $news->title_ar = $request->get('title_ar');


        $images = $request->has('images') ? $request->get('images') : [];

        if( $request->hasFile('newsImages'))
            foreach ($request->file('newsImages') as $file) {
                if ($file->isValid()) {
                    $path = $file->storePublicly('news');
                    if( $path ) {
                        $images[] = str_replace('news/', '', $path);
                    }
                }
            }

        $values = [];
        foreach ($images as $image) {
            $values[] = [
                'image' => $image,
            ];
        }

        $news->images = json_encode($values);

//        if ($request->hasFile('file')) {
//            if ($request->file('file')->isValid()) {
//                $path = $request->file->storePublicly('news');
//                if( $path ) {
//                    $news->image = str_replace('news/', '', $path);
//                }
//            }
//        } else {
//            return response()->json([
//                'errors' => [
//                    'file' => 'Please choose the image',
//                ]
//            ], 422);
//        }

        $news->save();

        return $news;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $news = News::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $news
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-news')) return no_permission();

        $this->validate($request, [
            'title'             => 'required|max:255',
            'category_id'       => 'required',
            'content'       => 'required',
        ]);

        $news = News::find($id);
        $news->title = $request->title;
        $news->category_id = $request->category_id;
        $news->content = $request->get('content');
        $news->content_ar = $request->get('content_ar');
        $news->title_ar = $request->get('title_ar');
        $images = $request->has('images') ? $request->get('images') : [];

        if( $request->hasFile('newsImages'))
            foreach ($request->file('newsImages') as $file) {
                if ($file->isValid()) {
                    $path = $file->storePublicly('news');
                    if( $path ) {
                        $images[] = str_replace('news/', '', $path);
                    }
                }
            }

        $values = [];
        foreach ($images as $image) {
            $values[] = [
                'image' => $image,
            ];
        }

        $news->images = json_encode($values);

        $news->save();

        return $news;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-news')) return no_permission();

        try{
            $news = News::findOrFail($id);
            $news->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}
