<?php namespace App\Http\Controllers;

use App\Category;
use App\Project;

class CategoriesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $id )
    {
        $category = Category::findOrFail($id);
        $related_projects = Project::where('id', '!=', $id)->limit(3)->get();
        return view('project', compact('project', 'related_projects'));
    }

}
