<?php namespace App\Http\Controllers;

use App\Category;
use App\News;

class NewsController extends Controller
{

    public function index() {
        $news = News::paginate(10);
        return view('news.index', compact('news'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function article( $id )
    {
        $news = News::findOrFail($id);
        $categories = Category::whereType('news')->get();
        $related_news = News::where('id', '!=', $id)->limit(3)->get();
        return view('news.show', compact('news', 'categories', 'related_news'));
    }

}
