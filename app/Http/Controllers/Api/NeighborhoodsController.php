<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Neighborhood;

class NeighborhoodsController extends Controller
{
    public function show($id)
    {
        $neighborhood = Neighborhood::select('id', 'name')->find($id);
        return [
            'status' => 'ok',
            'result' => $neighborhood
        ];
    }

    public function companies($id)
    {
        $neighborhood = Neighborhood::select('id', 'name')->with('companies.user')->find($id);
        if($neighborhood) {

            $companies = [];
            $ids = [];
            foreach ($neighborhood->companies as $company) {
                if(in_array($company->id, $ids)) continue;
                $ids[] = $company->id;
                $companies[] = [
                    'id'    => $company->id,
                    'name'  => $company->name,
                    'user'  => [
                        'id'        => $company->user->id,
                        'name'      => $company->user->name,
                        'email'     => $company->user->email,
                        'phone'     => $company->user->phone,
                    ]
                ];
            }

            return [
                'status' => 'ok',
                'result' => $companies,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function mosques($id)
    {
        $neighborhood = Neighborhood::select('id', 'name')->with(['mosques' => function ($query) {
            $query->select('id', 'name', 'address', 'neighborhood_id');
        }])->find($id);
        if($neighborhood) {

            return [
                'status' => 'ok',
                'result' => $neighborhood->mosques,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function products($id)
    {
        $neighborhood = Neighborhood::select('id', 'name')->with('companies.products')->find($id);

        if($neighborhood) {

            $products = [];
            $ids = [];
            foreach ($neighborhood->companies as $company) {
                foreach ($company->products as $product) {
                    if(in_array($product->id, $ids)) continue;
                    $ids[] = $product->id;
                    $products[] = [
                        'id'            => $product->id,
                        'name'          => $product->name,
                        'description'   => $product->description,
                        'price'         => $product->price,
                        'image'         => url('image/product/' . $product->image),
                    ];
                }
            }

            return [
                'status' => 'ok',
                'result' => $products,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

}
