<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mosque;

class MosquesController extends Controller
{
    public function index()
    {
        $data = Mosque::with('city', 'neighborhood')->get();

        $mosques = [];
        foreach ($data as $item) {
            $mosques[] = [
                'id'            => $item->id,
                'name'          => $item->name,
                'address'       => $item->address,
                'latitude'       => $item->latitude,
                'longitude'       => $item->longitude,
                'image'         => url('image/mosque/' . $item->image),
                'city'          => [
                    'id'        => $item->city->id,
                    'name'      => $item->city->name,
                ],
                'neighborhood'  => [
                    'id'        => $item->neighborhood->id,
                    'name'      => $item->neighborhood->name,
                ],
            ];
        }

        return [
            'status' => 'ok',
            'result' => $mosques
        ];
    }

    public function show($id)
    {
        $mosque = Mosque::select('id', 'image', 'name', 'address', 'latitude', 'longitude')->find($id);
        if($mosque) {

            $mosque->image = url('image/mosque/' . $mosque->image);

            return [
                'status' => 'ok',
                'result' => $mosque,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'mosque_not_found',
        ];
    }

}
