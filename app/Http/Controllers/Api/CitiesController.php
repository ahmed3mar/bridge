<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    public function index()
    {
        $cities = City::select('id', 'name', 'city_order as order')->orderBy('order')->get();
        return [
            'status' => 'ok',
            'result' => $cities
        ];
    }

    public function show($id)
    {
        $city = City::select('id', 'name')->find($id);
        if($city) {

            return [
                'status' => 'ok',
                'result' => $city,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function neighborhoods($id)
    {
        $city = City::select('id', 'name')->with([
            'neighborhoods' => function($query) {
                $query->select('id', 'name', 'neighborhood_order as order', 'city_id');
                $query->orderBy('order');
            }
        ])->find($id);
        if($city) {

            return [
                'status' => 'ok',
                'result' => $city->neighborhoods,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function companies($id)
    {
        $city = City::select('id', 'name')->with('companies.user')->find($id);
        if($city) {

            $companies = [];
            $ids = [];
            foreach ($city->companies as $company) {
                if(in_array($company->id, $ids)) continue;
                $ids[] = $company->id;
                $companies[] = [
                    'id'    => $company->id,
                    'name'  => $company->name,
                    'user'  => [
                        'id'        => $company->user->id,
                        'name'      => $company->user->name,
                        'email'     => $company->user->email,
                        'phone'     => $company->user->phone,
                    ]
                ];
            }

            return [
                'status' => 'ok',
                'result' => $companies,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function mosques($id)
    {
        $city = City::select('id', 'name')->with(['mosques' => function ($query) {
            $query->select('id', 'name', 'address', 'city_id');
        }])->find($id);
        if($city) {

            return [
                'status' => 'ok',
                'result' => $city->mosques,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

    public function products($id)
    {
        $city = City::select('id', 'name')->with('companies.products')->find($id);

        if($city) {

            $products = [];
            $ids = [];
            foreach ($city->companies as $company) {
                foreach ($company->products as $product) {
                    if(in_array($product->id, $ids)) continue;
                    $ids[] = $product->id;
                    $products[] = [
                        'id'            => $product->id,
                        'name'          => $product->name,
                        'description'   => $product->description,
                        'price'         => $product->price,
                        'image'         => url('image/product/' . $product->image),
                    ];
                }
            }

            return [
                'status' => 'ok',
                'result' => $products,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'city_not_found',
        ];
    }

}
