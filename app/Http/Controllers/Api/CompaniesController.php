<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Company;
use App\Product;

class CompaniesController extends Controller
{
    public function index()
    {
        $data = Company::with('places.city', 'places.neighborhood')->get();

        $companies = [];
        foreach ($data as $item) {

            $places = [];
            foreach ($item->places as $place) {
                $places[] = [
                    'city' => [
                        'id'    => $place->city->id,
                        'name'  => $place->city->name,
                    ],
                    'neighborhood' => [
                        'id'    => $place->neighborhood->id,
                        'name'  => $place->neighborhood->name,
                    ],
                ];
            }

            $companies[] = [
                'id'            => $item->id,
                'name'          => $item->name,
                'image'         => url('image/company/' . $item->image),
                'places'        => $places,
            ];
        }

        return [
            'status' => 'ok',
            'result' => $companies
        ];
    }

    public function show($id)
    {
        $company = Company::select('id', 'name', 'image')->find($id);
        if($company) {

            $company->image = url('image/company/' . $company->image);

            return [
                'status' => 'ok',
                'result' => $company,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'company_not_found',
        ];
    }

    public function products($id)
    {
        $company = Company::select('id', 'name', 'image')->with('products')->find($id);
        if($company) {
            $company->image = url('image/company/' . $company->image);

            $products = [];
            $ids = [];
            foreach ($company->products as $product) {
                if(in_array($product->id, $ids)) continue;
                $ids[] = $product->id;
                $products[] = [
                    'id'            => $product->id,
                    'name'          => $product->name,
                    'description'   => $product->description,
                    'price'         => $product->price,
                    'image'         => url('image/product/' . $product->image),
                ];
            }

            return [
                'status' => 'ok',
                'result' => $products,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'company_not_found',
        ];
    }

    public function product($id)
    {
        $product = Product::select('id', 'name', 'description', 'price', 'image')->find($id);
        if($product) {

            $product->image = url('image/product/' . $product->image);

            return [
                'status' => 'ok',
                'result' => $product,
            ];

        }

        return [
            'status' => 'fail',
            'reason' => 'product_not_found',
        ];
    }

}
