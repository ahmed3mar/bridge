<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Http\Controllers\Controller;
use App\Mosque;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    function store(Request $request) {

        $validator = \Validator::make($request->all(), [
//            'company_id'    => 'required|exists:companies,id',
//            'product_id'    => 'required|exists:products,id',
//            'quantity'      => 'required|numeric|min:1',
            'products'      => 'required',
            'mosque_id'     => 'required|exists:mosques,id',
            'phone'         => 'required|numeric',
            'name'          => 'required|max:255',
            'address'       => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 422);
        }

//        $this->validate($request, [
////            'company_id'    => 'required|exists:companies,id',
////            'product_id'    => 'required|exists:products,id',
////            'quantity'      => 'required|numeric|min:1',
//            'products'      => 'required',
//            'mosque_id'     => 'required|exists:mosques,id',
//            'phone'         => 'required|numeric',
//            'name'          => 'required|max:255',
//            'address'       => 'required|max:255',
//        ]);

        # Check if valid products
        if(isset($request->products) && sizeof($request->products) > 0) {
            foreach ($request->products as $product) {
                if (!isset($product['company_id']) or !isset($product['product_id']) or !isset($product['quantity'])) {
                    return response()->json([
                        'errors' => [
                            'products' => 'Please enter valid object',
                        ]
                    ], 422);
                }
                $_product = Product::find($product['product_id']);
                if (!$_product) {
                    return response()->json([
                        'errors' => [
                            'products' => 'Product of id ' . $product['product_id'] . ' Not found',
                        ]
                    ], 422);
                }
            }
        } else {
            return response()->json([
                'errors' => [
                    'products' => 'Please choose your products',
                ]
            ], 422);
        }

        $order = new Order();
//        $order->company_id = $request->company_id;
//        $order->product_id = $request->product_id;
//        $order->quantity = $request->quantity;
        $order->products = json_encode($request->products);
        $order->mosque_id = $request->mosque_id;
        $order->phone = $request->phone;
        $order->name = $request->name;
        $order->address = $request->address;
        $order->save();

        return [
            'status'    => 'ok',
            'result'    => $order->id
        ];

    }

}
