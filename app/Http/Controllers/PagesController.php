<?php namespace App\Http\Controllers;

use App\Page;

class PagesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $page = Page::whereSlug($slug)->firstOrFail();

        return view('page', compact('page'));
    }

}
