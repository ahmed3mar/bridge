<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function profile() {
        $user = auth()->user();

        return view('user.profile', compact('user'));
    }

    public function save(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        $user = auth()->user();
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return view('user.profile', compact('user'));
    }

    public function edit() {
        $user = auth()->user();

        return view('user.edit_profile', compact('user'));
    }

    function getPayments($contractID)
    {
        $query = 'SELECT sum( brg_sspayments.Amount ) AS paid
                  FROM brg_ssinstallments
                  INNER JOIN brg_sspayments ON brg_sspayments.installmentID = brg_ssinstallments.ID
                  and brg_ssinstallments.ContractID =' . $contractID;
        $customerUnits = \DB::select($query);

        return $customerUnits[0]->paid;
    }// function

    function getInstallmentsCount($contractID)
    {
        //die($contractID);

//        $db =& JFactory::getDBO();
//        $db->setQuery('SET SQL_BIG_SELECTS=1');
//        $db->query();

        $query = 'SELECT count(brg_ssinstallments.ID)
                  FROM brg_ssinstallments left join brg_sspayments on brg_sspayments.installmentID = brg_ssinstallments.ID
                  where brg_ssinstallments.ContractID='. $contractID . ' and brg_sspayments.ID is NULL';
        $Installments1 = \DB::select($query);
        $Installments1 = $Installments1[0]->{'count(brg_ssinstallments.ID)'};

        #$db->setQuery('SET SQL_BIG_SELECTS=1');
        # \DB::query('SET SQL_BIG_SELECTS=1');
        \DB::query('SQL_BIG_SELECTS=1');
        #$db->query();
        $query = 'SELECT count(brg_ssinstallments.ID), sum( brg_sspayments.amount ) AS pamount,brg_ssinstallments.amount
                FROM brg_ssinstallments
                LEFT JOIN brg_sspayments ON brg_sspayments.installmentID = brg_ssinstallments.ID
                WHERE brg_ssinstallments.ContractID ='. $contractID . '
                GROUP BY brg_ssinstallments.ID
                HAVING pamount <> brg_ssinstallments.amount';

        //die( $query);
        $Installments2 = \DB::select($query);
        #$Installments2 = $Installments2[0]->{'count(brg_ssinstallments.ID)'};

        return $Installments1;# + $Installments2;
    }// function

    public function units() {
        $user = auth()->user();

        if(!$user->client) abort(404);

#        \DB::query('SET SQL_BIG_SELECTS=1');

        $ext = "";
        $query = 'SELECT brg_sscontracts.ID as CID,brg_sscontracts.UnitID as ID,brg_sscontracts.TotalValue, if(brg_ssconstructionphases.Name'.$ext.'<>"",brg_ssconstructionphases.Name'.$ext.',brg_ssconstructionphases.Name) as ConstructionPhase,brg_ssunits.PlotNumber,
                    if(brg_ssprojects.Name'.$ext.'<>"",brg_ssprojects.Name'.$ext.',brg_ssprojects.Name) as Name, "1" as paid, "1" as inst
                    FROM brg_sscustomers inner join brg_sscontracts on brg_sscustomers.ID = brg_sscontracts.CustomerID
                    inner join brg_ssunits on brg_ssunits.ID = brg_sscontracts.UnitID
                    inner join brg_ssprojects on brg_ssprojects.ID = brg_ssunits.ProjectID
                    inner join brg_ssconstructionphases on brg_ssconstructionphases.ID = brg_ssunits.ConstructionPhaseID
                    WHERE brg_sscontracts.CustomerID=' . $user->client->ID;

        $units = \DB::select($query);
        foreach ($units as $unit)
        {
            $unit->paid = $this->getPayments($unit->CID);
        }
        foreach ($units as $unit)
        {
            $unit->inst = $this->getInstallmentsCount($unit->CID);
        }
        return view('user.units', compact('user', 'units'));
    }

    var $_ext = '';
    public function unit($id) {

        $resStatExt = ($this->_ext == "_trans")? "_en" : "_ara";

        $query = 'SELECT brg_ssunits.ID as uid, brg_ssunits.*, brg_ssprojects.ID as pid,brg_ssprojects.*, if(brg_ssprojects.Name'.$this->_ext.'<>"",brg_ssprojects.Name'.$this->_ext.',brg_ssprojects.Name) as Name,
                 if(brg_ssregions.Name'.$this->_ext.'<>"",brg_ssregions.Name'.$this->_ext.',brg_ssregions.Name) as region,
                 if(brg_sscities.Name'.$this->_ext.'<>"",brg_sscities.Name'.$this->_ext.',brg_sscities.Name) as city,
                 if(brg_ssprovinces.Name'.$this->_ext.'<>"",brg_ssprovinces.Name'.$this->_ext.',brg_ssprovinces.Name) as province, '
            . 'if(brg_sscategories.Name'.$this->_ext.'<>"",brg_sscategories.Name'.$this->_ext.',brg_sscategories.Name) as cat,
            if(brg_sssubcategories.Name'.$this->_ext.'<>"",brg_sssubcategories.Name'.$this->_ext.',brg_sssubcategories.Name) as subcat,
            if(brg_ssfloors.Name'.$this->_ext.'<>"",brg_ssfloors.Name'.$this->_ext.',brg_ssfloors.Name) as floor,
             if(brg_ssfinishinglevels.Name'.$this->_ext.'<>"",brg_ssfinishinglevels.Name'.$this->_ext.',brg_ssfinishinglevels.Name) as flevel, '
            . ' if(brg_ssreservationstatus.Name'.$resStatExt.'<>"",brg_ssreservationstatus.Name'.$resStatExt.',brg_ssreservationstatus.Name) as reserv ,
             if(brg_ssconstructionphases.Name'.$this->_ext.'<>"",brg_ssconstructionphases.Name'.$this->_ext.',brg_ssconstructionphases.Name) as const,
             if(brg_ssconstructionphasedetails.Name'.$this->_ext.'<>"",brg_ssconstructionphasedetails.Name'.$this->_ext.',brg_ssconstructionphasedetails.Name) as constdetails,'
            . 'DATE_FORMAT(DATE(brg_ssprojects.IntialDeliveryDate),"%d-%m-%Y") as IntialDeliveryDate, DATE_FORMAT(DATE(brg_ssprojects.FinalDeliveryDate),"%d-%m-%Y") as FinalDeliveryDate'
            . ' FROM brg_ssunits inner join brg_ssprojects on brg_ssunits.ProjectID = brg_ssprojects.ID and brg_ssunits.ID='.$id
            . ' left join brg_ssregions on brg_ssregions.ID = brg_ssprojects.RegionID  '
            . 'left join brg_sscities on brg_sscities.ID = brg_ssregions.CityID  '
            . 'left join brg_ssprovinces on brg_ssprovinces.ID = brg_sscities.ProvinceID  '
            . 'left join brg_sscategories on brg_sscategories.ID = brg_ssunits.CategoryID  '
            . 'left join brg_sssubcategories on brg_sssubcategories.ID = brg_ssunits.SubcategoryID  '
            . 'left join brg_ssfloors on brg_ssfloors.ID = brg_ssunits.FloorID  '
            . 'left join brg_ssfinishinglevels on brg_ssfinishinglevels.ID = brg_ssunits.FinishingLevelID  '
            . 'left join brg_ssreservationstatus on brg_ssreservationstatus.ID = brg_ssunits.ReservationStatusID  '
            . 'left join brg_ssconstructionphases on brg_ssconstructionphases.ID = brg_ssunits.ConstructionPhaseID  '
            . 'left join brg_ssconstructionphasedetails on brg_ssconstructionphasedetails.ID = brg_ssunits.ConstructionPhaseDetailID  ';

        //die($query);

        $unit = \DB::select($query)[0];
        $unit->pimages = \DB::select('select Image from brg_sspimages where ID = ' . $unit->pid);
        return view('user.unit', compact('unit'));
    }

    public function payments($contractID) {

        $curLang = app()->getLocale();
        $ext = "";
        if($curLang == "en")
            $this->_ext = "_trans";

        $query = '(SELECT brg_ssinstallments.Amount, if(Type'.$ext.'<>"",Type'.$ext.',Type) as Type,DATE_FORMAT(DATE(brg_ssinstallments.Date),"%d-%m-%Y") as idate , "0" as pamount, brg_ssinstallments.Date
                  FROM brg_ssinstallments left join brg_sspayments on brg_sspayments.installmentID = brg_ssinstallments.ID
                  where brg_ssinstallments.ContractID= '. $contractID . ' and brg_sspayments.ID is NULL order by brg_ssinstallments.Date)
                    union
                    (SELECT brg_ssinstallments.amount,if(Type'.$ext.'<>"",Type'.$ext.',Type) as Type,DATE_FORMAT(DATE(brg_ssinstallments.Date),"%d-%m-%Y") as idate, sum( brg_sspayments.amount ) AS pamount, brg_ssinstallments.Date
                    FROM brg_ssinstallments
                    LEFT JOIN brg_sspayments ON brg_sspayments.installmentID = brg_ssinstallments.ID
                    WHERE brg_ssinstallments.ContractID ='. $contractID . '
                    GROUP BY brg_ssinstallments.ID
                    HAVING pamount <> brg_ssinstallments.amount)
                    order by date';
        $payments = \DB::select($query);

        if(\request()->has('ajax')) return view('user.payments_ajax', compact('payments'));
        return view('user.payments', compact('payments'));
    }

}
