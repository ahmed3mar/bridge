<?php

use App\Option;
static $options = [];

function parseOptions() {
    global $options;

    $options = Option::all()->pluck('value', 'key');
    return $options;
}

function option( $key ) {
    global $options;

    if (!$options || sizeof($options) == 0) $options = parseOptions();

    return $options->get($key);

}

function slider_image($img) {
    return route('image', ['slider', $img]);
}

function drop_categories($type, $parent = 0, $level = 0) {
    $categories = \App\Category::whereType($type)->whereParentId($parent)->orderBy('order')->get();
    $options = "";
    foreach ($categories as $category) {
        $options .= '<option value="'.$category->id .'">'. str_repeat('&nbsp;&nbsp;', $level) . '- '  . $category->title .'</option>';
        $options .= drop_categories($type, $category->id, ($level + 1));
    }
    return $options;
}

function save_option($key, $value) {
    $option = Option::where(['key' => $key])->first();
    if(!$option) {
        $option = new Option();
        $option->key = $key;
    }
    $option->value = $value;
    $option->save();
}

function youtube_id_from_url($url) {
    $pattern =
        '%^# Match any youtube URL
                (?:https?://)?  # Optional scheme. Either http or https
                (?:www\.)?      # Optional www subdomain
                (?:             # Group host alternatives
                  youtu\.be/    # Either youtu.be,
                | youtube\.com  # or youtube.com
                  (?:           # Group path alternatives
                    /embed/     # Either /embed/
                  | /v/         # or /v/
                  | /watch\?v=  # or /watch\?v=
                  )             # End path alternatives.
                )               # End host alternatives.
                ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
                $%x'
    ;
    $result = preg_match($pattern, $url, $matches);
    if ($result) {
        return $matches[1];
    }
    return false;
}

function admin_resource($route, $controller, $name) {
    Route::post($route . '/data', $controller . '@data')->name('.' . $name . '.data');

    Route::get($route, $controller . '@index')->name('.' . $name);
    Route::get($route . '/create', $controller . '@create')->name('.' . $name . '.create');
    Route::post($route, $controller . '@store')->name('.' . $name . '.store');
    Route::get($route . '/{id}', $controller . '@show')->name('.' . $name . '.show');
    Route::get($route . '/{id}/edit', $controller . '@edit')->name('.' . $name . '.edit');
    Route::put($route . '/{id}', $controller . '@update')->name('.' . $name . '.update');
    Route::delete($route . '/{id}', $controller . '@destroy')->name('.' . $name . '.destroy');
//    Route::resource($route, $controller)->names([
//        'index' => '.' . $name
//    ]);
}

# Translations
function _trans($key) {
    return \Lang::locale() == 'ar' ? $key . '_ar' : $key;
}

# Permissions
function list_permissions() {
    $permissionsData = \App\Permission::all();
    $permissionsGroups = [];
    foreach ($permissionsData as $permission) {
        if (!isset($permissionsGroups[$permission->category]))
            $permissionsGroups[$permission->category] = [];

        $permissionsGroups[$permission->category][] = [
            'name'  => $permission->name,
            'id'    => $permission->id,
        ];
    }
    return $permissionsGroups;
}

function no_permission() {
    return response()->json([
        'success'   => false,
        'message'   => 'no-permission'
    ], 403);
}

function can($permission) {
    return auth()->user()->can($permission);
}

// Due to repeating in each Controller in Admin
function data_edit_btn($model) {
    $back = "&nbsp;&nbsp;";
    $back .= '<a href="#" data-id="' . $model->id . '" return false;" title="Edit" class="edit-item tooltips"><i class="fa fa-1-8x fa-edit"></i></a>';
    return $back;
}

function data_btn($model, $title, $class, $icon) {
    $back = "&nbsp;&nbsp;";
    $back .= '<a href="#" data-id="' . $model->id . '" return false;" title="'.$title.'" class="'.$class.' tooltips">'.$icon.'</a>';
    return $back;
}

function data_delete_btn($model, $text = "", $icon = '<i class="fa fa-times fa-1-8x text-danger"></i>') {
    $back = $text ? "" : "&nbsp;&nbsp;";
    $back .= '<a href="#"
                        class="delete-item"
                        data-id="' . $model->id . '"
                        data-toggle="confirmation"
                        data-placement="top"
                        data-btnOkLabel="<i class=\'icon-ok-sign icon-white\'></i> Yes"
                        data-btnCancelLabel="<i class=\'icon-remove-sign\'></i>  No"
                        data-title="Are you sure to delete?" data-target="_self"
                        >' . $icon.$text.'</a>';
    return $back;
}

function img($url, $alt = '', $attrs = "") {
    return '<img src="' . $url . '" alt="'.$alt.'" '.$attrs.' />';
}