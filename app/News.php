<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $connection = 'mysql';

    public function category() {
        return $this->belongsTo(Category::class);
    }

    function getUrlAttribute() {
        return route('article', $this->id);
    }

    function getImageUrlAttribute() {

        $images = json_decode($this->images);
        if(sizeof($images) == 0) return '';

        return route('image', ['news', $images[0]->image]);
    }
}
