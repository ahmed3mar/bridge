<?php

return [

    'users' => [
        'permissions'   => ['add', 'edit', 'delete'],
        'datatable'     => [
            'columns'   => [
                'id' => '#',
                'name',
                'role',
                'phone',
                'status' => function(\App\User $user) {
                    if ($user->isBlocked()) {
                        return '<span class="label label-danger">BLOCKED</span>';
                    } else if ($user->isActive()) {
                        return '<span class="label label-success">ACTIVE</span>';
                    }
                    return '<span class="label label-warning">REQUIRE ACTIVATION</span>';
                },
                'created_at' => 'Register Date',
                'options'
            ],
            'options'   => ['edit', 'delete']
        ],
        'options'       => ['add']
    ]

];
