<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();

            $table->string('title');
            $table->string('title_ar')->nullable();

            $table->string('image')->nullable();

            $table->string('description')->nullable();
            $table->string('description_ar')->nullable();

            $table->text('content');
            $table->text('content_ar')->nullable();

            # SEO
            $table->string('seo_title')->nullable();
            $table->string('seo_title_ar')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('seo_description_ar')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
