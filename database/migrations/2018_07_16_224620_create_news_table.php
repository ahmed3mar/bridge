<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');

            $table->string('title');
            $table->string('title_ar')->nullable();

            $table->longText('images');

            $table->text('content');
            $table->text('content_ar')->nullable();

            # SEO
            $table->string('seo_title')->nullable();
            $table->string('seo_title_ar')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('seo_description_ar')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
