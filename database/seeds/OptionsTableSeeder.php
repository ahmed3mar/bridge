<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $options = [
            'site_title'            => 'Drive Yalla',
            'site_title_ar'         => 'درايف يلا',

            'site_description'      => 'Site Description',
            'site_description_ar'   => 'وصف الموقع',

            'site_keywords'         => 'Site,Word,Test,Drive,Yalla',
            'site_keywords_ar'      => 'موقع,yalla',
            'slider_links'      => '[]',


            'site_close'    => 0,
            'site_close_message' => '',
            'site_close_message_ar' => '',
        ];

        $i = 0;
        foreach ($options as $key => $val) {
            DB::table('options')->insert([
                'key' => $key,
                'value' => $val,
            ]);
            $i++;
        }

    }
}
