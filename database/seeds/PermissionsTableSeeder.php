<?php

use App\User;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            'users'                 => ['add-user'  , 'edit-user'   , 'delete-user'],
            'images-categories'     => ['add-images-category'  , 'edit-images-category'   , 'delete-images-category'],
            'videos-categories'     => ['add-videos-category'  , 'edit-videos-category'   , 'delete-videos-category'],
            'news-categories'     => ['add-news-category'  , 'edit-news-category'   , 'delete-news-category'],
            'projects-categories'   => ['add-projects-category'  , 'edit-projects-category'   , 'delete-projects-category'],
            'projects'              => ['add-project'  , 'edit-project'   , 'delete-project'],
            'translations'          => ['add-translation'  , 'edit-translation'   , 'delete-translation'],
            'gallery'               => ['add-gallery'  , 'edit-gallery'   , 'delete-gallery'],
            'team'               => ['add-person'  , 'edit-person'   , 'delete-person'],
            'pages'               => ['add-page'  , 'edit-page'   , 'delete-page'],
            'news'               => ['add-news'  , 'edit-news'   , 'delete-news'],
            'services'               => ['add-service'  , 'edit-service'   , 'delete-service'],
            'options'               => ['edit-options'],
            'contacts'               => ['view-contacts', 'edit-contact', 'delete-contact'],
        ];

        $per = [];
        foreach ($permissions as $category => $dd) {
            foreach ($dd as $p) {
                $permission = new \App\Permission();
                $permission->category = $category;
                $permission->name = $p;
                $permission->save();
                $per[] = $permission->id;
            }
        }

        $user = User::all();
        $user[0]->permissions()->attach($per);

    }
}
