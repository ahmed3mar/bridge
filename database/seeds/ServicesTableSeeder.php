<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                'title' => 'UX & UI Interface Design',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, voluptatem, temporibus, inventore corrupti repellat exercitationem error consequuntur debitis itaque deleniti nesciunt est qui aut praesentium vero rerum unde suscipit explicabo.
                Consectetur, consequuntur, culpa iste sunt ab illo eum quidem reprehenderit repellendus id doloremque ex explicabo possimus quasi ea nam sed. Ullam, quae.',
                'icon' => 'desktop'
            ],
            [
                'title' => '3D Modeling & Animation',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, voluptatem, temporibus, inventore corrupti repellat exercitationem error consequuntur debitis itaque deleniti nesciunt est qui aut praesentium vero rerum unde suscipit explicabo.
                Consectetur, consequuntur, culpa iste sunt ab illo eum quidem reprehenderit repellendus id doloremque ex explicabo possimus quasi ea nam sed. Ullam, quae.',
                'icon' => 'cubes'
            ],
            [
                'title' => 'Branding & Marketing',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, voluptatem, temporibus, inventore corrupti repellat exercitationem error consequuntur debitis itaque deleniti nesciunt est qui aut praesentium vero rerum unde suscipit explicabo.
                Consectetur, consequuntur, culpa iste sunt ab illo eum quidem reprehenderit repellendus id doloremque ex explicabo possimus quasi ea nam sed. Ullam, quae.',
                'icon' => 'star'
            ],
        ];
        foreach ($services as $service) {
            DB::table('services')->insert([
                'title' => $service['title'],
                'description' => $service['description'],
                'icon' => $service['icon'],
            ]);
        }
    }
}
