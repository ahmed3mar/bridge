/**
 * Created by ahmed3mar on 08/04/2016.
 */

/**
 * available rules!
 * `` required
 * `` max[x]
 * `` min[x]
 * `` url
 * `` email
 * `` number
 * `` equalTo
 * `` creditcard
 * `` digits
 * `` date
 * `` dateISO
 * @param rules
 * @returns {Array}
 */

function validateRules(rules) {
    var _r = [];
    for(var x in rules){
        var __r = rules[x].replace(/\s+/, "");
        _r.push(__r);
    }
    return _r;
}
var _lang = function (s) {
    if( lang[s] )
    {
        return lang[s];
    } else {
        var _s = s.split('.');
        if( lang[_s[0]] && lang[_s[0]][_s[1]] )
            return lang[_s[0]][_s[1]];
    }
    return s;
}
$.extend($.fn, {

    validate_error: function (type) {
        var selector = $(this).attr('id');

        var $msg = _lang('validation.' + type);
        $msg = $msg.replace(/{field}/g, $('[for="'+selector+'"]').text());
        return $msg;

    },

    validation: function (options) {

        // if nothing is selected, return nothing; can't chain anyway
        if ( !this.length ) {
            if ( options && options.debug && window.console ) {
                console.warn( "Nothing selected, can't validate, returning nothing." );
            }
            return;
        }

        var validationType = 'normal';
        if( options && options.type == 'icon' ) validationType = 'icon';
        if( options && options.type == 'all' ) validationType = 'all';

        var selector = '#' + $(this).attr('id');

        var rules = {};
        $(selector).find("[data-validation]").each(function () {

            var $id = $(this).attr("id");

            var $opt = {};
            var opts = validateRules( $(this).attr("data-validation").split(',') );
            for(var x in opts)
            {
                if( opts[x] == "required" )
                    $opt["required"] = true;

                if( opts[x] == "number" )
                    $opt["number"] = true;
            }
            rules[$id] = $opt;

        });

        var messages = {};
        $(selector).find("[data-validation]").each(function () {

            var $id = $(this).attr("id");
            var $opt = {};
            var opts = validateRules( $(this).attr("data-validation").split(',') );
            for(var x in opts)
            {
                if( opts[x] == "required" )
                    $opt["required"] = $('#' + $id).validate_error('required');

                if( opts[x] == "number" )
                    $opt["number"] = true;
            }
            messages[$id] = $opt;

        });
        var defaultOptions = {

            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input

            rules: rules,
            messages: messages,

            invalidHandler: function (event, validator) { //display error alert on form submit
                //App.toast('error', App.lang("error_title"), App.lang("form_has_errors"));
                // App.scrollTo($error, -200);
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label, element) {
                if( validationType == 'icon' || validationType == 'all' )
                {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                }

                if( validationType == 'normal' || validationType == 'all' )
                {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                }

            },

            errorPlacement: function(error, element) {
                if( validationType == 'icon' || validationType == 'all' ) {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                }
                if( validationType == 'normal' || validationType == 'all' ) {
                    error.insertAfter( element );
                    $(element).closest('.form-group').addClass('has-error');//.find('.help-block').html($(error).html())
                }
            },

            submitHandler: function(form) {
                //form.submit(); // form validation success, call ajax form submit
            }

        };
        $.extend( defaultOptions, options );

        $(selector).validate(defaultOptions);

        $(selector + ' input').keypress(function(e) {
            if (e.which == 13) {
                if( $(this).attr('no-submit') ) return false;
                if ($(selector).validate().form()) {
                    $(selector).submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        $('[type="reset"]').click(function () {
            $(selector).validate().resetForm();
        });

        $('.submit-form').click(function () {
            if( typeof selector == 'undefined') return false;
            if ($(selector).validate().form()) {
                $(selector).submit(); //form validation success, call ajax form submit
            }
            return false;
        });

    }

});