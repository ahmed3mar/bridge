/*===========================================
=            SCRIPT.JS STRUCTURE            =
===========================================*/

/**
 *
 * 1. Header Slider
 * 2. Video Player
 * 3. Go Up
 * 4. Parallax Header
 * 5. Our Team Carousel
 * 6. Gallery animations
 * 7. Responsive Menu
 * 8. Google Maps
 *
 **/



$(document).ready(function() {




        if (navigator.platform != "MacIntel") { // Because Apple has much better scrolling
            $("html").niceScroll({
                cursorwidth: 10,

                scrollspeed: 		60,
                autohidemode: 		false,
                hidecursordelay: 	400,
                cursorfixedheight: 	false,
                cursorminheight: 	20,
                enablekeyboard: 	true,
                horizrailenabled: 	true,
                bouncescroll: 		false,
                smoothscroll: 		true,
                iframeautoresize: 	true,
                touchbehavior: 		false,
                zindex: 999
            });
        }

    }


);

$(window).load(function() {

    clearInterval(timer);
    var inner = document.getElementById('inner');
    inner.setAttribute("style", "width:"+ 100 +"%");

    setTimeout(function(){
        $("body").addClass("loaded");
        $("body").removeClass("loading");
    }, 500);

});



/*===================================
=            LOADING BAR            =
===================================*/

(function(){

    var position = 0;
    var loadingBar = document.getElementById('loading-bar');
    var inner = document.getElementById('inner');
    var max = 100;

    timer = setInterval(function(){
        var widthToSet = position + ((max - position)/10);
        inner.setAttribute("style", "width:"+ widthToSet +"%");
        position = widthToSet;
    }, 250);



})();



/*=====================================
=            HEADER SLIDER            =
=====================================*/
$(function() {

if($("body").hasClass("home")){
    headerSwiper = $('header .swiper-container').swiper({
        //Your options here:
        mode: 'horizontal',
        loop: true,
        speed: 750,
        autoplay: 3000,
        longSwipesRatio: 0.1,
        pagination: '.swiper-indicators',
        paginationClickable: true


        //etc..
    });

    $("header").mouseenter(function() {
        headerSwiper.stopAutoplay();

    });
    $("header").mouseleave(function() {
        headerSwiper.startAutoplay();
    })

    $("header .arrow.right").click(function() {
        headerSwiper.swipeNext();
    });
    $("header .arrow.left").click(function() {
        headerSwiper.swipePrev();
    });



    var slideAmount = $("header .swiper-slide").length;


    $("header .prev_thumbnail").css("background-image", $("header .swiper-slide:nth-of-type(" + 1 + ")").css("background-image"));
    $("header .next_thumbnail").css("background-image", $("header .swiper-slide:nth-of-type(" + 3 + ")").css("background-image"));


    headerSwiper.addCallback('SlideChangeStart', function(swiper) {
        var activeSlide = headerSwiper.activeIndex;

        if (activeSlide == 0) {
            activeSlide = slideAmount - 2;
        } else if (activeSlide == slideAmount) {
            activeSlide = 1;
        }

        var prevImage = $("header .swiper-slide:nth-of-type(" + activeSlide + ")").css("background-image");


        if (activeSlide + 1 == slideAmount) {
            activeSlide = 1;
        }

        var nextImage = $("header .swiper-slide:nth-of-type(" + (activeSlide + 2) + ")").css("background-image");

        $("header .prev_thumbnail").css("background-image", prevImage);
        $("header .next_thumbnail").css("background-image", nextImage);
    })

}
});

/*====================================
=            VIDEO PLAYER            =
====================================*/
(function() {
    $("section.video .playButton, section.video video").click(function() {
        var video = $("#vid1").get(0);
        if (video.paused) {
            $("section.video").addClass("wrapped");
            $("section.video .fa").removeClass("fa-play");
            $("section.video .fa").addClass("fa-pause");
            video.play();
        } else {
            $("section.video").removeClass("wrapped");
            $("section.video .fa").removeClass("fa-pause");
            $("section.video .fa").addClass("fa-play");
            video.pause();
        }
    })

})(jQuery);

/*=============================
=            GO UP            =
=============================*/
(function() {
    $(".goUp").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1000, 'swing');
    })
})(jQuery);

/*=======================================
=            PARALLAX HEADER            =
=======================================*/
(function() {
    $(window).scroll(function() {
        var position = ($(this).scrollTop()) / 2;
        $("header .swiper-container .swiper-slide").css('background-position-y', position);
        $("header .swiper-container .swiper-slide video").css('margin-top', position);

    });
})(jQuery);

/*=========================================
=            OUR TEAM CAROUSEL            =
=========================================*/
(function() {

    //Currently centered member
    var current_Member = 1
    //Member object
    var memberObj = $("section.team .member");
    // Count team members
    var count = memberObj.length;
    // Animation time in ms
    var animation_Time = 330;


    if (count < 4) {
        $(".arrow-left, .arrow-right").addClass("hidden");
    }


    function moveCarousel(direction) {



        $(memberObj[current_Member]).removeClass("center");
        if (direction > 0) {
            $(memberObj[current_Member]).addClass("left");
        } else {
            $(memberObj[current_Member]).addClass("right");
        }

        $(memberObj[current_Member - direction]).addClass("hiddenMember");
        $(memberObj[current_Member - direction]).removeClass("left right");


        $(memberObj[current_Member + direction]).addClass("center");
        $(memberObj[current_Member + direction]).removeClass("left right");


        if (direction > 0) {
            $(memberObj[current_Member + (2 * direction)]).addClass("right");
        } else {
            $(memberObj[current_Member + (2 * direction)]).addClass("left");
        }
        $(memberObj[current_Member + (2 * direction)]).removeClass("hiddenMember");



        current_Member += direction;


    }
    $("section.team .arrow-right").click(function() {
        if (current_Member < count - 1) {
            moveCarousel(1);
        }

    });
    $("section.team .arrow-left").click(function() {
        if (current_Member > 0) {
            moveCarousel(-1);
        }

    });
})(jQuery);

/*==========================================
=            GALLERY ANIMATIONS            =
==========================================*/


(function() {

    function init() {
        var speed = 250,
            easing = mina.easeinout;

        [].slice.call(document.querySelectorAll('.gallery  figure')).forEach(function(el) {


                var s = Snap(el.querySelector('svg')),
                    path = s.select('path'),
                    pathConfig = {
                        from: path.attr('d'),
                        to: el.getAttribute('data-path-hover')
                    };

                el.addEventListener('mouseenter', function() {
                    path.animate({
                        'path': pathConfig.to
                    }, speed, easing);

                });

                el.addEventListener('mouseleave', function() {
                    path.animate({
                        'path': pathConfig.from
                    }, speed, easing);
                    //path.animate( { fill: "#ececec" }, speed, easing );
                });
            });
    }


    $(document).ready(function() {

    var galSwiper = $('.gallery .swiper-container').swiper({
        //Your options here:
        mode: 'horizontal',
        speed: 750,
        longSwipesRatio: 0.1
        //etc..
    });

    $(".gallery-next").click(function() {
        galSwiper.swipeNext();
    });

    $(".gallery-prev").click(function() {
        galSwiper.swipePrev();
    });


    init();

    });
})(jQuery);


/*=====================================
 =            Voice Control            =
 =====================================*/

// (function() {

//     function goTo(id){
//         var scrollTo = $(id).offset().top;

//         $("html, body").animate({
//             scrollTop: scrollTo - 100
//         }, 1000, 'swing');
//     }

//     var commands = {
//         'services': function() {
//             goTo("#services");

//         },

//         'portfolio': function() {
//             goTo("#portfolio");

//         },

//         'why the one': function() {
//             goTo("#features");

//         },

//         'blog': function() {
//             goTo("#blog");

//         },

//         'our team': function() {
//             goTo("#team");

//         },

//         'contact': function() {
//             goTo("#contact");

//         },

//         'previous': function() {
//             headerSwiper.swipePrev();
//         },
//         'back': function() {
//             headerSwiper.swipePrev();
//         },
//         'next': function() {
//             headerSwiper.swipeNext();
//         }


//     };
//     annyang.addCommands(commands);
//     annyang.start();

// })(annyang);


/*=======================================
=            RESPONSIVE MENU            =
=======================================*/
(function() {
    var showMenu = false;
    $(".show-menu, .menu-icon").click(function() {
        if (showMenu) {
            showMenu = false;
            $(this).html("MENU");
            $(this).attr("href", "#menu");
            window.location = "#";
            $('#menu').hide();
        } else {
            showMenu = true;
            $(this).html("Hide Menu");
            $(this).attr("href", "#");
            window.location = "#menu";
            $('#menu').show();
        }
    });
})(jQuery);

/*===================================
=            STICKY MENU            =
===================================*/

(function() {
if($("body").hasClass("home")){







    var lastScrollTop = 0;
    $(window).scroll(function(event) {


        if($(".top-menu").hasClass("mobileMenu")){

        }else{


            var st = $(this).scrollTop();

            if (st > $("header").height() - 130) {
                $(".top-menu").addClass("sticky");

                $(".top-menu").css("top", 0);


            } else {
                $(".top-menu").removeClass("sticky");

                if (st < 100) {
                    $(".top-menu").css("top", -st);
                } else if (st < 200) {
                    $(".top-menu").removeClass("animated");
                } else {
                    $(".top-menu").css("top", -100);
                    $(".top-menu").addClass("animated");
                }
            }


            lastScrollTop = st;


        } // if has class mobilemenu
    });
}
})(jQuery);

/*============================
=            MENU            =
============================*/

(function() {

    $('#video-player').attr('src', 'videos/new.mp4');

    $("nav#menu a").click(function(event) {
        if(config.current !== 'home') {
            location.href = config.url
        }
        var whereTo = $(this).attr("href");
        if(whereTo.startsWith('#')) event.preventDefault();

        var scrollTo = $(whereTo).offset().top;

        $("html, body").animate({
            scrollTop: scrollTo - 100
        }, 1000, 'swing');
    })

    // Grab positions of our sections
    $(document).ready(function() {
        sectionsPosition = [];
        sectionId = [];

        $('section').each(function() {
            var ot = $(this).offset().top;
            var id = $(this).attr("id");
            sectionsPosition.push( ot ) ;
            sectionId.push( id );


        });


        // setInterval(function() {
        //     var st = $("html, body").scrollTop();
        //
        //     for (i = 0; i < sectionsPosition.length; i++) {
        //         if (st < sectionsPosition[i]-100) {
        //             $("nav#menu li").removeClass("active");
        //             $("nav#menu li").find('a[href="#'+ sectionId[i] +'"]').parent("li").addClass("active");
        //
        //             break;
        //         }
        //     }
        //
        // }, 330);


    });
})(jQuery);

/*===================================
=            GOOGLE MAPS            =
===================================*/
(function() {
    function initialize() {

        var $_map = $('#map');

        var mapOptions = {
            center: new google.maps.LatLng($_map.data('lat'), $_map.data('lng')),
            zoom: 15,
            scrollwheel: false,
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        var image = 'img/pointer.png';
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng($_map.data('lat'), $_map.data('lng')),
            title: "Hello World!",
            icon: image
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);

        var styles = [{
            stylers: [{
                saturation: -100
            }]
        }, {
            featureType: "road",
            elementType: "geometry",
            stylers: [{
                lightness: 100
            }, {
                visibility: "simplified"
            }]
        }, {
            featureType: "road",
            elementType: "labels",
            stylers: [{
                visibility: "off"
            }]
        }];

        map.setOptions({
            styles: styles
        });


        infobox = new InfoBox({
            content: document.getElementById("infobox"),
            disableAutoPan: false,
            maxWidth: 185,
            pixelOffset: new google.maps.Size(30, -133),
            zIndex: null,
            infoBoxClearance: new google.maps.Size(1, 1)
        });

        infobox.open(map, marker);



    }
    google.maps.event.addDomListener(window, 'load', initialize);
})();

/*========================================
=            SCROLLING EFFECT            =
========================================*/
(function($) {

    $.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})(jQuery);

(function() {
    setInterval(function() {


        $(".scrollAnimation").each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                el.addClass("come-in");
            }
        });
    }, 330);
})(jQuery);




(function(){
    var gallery = $("section.gallery");
    var figure = $("section.gallery figure");
    var figureHeight = figure.height();
    var rows = 2;
    $( window ).resize(function() {

        if($(this).width() < 768){
            rows = 4;
        }else if($(this).width() < 1200){
            rows = 3;
        }else{
            rows = 2;
        }
        figureHeight = figure.height();
        gallery.height(220 + (figureHeight*rows));

    });

    setTimeout(function () {
        $(window).trigger('scroll')
        $("html, body").animate({
            scrollTop: 1
        }, 1, 'swing');
    }, 1000)

    $(window).load(function() {
        if($(this).width() < 768){
            rows = 4;
        }else if($(this).width() < 1200){
            rows = 3;
        }else{
            rows = 2;
        }
        figureHeight = figure.height();
        gallery.height(220 + (figureHeight*rows));
    });
})()


$(function(){
    var element = $("#menu li");
    var container = $("#menu")
    var el_width = 0;

    element.each(function(){
        el_width += $(this).outerWidth();
    })

    if(el_width > container.width()){
        $(".top-menu").addClass("mobileMenu");
    }else{
        $(".top-menu").removeClass("mobileMenu");
    }

    $( window ).resize(function() {

        if(el_width > container.width()){
            $(".top-menu").addClass("mobileMenu");
        }else{
            $(".top-menu").removeClass("mobileMenu");
        }
    });
});

