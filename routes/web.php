<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('image/{type}/{filename}', 'ImageController@index')
    ->name('image')
;

Route::get('thumb/{w}x{h}/{type}/{filename}', 'ImageController@thumb')
    ->where('w', '5|50|100|150|300|400|275|250')
    ->where('h', '5|50|100|150|300|280|180|250')
    ->name('thumb')
;

Route::get('start', function() {
    dd(Artisan::call("migrate:refresh", [
        '--force' => true,
    ]));
});

# Admin Routes!
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin'], function () {

    Route::get('/', 'HomeController@index')->name('.main');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('.logout');
    Route::get('logout', function () {
        Auth::logout();
        return redirect('/admin');
    })->name('.logout');

    Route::group(['middleware' => ['auth.admin']], function () {

        #if(function_exists('admin_resource')) {
            admin_resource('users', 'UsersController', 'users');
            admin_resource('translations', 'TranslationsController', 'translations');
            admin_resource('services', 'ServicesController', 'services');


//            Route::get('categories/{type}', 'CategoriesController@index')->name('.categories');
//            Route::delete('categories/{type}/{id}', 'CategoriesController@destroy')->name('categories.delete');
//    //        Route::post('categories/{type}/sort', 'CategoriesController@sort')->name('images.sort');
//            Route::put('categories/{type}', 'CategoriesController@store')->name('categories.store');

            admin_resource('categories/{type}', 'CategoriesController', 'categories');
            Route::post('categories/{type}/sort', 'CategoriesController@sort');
            admin_resource('gallery/{type}', 'GalleryController', 'gallery');
            admin_resource('projects', 'ProjectsController', 'projects');
            admin_resource('news', 'NewsController', 'news');

            admin_resource('neighborhoods', 'NeighborhoodsController', 'neighborhoods');
            Route::get('neighborhoods/list/{city_id}', 'NeighborhoodsController@list')->name('.neighborhoods.list');
            admin_resource('mosques', 'MosquesController', 'mosques');
            admin_resource('companies', 'CompaniesController', 'companies');
            admin_resource('products', 'ProductsController', 'products');
            admin_resource('places', 'PlacesController', 'places');
            admin_resource('pages', 'PagesController', 'pages');
            admin_resource('orders', 'OrdersController', 'orders');
        #}

        Route::get('contacts/export', 'ContactsController@export')->name('.contacts.export');
        Route::post('contacts/data', 'ContactsController@data')->name('.contacts.data');
        Route::resource('contacts', 'ContactsController');

        Route::get('options', 'OptionsController@index')->name('.options.index');
        Route::post('options', 'OptionsController@update')->name('.options.update');

    });

});

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localizationRedirect', 'localeViewPath' ],
], function() {

    Route::get('/', 'HomeController@index')->name('home');

    // USER
    Route::get('profile', 'UserController@profile')->name('profile');
    Route::get('profile/edit', 'UserController@edit')->name('edit.profile');
    Route::post('profile', 'UserController@save')->name('save.profile');
    Route::get('units', 'UserController@units')->name('units');
    Route::get('unit/{id}', 'UserController@unit')->name('unit');
    Route::get('payments/{id}', 'UserController@payments')->name('payments');
    Route::get('logout', function () {
        Auth::logout();
        return redirect('/profile');
    })->name('user.logout');

    Route::get('/news', 'NewsController@index')->name('news');
    Route::post('/order', 'OrdersController@index')->name('order.send');
    Route::get('/news/{id}', 'NewsController@article')->name('article');

    Route::get('/gallery', 'GalleryController@index')->name('gallery');
    Route::get('/projects', 'ProjectsController@index')->name('projects');
    Route::get('/projects/{id}', 'ProjectsController@projects')->name('projects.category');
    Route::get('/project/{id}', 'ProjectsController@project')->name('project');
    Route::get('/page/{slug}', 'PagesController@index')->name('page');
    Route::get('/{type}/{id}', 'CategoriesController@index')->name('category');

});

Auth::routes();
