<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api', 'prefix' => 'v1', 'as' => 'api'], function () {
    Route::get('cities', 'CitiesController@index');
    Route::get('city/{id}', 'CitiesController@show');
    Route::get('city/{id}/neighborhoods', 'CitiesController@neighborhoods');
    Route::get('city/{id}/companies', 'CitiesController@companies');
    Route::get('city/{id}/mosques', 'CitiesController@mosques');
    Route::get('city/{id}/products', 'CitiesController@products');

    Route::get('neighborhood/{id}', 'NeighborhoodsController@show');
    Route::get('neighborhood/{id}/companies', 'NeighborhoodsController@companies');
    Route::get('neighborhood/{id}/mosques', 'NeighborhoodsController@mosques');
    Route::get('neighborhood/{id}/products', 'NeighborhoodsController@products');

    Route::get('mosques', 'MosquesController@index');
    Route::get('mosque/{id}', 'MosquesController@show');

    Route::get('companies', 'CompaniesController@index');
    Route::get('company/{id}', 'CompaniesController@show');
    Route::get('company/{id}/products', 'CompaniesController@products');

    Route::get('product/{id}', 'CompaniesController@product');
    Route::post('order', 'OrdersController@store');
});
