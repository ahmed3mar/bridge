const News = function () {

	let dataTable;

    function slider() {
        $('.addNewsImage').click(function () {
            $('#newsImages').append(`
            <tr>
                <td style="padding: 10px;">${$('#newsImages tr').length}</td>
                <td><input type="file" name="newsImages[]"></td>
                <td><a href="#" class="deleteNewsImage"><i class="fa fa-trash text-danger"></i></a></td>
            </tr>
            `)
            return false;
        });

        $(document).on('click', '.deleteNewsImage', function () {
            $(this).parent().parent().remove();
            return false;
        })


    }

	function init() {

    	slider()

		dataTable = $("#datatable").DataTable({
			"dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			"processing": true,
			"serverSide": true,
			// "deferLoading": $('#datatable').attr("data-total"),
			"ajax": {
				url: pageInfo.ajax,
				type: "POST",
				headers: {
					"X-CSRF-TOKEN": window.Laravel.csrfToken,
					"X-Requested-With": "XMLHttpRequest"
				}
			},
			"columns": [
				{ "data": "id"},
				{ "data": "image_url", width: "10%"},
				{ "data": "title", width: "50%"},
				{ "data": "options", searchable: false, orderable: false, "class": "text-center" }
			],
			"order" : [
				[0, "desc"]
			],
			"drawCallback": function(  ) {
				$("[data-toggle=tooltip],.tooltips").tooltip();
			}
		});

		$("#addNewItem").click(function () {
			News.add();
		});

		$(document).on("click", ".edit-item", function () {
			News.edit(this);
			return false;
		});

		$(document).on("click", ".delete-item", function () {
			News.delete(this);
			return false;
		});

        CKEDITOR.replace( 'content-editor' );
        CKEDITOR.instances['content-editor'].setData( $('#content').val() )

        CKEDITOR.replace( 'content_ar-editor' );
        CKEDITOR.instances['content_ar-editor'].setData( $('#content_ar').val() )

		if (!jQuery().validation) {
			return;
		}

		$("#item-form").validation({
			type: "normal"
		});

		if( $("#save-item").length )
		{
			$("#item-form").submit(function () {

				$(".has-error").removeClass("has-error");
				$(".submit-form").html("<i class=\"fa fa-spin fa-spinner\"></i> Save").prop("disabled", true);

				var data = new FormData();
				data.append("id", $("#id").val());
				data.append("type", $("#type").val());
				data.append("title", $("#title").val());
				data.append("title_ar", $("#title_ar").val());
				data.append("category_id", $("#category_id").val());
                data.append('content', CKEDITOR.instances['content-editor'].getData());
                data.append('content_ar', CKEDITOR.instances['content_ar-editor'].getData());

                $('[name="news_images[]"').each(function () {
                    data.append('images[]', $(this).val());
                })
                $('[name="newsImages[]"]').each(function () {
                    data.append('newsImages[]', $(this)[0].files[0]);
                })

				let req;
				if( $("#id").val() != "0" )
				{
					data.append("_method", "PUT");
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#id").val();
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "News updated successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				} else {
					let action = config.admin_url + "/" + pageInfo.slug;
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "News added successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				}

				req
					.catch( err => {
						if( err.response ) {
							let errors = err.response.data;
							parseErrors(errors);
						}
						$(".submit-form").html("Save").prop("disabled", false);
					});

				return false;
			});
		}

	}

	return {
		init: init,

		clear: function () {
			// clear the form inputs and textareas !
			$("#item-form").find("input, textarea").val("");

			// hide help-block and error messages
			$(".help-block").hide();
			$(".has-error").removeClass("has-error");

			$("#add-label").show();
			$("#edit-label").hide();

			// Remove preview image
			$("#newsImages").html("");
            $('#content-editor').html('')
            $('#content_ar-editor').html('')
            CKEDITOR.instances['content-editor'].setData( $('#content').val() )
            CKEDITOR.instances['content_ar-editor'].setData( $('#content_ar').val() )

			// reset back the input #id
			$("#id").val(0);
		},

		add: function () {

			News.clear();
			$("#addEditModal").modal("show");

		},

		edit: function (t) { // t == this

			News.clear();

			$("#edit-label").show();
			$("#add-label").hide();

			$(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						for( var i in response.data )
						{
							$("#" + i).val(response.data[i]).trigger("change");
						}

                        CKEDITOR.instances['content-editor'].setData( $('#content').val() )
                        CKEDITOR.instances['content_ar-editor'].setData( $('#content_ar').val() );

						const images = JSON.parse(response.data['images']);
						for( let i in images) {
							const image = images[i];
							const count = $('#newsImages tr').length + 1;
                            $('#newsImages').append(`
            <tr>
                                            <td style="padding: 10px;">${count}</td>
                                            <td>
                                                <input type="hidden" name="news_images[]" value="${image['image']}">
                                                <img style="width: 50px;" src="/image/news/${image['image']}" alt="">
                                            </td>
                                            <td><a href="#" class="deleteNewsImage"><i class="fa fa-trash text-danger"></i></a></td>
                                        </tr>
            `)
						}



						$("#addEditModal").modal("show");
					}
					$(t).html(icons.edit);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					$(t).html(icons.edit);
				});

		},

		delete: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.delete(config.admin_url + "/" + pageInfo.slug +"/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.delete);
							showToastr("success", "News deleted successfully");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		}

	};

}();

News.init();