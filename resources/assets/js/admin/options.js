const Options = function () {

    function slider() {
        $('.addSliderImage').click(function () {
            $('#sliderImages').append(`
            <tr>
                <td style="padding: 10px;">${$('#sliderImages tr').length}</td>
                <td><input type="file" name="sliderImages[]"></td>
                <td><input type="url" name="links[]"></td>
                <td><a href="#" class="deleteSliderImage"><i class="fa fa-trash text-danger"></i></a></td>
            </tr>
            `)
            return false;
        });

        $(document).on('click', '.deleteSliderImage', function () {
            $(this).parent().parent().remove();
            return false;
        })

        $('#options-slider').validation({type: 'normal'});

        $('#options-slider').submit(function () {

            $('.has-error').removeClass('has-error');
            $('.help-block:not(.keep)').hide();

            var data = new FormData();
            data.append('type', 'slider');
            // data.append('slider_top', $('#slider_top').val());
            // data.append('slider_bottom', $('#slider_bottom').val());
            $('[name="slider_images[]"').each(function () {
                data.append('images[]', $(this).val());
            })
            $('[name="sliderImages[]"]').each(function () {
                data.append('sliderImages[]', $(this)[0].files[0]);
            })
            $('[name="links[]"]').each(function () {
                data.append('links[]', $(this).val());
            })

            // So it's global form =D
            const req = axios.post( pageInfo.ajax , data)
                .then( res => {
                    showToastr('success', 'Options updated successfully');
                }).catch( err => {

                    if( err.response ) {
                        let errors = err.response.data;
                        if (err.response.status == 500) {
                            showToastr('error', 'Unknown error!');
                        } else {
                            for( let i in errors )
                            {
                                $('[name="' + i + '"]')
                                    .closest('.form-group').addClass('has-error').removeClass("has-info");
                                if( !$('#' + i).parent().find(".help-block").length )
                                {
                                    $('#' + i).parent().append('<span id="'+i+'-error" class="help-block help-block-error"></span>');
                                }
                                $('#' + i).parent().find(".help-block").html(errors[i]);
                            }
                        }
                    } else {
                        showToastr('error', 'Unknown error!');
                    }
                })

            return false;

        });
    }

	function init() {

    	slider();

		$("#submit-global").prop("disabled", false);
		$("#submit-close").prop("disabled", false);
		$("#submit-slider").prop("disabled", false);
		$("#submit-about").prop("disabled", false);
		$("#submit-social").prop("disabled", false);
		$("#submit-map").prop("disabled", false);

		$("[name=\"site_close\"]").change(function () {
			if ($(this).val() == 0) {
				$("#site_close_message").prop("disabled", true);
				$("#site_close_message_ar").prop("disabled", true);
			} else {
				$("#site_close_message").prop("disabled", false);
				$("#site_close_message_ar").prop("disabled", false);
			}
		});

		if ($("[name=\"site_close\"]:checked").val() == 0) {
			$("#site_close_message").prop("disabled", true);
			$("#site_close_message_ar").prop("disabled", true);
		}

		if (!jQuery().validation) {
			return;
		}

		// Init the validation
		$("#options-global").validation({type: "normal"});
		$("#options-close").validation({type: "normal"});
		$("#options-about").validation({type: "normal"});
		$("#options-social").validation({type: "normal"});
		$("#options-map").validation({type: "normal"});

		// On click event for the forms !
		$("#submit-global").click(() => {
			if ($("#options-global").validate().form()) {
				$("#options-global").submit(); //form validation success, call ajax form submit
			}
			return false;
		});

		$("#submit-close").click(() => {
			if ($("#options-close").validate().form()) {
				$("#options-close").submit(); //form validation success, call ajax form submit
			}
			return false;
		});

		$("#submit-social").click(() => {
			if ($("#options-social").validate().form()) {
				$("#options-social").submit(); //form validation success, call ajax form submit
			}
			return false;
		});

		$("#submit-about").click(() => {
			if ($("#options-about").validate().form()) {
				$("#options-about").submit(); //form validation success, call ajax form submit
			}
			return false;
		});

		$("#submit-map").click(() => {
			if ($("#options-map").validate().form()) {
				$("#options-map").submit(); //form validation success, call ajax form submit
			}
			return false;
		});

		$("#options-about").submit(function () {


			$(".has-error").removeClass("has-error");
			$(".help-block:not(.keep)").hide();

            var data = new FormData();
            data.append('type', 'about');
            data.append('about_title', $('#about_title').val());
            data.append('about_title_ar', $('#about_title_ar').val());
            data.append('about_content', $('#about_content').val());
            data.append('about_content_ar', $('#about_content_ar').val());
            data.append('about_file', document.getElementById('about_file').files[0]);

            console.log(pageInfo.ajax)
            console.log(data)
			// So it's global form =D
			const req = axios.post( pageInfo.ajax , data)
				.then( res => {
					showToastr("success", "Options updated successfully");
				}).catch( err => {

					if( err.response ) {
						let errors = err.response.data;
						if (err.response.status == 500) {
							showToastr("error", "Unknown error!");
						} else {
							for( let i in errors )
							{
								$("[name=\"" + i + "\"]")
									.closest(".form-group").addClass("has-error").removeClass("has-info");
								if( !$("#" + i).parent().find(".help-block").length )
								{
									$("#" + i).parent().append("<span id=\""+i+"-error\" class=\"help-block help-block-error\"></span>");
								}
								$("#" + i).parent().find(".help-block").html(errors[i]);
							}
						}
					} else {
						showToastr("error", "Unknown error!");
					}
				});

            return false;

		});

		$("#options-global,#options-close,#options-social,#options-map").submit(function () {

			$(".has-error").removeClass("has-error");
			$(".help-block:not(.keep)").hide();

			// So it's global form =D
			const req = axios.post( pageInfo.ajax , $(this).serialize())
				.then( res => {
					showToastr("success", "Options updated successfully");
				}).catch( err => {

					if( err.response ) {
						let errors = err.response.data;
						if (err.response.status == 500) {
							showToastr("error", "Unknown error!");
						} else {
							for( let i in errors )
							{
								$("[name=\"" + i + "\"]")
									.closest(".form-group").addClass("has-error").removeClass("has-info");
								if( !$("#" + i).parent().find(".help-block").length )
								{
									$("#" + i).parent().append("<span id=\""+i+"-error\" class=\"help-block help-block-error\"></span>");
								}
								$("#" + i).parent().find(".help-block").html(errors[i]);
							}
						}
					} else {
						showToastr("error", "Unknown error!");
					}
				});

		});

	}

	return {
		init: init,

	};

}();

Options.init();