var Contact = function () {

	var dataTable;

	function init() {

		dataTable = $("#datatable").DataTable({
			buttons: [
				"copy", "csv", "excel", "pdf", "print"
			],

			"dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			"processing": true,
			"serverSide": true,
			// "deferLoading": $('#datatable').attr("data-total"),
			"ajax": {
				url: pageInfo.ajax,
				type: "POST",
				headers: {
					"X-CSRF-TOKEN": window.Laravel.csrfToken,
					"X-Requested-With": "XMLHttpRequest"
				}
			},
			"columns": [
				{ "data": "id"},
				{ "data": "name", width: "20%"},
				{ "data": "phone", width: "10%"},
				{ "data": "email", width: "15%"},
				{ "data": "comment", width: "15%"},
				{ "data": "message", width: "15%"},
				{ "data": "created_at", width: "10%"},
				{ "data": "options", searchable: false, contactable: false, "class": "text-center" }
			],
			"contact" : [
				[0, "desc"]
			],
			"drawCallback": function( settings ) {
				$("[data-toggle=tooltip],.tooltips").tooltip();
			}
		});

		Vue.component("contact-filter", require("./components/ContactFilter.vue"));
		new Vue({
			el: "#vue-components"
		});

		$(document).on("click", "#filterIt", () => {

			const query = $("#query").val();
			const brand = $("#brand").val();
			const city = $("#city").val();
			const model = $("#model").val();
			const transmission_type = $("#transmission_type").val();
			const kilometer = $("#kilometer").val();
			const status = $("#status").val();
			const date_from = $("#date-from").val();
			const date_to = $("#date-to").val();

			let queries = "";

			if ( date_from )
				queries += "date_from=" + date_from + "&";

			if ( date_to )
				queries += "date_to=" + date_to + "&";

			dataTable.ajax.url(pageInfo.url + queries).load();

		});

		$(document).on("click", ".edit-comment", function () {
			Contact.comment(this);
			return false;
		});

		$("#save-comment").click(function () {
			const id = $("#id").val();
			axios.put( config.admin_url + "/" + pageInfo.slug + "/" + id, {
				type: "comment",
				comment: $("#comment").val()
			})
				.then( res => {
					showToastr("success", "contact updated successfully");
					dataTable.ajax.reload( null, false );
					$("#viewModal").modal("hide");
				});
			return false;
		});

		$(document).on("click", "#exportIt", function () {
			location.href = config.admin_url + "/" + pageInfo.slug + "/export?type=" + $("#type").val();
			return false;
		});

		$(document).on("click", ".view-contact", function () {
			Contact.view(this);
			$(".accept-contact-btn, .cancel-contact-btn").prop("disabled", false);
			return false;
		});

		$(document).on("click", ".accept-contact", function () {
			Contact.status(this, "accept");
			return false;
		});

		$(document).on("click", ".delete-contact", function () {
			Contact.delete(this);
			return false;
		});

		$(document).on("click", ".cancel-contact", function () {
			Contact.status(this, "cancel");
			return false;
		});

		$(document).on("click", ".accept-contact-btn", function () {
			Contact.status(this, "accept");
			return false;
		});

		$(document).on("click", ".cancel-contact-btn", function () {
			Contact.status(this, "cancel");
			return false;
		});

	}

	return {
		init: init,

		comment: function (t) {

			// $(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						$("#id").val(response.data["id"]);
						$("#comment").val(response.data["comment"]);
						for( var i in response.data )
						{
							$("td[data-id=\""+ i +"\"]").html(response.data[i]);
						}
						$(".accept-contact-btn").text("Accept");
						$(".cancel-contact-btn").text("Cancel");

						$(".accept-contact-btn, .cancel-contact-btn").data("id", response.data["id"]);

						if (response.data["status"] === "pending" ) {
							$(".accept-contact-btn, .cancel-contact-btn").show();
						} else {
							$(".accept-contact-btn, .cancel-contact-btn").hide();
						}

						$("#viewModal").modal("show");
					}
					// $(t).html(icons.view);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					// $(t).html(icons.view);
				});

		},

		view: function (t) {

			$(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						for( var i in response.data )
						{
							$("td[data-id=\""+ i +"\"]").html(response.data[i]);
						}
						$(".accept-contact-btn").text("Accept");
						$(".cancel-contact-btn").text("Cancel");

						$(".accept-contact-btn, .cancel-contact-btn").data("id", response.data["id"]);

						if (response.data["status"] === "pending" ) {
							$(".accept-contact-btn, .cancel-contact-btn").show();
						} else {
							$(".accept-contact-btn, .cancel-contact-btn").hide();
						}

						$("#infoModal").modal("show");
					}
					$(t).html(icons.view);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					$(t).html(icons.view);
				});

		},

		delete: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.delete(config.admin_url + "/" + pageInfo.slug +"/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.delete);
							showToastr("success", "Contact deleted successfully");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		},

		status: function (t, type = "accept") {

			if (type !== "accept" && type !== "cancel") return;

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.put(config.admin_url + "/" + pageInfo.slug +"/" + $(t).data("id"), {
						type
					})
						.then( res => {
							$(t).html(type === "accept" ? icons.check : icons.delete);
							showToastr("success", "Contact updated successfully");
							$("#viewModal").modal("hide");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(type === "accept" ? icons.check : icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		}

	};

}();

Contact.init();