const Category = function () {

	let dataTable;

	const generateItemHtml = (item) => {

		let itemHtml = `<li class="dd-item" data-id="${item.id}">
            <div class="dd-handle dd3-handle">Drag</div>
				<div class="dd3-image"><img class="dd-image" src="${item.image}" alt=""></div>
				<div class="dd3-content">${item.title}</div>
				<div class="dd3-options">
				<a href="#" data-id="${item.id}" class="text-success edit-item"><i class="fa fa-edit"></i></a>
					&nbsp;
				<a href="#" data-id="${item.id}" class="text-danger delete-item"><i class="fa fa-trash"></i></a>
        </div>`;

		if(item.subs.length) {
			itemHtml += `<ol class="dd-list">`;
			for( let x in item.subs) {
                itemHtml += generateItemHtml(item.subs[x]);
            }
			itemHtml += `</ol>`;
		}

		itemHtml += `</li>`;

        return itemHtml;
	}

	const getCategories = () => {
        axios.post(pageInfo.ajax)
            .then( res => {
                $('.dd-list').html('');
            	let html = "";
            	for( let x in res.data ) {
            		html += generateItemHtml(res.data[x])
				}
                $('.dd-list').append(html);
                $('.dd').nestable({ /* config options */ })
            });
	};

	const saveCategoriesOrder = () => {
        $('.dd').on('change', function() {
            axios.post(config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() + "/sort", {
            	categories: $('.dd').nestable('serialize')
			})
				.then(res => {
					console.log(res);
				})
        });
	}

	function init() {

        getCategories();
        saveCategoriesOrder();


		dataTable = $("#datatable").DataTable({
			"dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			"processing": true,
			"serverSide": true,
			// "deferLoading": $('#datatable').attr("data-total"),
			"ajax": {
				url: pageInfo.ajax,
				type: "POST",
				headers: {
					"X-CSRF-TOKEN": window.Laravel.csrfToken,
					"X-Requested-With": "XMLHttpRequest"
				}
			},
			"columns": [
				{ "data": "id"},
				{ "data": "title", width: "50%"},
				{ "data": "options", searchable: false, orderable: false, "class": "text-center" }
			],
			"order" : [
				[0, "desc"]
			],
			"drawCallback": function(  ) {
				$("[data-toggle=tooltip],.tooltips").tooltip();
			}
		});

		$("#addNewItem").click(function () {
			Category.add();
		});

		$(document).on("click", ".edit-item", function () {
			Category.edit(this);
			return false;
		});

		$(document).on("click", ".delete-item", function () {
			Category.delete(this);
			return false;
		});

		if (!jQuery().validation) {
			return;
		}

		$("#item-form").validation({
			type: "normal"
		});

		if( $("#save-item").length )
		{
			$("#item-form").submit(function () {

				$(".has-error").removeClass("has-error");
				$(".submit-form").html("<i class=\"fa fa-spin fa-spinner\"></i> Save").prop("disabled", true);

				var data = new FormData();
				data.append("id", $("#id").val());
				data.append("title", $("#title").val());
				data.append("title_ar", $("#title_ar").val());
                data.append('file', document.getElementById('file').files[0]);

				let req;
				if( $("#id").val() != "0" )
				{
					data.append("_method", "PUT");
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() + "/" + $("#id").val();
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "Category updated successfully");
                            getCategories();
							$(".submit-form").html("Save").prop("disabled", false);
						});
				} else {
                    data.append("type", $("#type").val());
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val();
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "Category added successfully");
                            getCategories();
							$(".submit-form").html("Save").prop("disabled", false);
						});
				}

				req
					.catch( err => {
						if( err.response ) {
							let errors = err.response.data;
							parseErrors(errors);
						}
						$(".submit-form").html("Save").prop("disabled", false);
					});

				return false;
			});
		}

	}

	return {
		init: init,

		clear: function () {
			// clear the form inputs and textareas !
			$("#item-form").find("input, textarea").val("");

			// hide help-block and error messages
			$(".help-block").hide();
			$(".has-error").removeClass("has-error");

			$("#add-label").show();
			$("#edit-label").hide();
			$('.only-edit').hide();

			// Remove preview image
			$("#image-preview").html("");

			// reset back the input #id
			$("#id").val(0);
		},

		add: function () {

			Category.clear();
			$("#addEditModal").modal("show");

		},

		edit: function (t) { // t == this

			Category.clear();

			$("#edit-label").show();
			$("#add-label").hide();
            $('.only-edit').show();

			$(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						for( var i in response.data )
						{
							if( i == 'file' ) continue;
							$("#" + i).val(response.data[i]).trigger("change");
						}
						const previewImage = $("<img>", {
							src: response.data["image_url"]
						});
						$("#image-preview").append(previewImage);
						$("#addEditModal").modal("show");
					}
					$(t).html(icons.edit);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					$(t).html(icons.edit);
				});

		},

		delete: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.delete(config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() +"/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.delete);
							showToastr("success", "Category deleted successfully");
                            getCategories();
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		}

	};

}();

Category.init();