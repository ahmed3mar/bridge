const Gallery = function () {

	let dataTable;

	function init() {

		dataTable = $("#datatable").DataTable({
			"dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			"processing": true,
			"serverSide": true,
			// "deferLoading": $('#datatable').attr("data-total"),
			"ajax": {
				url: pageInfo.ajax,
				type: "POST",
				headers: {
					"X-CSRF-TOKEN": window.Laravel.csrfToken,
					"X-Requested-With": "XMLHttpRequest"
				}
			},
			"columns": [
				{ "data": "id"},
				{ "data": "image_url", width: "20%"},
				{ "data": "title", width: "50%"},
				{ "data": "options", searchable: false, orderable: false, "class": "text-center" }
			],
			"order" : [
				[0, "desc"]
			],
			"drawCallback": function(  ) {
				$("[data-toggle=tooltip],.tooltips").tooltip();
			}
		});

		$("#addNewItem").click(function () {
			Gallery.add();
		});

		$(document).on("click", ".edit-item", function () {
			Gallery.edit(this);
			return false;
		});

		$(document).on("click", ".delete-item", function () {
			Gallery.delete(this);
			return false;
		});

		if (!jQuery().validation) {
			return;
		}

		$("#item-form").validation({
			type: "normal"
		});

		if( $("#save-item").length )
		{
			$("#item-form").submit(function () {

				$(".has-error").removeClass("has-error");
				$(".submit-form").html("<i class=\"fa fa-spin fa-spinner\"></i> Save").prop("disabled", true);

				var data = new FormData();
				data.append("id", $("#id").val());
                data.append("type", $("#type").val());
                data.append("category_id", $("#category_id").val());
				data.append("title", $("#title").val());
				data.append("url", $("#url").val());
                if($('#type').val() == 'images')
                data.append("file", document.getElementById("file").files[0]);

				let req;
				if( $("#id").val() != "0" )
				{
					data.append("_method", "PUT");
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() + "/" + $("#id").val();
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "Gallery updated successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				} else {
                    data.append("type", $("#type").val());
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val();
					req = axios.post(action, data)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "Gallery added successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				}

				req
					.catch( err => {
						if( err.response ) {
							let errors = err.response.data;
							parseErrors(errors);
						}
						$(".submit-form").html("Save").prop("disabled", false);
					});

				return false;
			});
		}

	}

	return {
		init: init,

		clear: function () {
			// clear the form inputs and textareas !
			$("#item-form").find("input, textarea").val("");

			// hide help-block and error messages
			$(".help-block").hide();
			$(".has-error").removeClass("has-error");

			$("#add-label").show();
			$("#edit-label").hide();

			// Remove preview image
			$("#image-preview").html("");

			// reset back the input #id
			$("#id").val(0);
		},

		add: function () {

			Gallery.clear();
			$("#addEditModal").modal("show");

		},

		edit: function (t) { // t == this

			Gallery.clear();

			$("#edit-label").show();
			$("#add-label").hide();

			$(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						for( var i in response.data )
						{
							$("#" + i).val(response.data[i]).trigger("change");
						}
						const previewImage = $("<img>", {
							src: response.data["image_url"]
						});
						$("#image-preview").append(previewImage);
						$("#addEditModal").modal("show");
					}
					$(t).html(icons.edit);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					$(t).html(icons.edit);
				});

		},

		delete: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.delete(config.admin_url + "/" + pageInfo.slug + "/" + $("#type").val() +"/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.delete);
							showToastr("success", "Gallery deleted successfully");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		}

	};

}();

Gallery.init();