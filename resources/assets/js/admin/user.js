var User = function () {

	var dataTable;

	function init() {

		dataTable = $("#datatable").DataTable({
			"dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
			"processing": true,
			"serverSide": true,
			// "deferLoading": $('#datatable').attr("data-total"),
			"ajax": {
				url: pageInfo.ajax,
				type: "POST",
				headers: {
					"X-CSRF-TOKEN": window.Laravel.csrfToken,
					"X-Requested-With": "XMLHttpRequest"
				}
			},
			"columns": [
				{ "data": "id"},
				{ "data": "email", width: "25%"},
				{ "data": "name", width: "25%"},
				{ "data": "is_admin", width: "10%"},
				// { "data": "phone", width: "10%"},
				// { "data": "status", width: "10%"},
				{ "data": "created_at", width: "20%"},
				{ "data": "options", searchable: false, orderable: false, "class": "text-center" }
			],
			"order" : [
				[0, "desc"]
			],
			"drawCallback": function( settings ) {
				$("[data-toggle=tooltip],.tooltips").tooltip();
			}
		});

		$("#addNewItem").click(function () {
			User.add();
		});

		$("#is_admin").on("change", function () {
			if ($(this).is(":checked")) {
				$(".permissions-section").slideDown();
			} else {
				$(".permissions-section").slideUp();
			}
		});

		$(document).on("click", ".edit-item", function () {
			User.edit(this);
			return false;
		});

		$(document).on("click", ".block-user", function () {
			User.block(this);
			return false;
		});

		$(document).on("click", ".delete-item", function () {
			User.delete(this);
			return false;
		});

		if (!jQuery().validation) {
			return;
		}

		$("#item-form").validation({
			type: "normal"
		});

		if( $("#save-item").length )
		{
			$("#item-form").submit(function () {

				$(".submit-form").html("<i class=\"fa fa-spin fa-spinner\"></i> Save").prop("disabled", true);

				$(".has-error").removeClass("has-error");

				var form = this;
				var data = new FormData( $(this).serialize() );

				let arr = $(this).serializeArray();
				var formData = new FormData();
				arr.map(s => {
					formData.append(s.name, s.value);
				});

				// formData.append("image", document.getElementById("image_file").files[0]);


				let req;
				if( $("#id").val() != "0" )
				{
					formData.append("_method", "PUT");
					let action = config.admin_url + "/" + pageInfo.slug + "/" + $("#id").val();
					req = axios.post(action, formData)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "User updated successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				} else {
					let action = config.admin_url + "/" + pageInfo.slug;
					req = axios.post(action, formData)
						.then( res => {
							$("#addEditModal").modal("hide");
							showToastr("success", "User added successfully");
							dataTable.ajax.reload( null, false );
							$(".submit-form").html("Save").prop("disabled", false);
						});
				}

				req
					.catch( err => {
						if( err.response ) {
							let errors = err.response.data;
							parseErrors(errors);
						}
						$(".submit-form").html("Save").prop("disabled", false);
					});

				return false;
			});
		}

	}

	return {
		init: init,

		clear: function () {
			// clear the form inputs and textareas !
			$("#item-form").find("input, textarea").val("");

			// hide help-block and error messages
			$(".help-block").hide();
			$(".has-error").removeClass("has-error");

			$("input[type=\"checkbox\"]").prop("checked", false).trigger("change");

			$("#add-label").show();
			$("#edit-label").hide();
			// Remove preview image
			$("#image-preview").html("");

			// reset back the input #id
			$("#id").val(0);
		},

		add: function () {

			User.clear();
			$(".add-only").show();
			$("#addEditModal").modal("show");

		},

		edit: function (t) { // t == this

			User.clear();
			$(".add-only").hide();

			$("#edit-label").show();
			$("#add-label").hide();

			$(t).html(icons.loading);

			axios.get( config.admin_url + "/" + pageInfo.slug + "/" + $(t).data("id") )
				.then(res => {
					const response = res.data;
					if( response && response.success )
					{
						for( var i in response.data )
						{
							if ( i == "image_file" ||  i == "is_admin" || i == "permissions" || i == "all_permissions" ) continue;
							$("#" + i).val(response.data[i]);
						}
						const previewImage = $("<img>", {
							src: response.data["image_url"]
						});
						$("#image-preview").append(previewImage);

						const allPermissions = response.data["all_permissions"];
						for( let x in allPermissions ) {
							$("#permission-" + allPermissions[x]).prop("checked", true);
						}
						if (response.data["is_admin"] == "1") {
							$("#is_admin").prop("checked", true).trigger("change");
						}
						$("#addEditModal").modal("show");
					}
					$(t).html(icons.edit);
				})
				.catch( err => {
					if( err.response ) {
						parseErrors(err.response.data);
					}
					$(t).html(icons.edit);
				});

		},

		delete: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.delete(config.admin_url + "/" + pageInfo.slug +"/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.delete);
							showToastr("success", "User deleted successfully");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.delete);
						});

					return false;
				}
			}).confirmation("show");

		},

		block: function (t) {

			$(t).confirmation({
				href: "javascript:;",
				onConfirm: function () {

					$(t).html(icons.loading);

					axios.put(config.admin_url + "/" + pageInfo.slug +"/block/" + $(t).data("id"))
						.then( res => {
							$(t).html(icons.ban);
							showToastr("success", "User blocked successfully");
							dataTable.ajax.reload( null, false );
						})
						.catch( err => {
							if( err.response ) {
								parseErrors(err.response.data);
							}
							$(t).html(icons.ban);
						});

					return false;
				}
			}).confirmation("show");

		}

	};

}();

User.init();
