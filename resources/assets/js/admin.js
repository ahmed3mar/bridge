const pageInfo = window.pageInfo;
const current = pageInfo ? pageInfo.slug : "home";
window.axios = require("axios");

window.icons = {
	loading: "<i class=\"fa fa-spin fa-spinner\"></i>",
	edit: "<i class=\"fa fa-1-8x fa-edit\"></i>",
	delete: "<i class=\"fa fa-trash fa-1-8x text-danger\"></i>",
	ban: "<i class=\"fa fa-ban fa-1-8x\"></i>",
	view: "<i class=\"fa fa-eye fa-1-8x\"></i>",
	check: "<i class=\"fa fa-check fa-1-8x\"></i>",
};

function showToastr( type, message ) {
	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-left",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	toastr[type](message);
}
window.showToastr = showToastr;

window.parseErrors = function(errors) {
	if(errors.errors) errors = errors.errors;
	for( let i in errors )
	{
		$("[name=\"" + i + "\"]")
			.closest(".form-group").addClass("has-error").removeClass("has-info");
		if( !$("#" + i).parent().find(".help-block").length )
		{
			$("#" + i).parent().append("<span id=\""+i+"-error\" class=\"help-block help-block-error\"></span>");
		}
		$("#" + i).parent().find(".help-block").html(errors[i]);
		$(".help-block").show();
	}

	if (errors.message) {
		if( errors.message == "login" )
		{
			location.reload();
			return false;
		} else if (errors.message == "no-permission") {
			return showToastr("error", "You don't have a permission");
		} else {
			showToastr("error", errors.message);
		}
	}

};

$("[data-toggle=tooltip],.tooltips").tooltip();

window.Vue = require("vue");

switch (current) {
case "users":
	require("./admin/user");
	break;
case "options":
	require("./admin/options");
	break;
case "services":
	require("./admin/service");
	break;
case "categories":
	require("./admin/category");
	break;
case "projects":
	require("./admin/project");
	break;
case "gallery":
	require("./admin/gallery");
	break;
case "pages":
	require("./admin/page");
	break;
case "news":
	require("./admin/news");
	break;

    case 'translations':
        require('./admin/translation');
        break;
    // case 'contacts':
    //     require('./admin/contact');
    //     break;
    // case 'cities':
    //     require('./admin/city');
    //     break;
    // case 'neighborhoods':
    //     require('./admin/neighborhood');
    //     break;
    // case 'mosques':
    //     require('./admin/mosque');
    //     break;
    // case 'companies':
    //     require('./admin/company');
    //     break;
    // case 'products':
    //     require('./admin/product');
    //     break;
}