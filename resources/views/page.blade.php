@extends('layouts.app')
@section('body_class') blog @stop

@section('content')

    <section class="" style="margin: 0">
        <div class="container">
            <div class="row headline">
                <h1>{{ $page->{_trans('title')} }}</h1>
                <hr />
            </div>

            <div class="content col-xs-11">
                <p>{!! $page->{_trans('content')} !!}</p>
            </div>
        </div>

    </section>

@endsection
