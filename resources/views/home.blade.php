@extends('layouts.app')

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <script>
        // $("a.fancy").fancybox({
        //     'titleShow'     : false,
        //     'transitionIn'	: 'elastic',
        //     'transitionOut'	: 'elastic',
        //     'easingIn'      : 'easeOutBack',
        //     'easingOut'     : 'easeInBack'
        // });

        $('.gallery-item').click(function (e) {
            if(e.target.className != 'btn') {
                var img = $(this).find('img');
                $.fancybox.open({
                    src: img.attr('src'),
                });
                return false;
            }
        })



    </script>
    <script>
        $('#request_unit').submit(function () {

            $('#request_btn').html('<i class="fa fa-spin fa-spinner"></i> {{ trans('home.request_unit') }}');

            $.ajax({
                url: '{{ route('order.send') }}',
                data: $(this).serialize(),
                success: function (response) {

                },
                error: function (xhr) {

                },
                type: 'post',
                dataType: 'json',
            })

            return false;
        })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                rtl: {{ app()->getLocale() == 'ar' ? 'true' : 'false' }},
                loop:true,
                margin:10,
                nav:true,

                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:3,
                        nav:false
                    },
                    1000:{
                        items:4,
                        nav:true,
                        loop:false
                    }
                }
            });
        });
    </script>
@stop

@section('after_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
@stop

@section('content')

    <section class="services container" id="services">
        <div class="row">
            <div class="col-lg-12 headline">
                <h1>Our services</h1>
                <hr />
            </div>
        </div>
        <div class="row">

            <div id="carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->


                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    @foreach($services as $service)
                        <div class="item @if($loop->index === 0) active @endif()">
                            <div class="col-lg-4 col-lg-push-7 col-lg-offset-1 title">

                                <div class="stroke"></div>
                                <div class="icon">
                                    <i class="fa fa-{{ $service->icon }}"></i>
                                </div>
                            </div>
                            <div class="col-lg-7 col-lg-pull-5 description">
                                <h4>{{ $service->{_trans('title')} }}</h4>
                                <p>{!! nl2br($service->{_trans('description')}) !!}</p>
                            </div>

                        </div>
                    @endforeach
                </div>


                <div class="container">
                    <div class="row controller">
                        <hr />
                        <div class="col-lg-12 ">
                            <ul class="carousel-indicators">
                                @foreach($services as $service)
                                    <li data-target="#carousel" data-slide-to="{{ $loop->index }}" class="@if($loop->index === 0) active @endif()"><span class="number">{{ $loop->index + 1 }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="#gallery" id="projects">
        <div class="container">
            <div class="row headline">
                <h1>{{ trans('home.our_projects') }}</h1>
                <hr />
            </div>
        </div>

        <div class="container">

            <style>
                .category {
                    position: relative;
                    display: block;
                    -webkit-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.01);
                    -moz-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.01);
                    box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.01);

                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                    -ms-transition: all 0.5s;
                    -o-transition: all 0.5s;
                    transition: all 0.5s;
                }
                .category:hover {
                    -webkit-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.25);
                    -moz-box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.25);
                    box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.25);
                }

                .category img.img {
                    width: 100%;
                    height: 200px;
                }

                .category h2 {
                    position: absolute;
                    bottom: 0;
                    font-size: 18px;
                    padding: 10px;
                    background: #0000006e;
                    left: 0;
                    right: 0;
                    color: white;
                    text-align: center;
                }
            </style>

            <div class="row">
                @foreach($main_categories as $category)
                    <div class="col-md-4">
                        <a href="{{ $category->url }}" class="category">
                            <img class="img" src="{{ $category->image_url }}" alt="">
                            <h2>{{ $category->{_trans('title')} }}</h2>
                        </a>
                    </div>
                @endforeach
            </div>

        </div>

        {{--<div class="controller">--}}
            {{--<div class="gallery-prev">--}}
                {{--<i class="fa fa-angle-left"></i>--}}
            {{--</div>--}}
            {{--<div class="gallery-next">--}}
                {{--Loading <strong>8</strong>/<strong>16</strong>--}}
                {{--<i class="fa fa-angle-right"></i>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="swiper-container" style="">--}}
            {{--<div class="swiper-wrapper">--}}

                {{--@foreach ($projects->chunk(8) as $chunk)--}}
                {{--<div class="swiper-slide">--}}
                    {{--@foreach ($chunk as $project)--}}
                        {{--@include('common.project', $project)--}}
                    {{--@endforeach--}}
                {{--</div>--}}
                {{--@endforeach--}}

            {{--</div>--}}
        {{--</div>--}}
        <div class="clearfix"></div>
        <div style="clear:both"></div>
    </section>

    <section class="features scrollAnimation" id="features">
        <div class="container">
            <div class="row headline">
                <h1>{{ trans('home.Why choose Bridge') }}</h1>
                <hr />
            </div>

            <div class="col-md-6 description " >
                <h1>{{ option(_trans('about_title')) }}</h1>
                <p>{!! nl2br(option(_trans('about_content'))) !!}</p>
                {{--<a href="http://themeforest.net/item/one-html-template/7974440" class="btn tour hidden-sm hidden-xs">Take a tour!</a>--}}
                {{--<a href="http://themeforest.net/item/one-html-template/7974440" class="btn buy hidden-sm hidden-xs">Buy it now</a>--}}
            </div>
            <div class="col-md-6 ipads">
                <img class="ipad black " src="{{ route('image', ['image', option('about_image')]) }}" alt="ipad">
                {{--<img class="ipad white " src="img/ipad_white.png" alt="ipad">--}}
            </div>
            {{--<div class="col-md-12">--}}
                {{--<a href="http://themeforest.net/item/one-html-template/7974440" class="btn tour visible-sm visible-xs">Take a tour!</a>--}}
                {{--<a href="http://themeforest.net/item/one-html-template/7974440" class="btn buy visible-sm visible-xs">Buy it now</a>--}}
            {{--</div>--}}
        </div>
    </section>


    <section class="gallery" id="gallery">
        <div class="container">
            <div class="row headline">
                <h1>{{ trans('home.gallery') }}</h1>
                <hr />
            </div>
        </div>
        <div class="controller">
            <div class="gallery-prev">
                <i class="fa fa-angle-left"></i>
            </div>
            <div class="gallery-next">
                {{--Loading <strong>8</strong>/<strong>16</strong>--}}
                <i class="fa fa-angle-right"></i>
            </div>
        </div>
        <div class="swiper-container" style="">
            <div class="swiper-wrapper">

                @foreach ($gallery->chunk(8) as $chunk)
                    <div class="swiper-slide">
                        @foreach ($chunk as $media)
                            @include('common.gallery', $media)
                        @endforeach
                    </div>
                @endforeach

            </div>
        </div>
        <div class="clearfix"></div>
        <div style="clear:both"></div>
    </section>

    <section class="blog" id="news">
        <div class="container">
            <div class="row headline">
                <h1>{{ trans('home.last_news') }}</h1>
                <hr />
            </div>
            <div class="#row">
                <!-- Set up your HTML -->
                <div class="owl-carousel">
                    @foreach ($news as $n)
                        <article class="item #col-md-4">
                            <figure>
                                <img src="{{ $n->image_url }}"  alt="img" />
                                <figurecaption>
                                    <a class="btn" href="{{ $n->url }}"><i class="fa fa-camera"></i>{{ trans('home.read_more') }}</a>
                                </figurecaption>
                            </figure>
                            <h3>{{ $n->{_trans('title')} }}</h3>
                            <hr>
                            <p>{{ $n->{_trans('description')} }}</p>
                        </article>
                    @endforeach
                </div>



                {{--@foreach($news as $n)--}}
                    {{--<article class="col-md-4">--}}
                        {{--<figure>--}}
                            {{--<img src="{{ $n->image_url }}"  alt="img" />--}}
                            {{--<figurecaption>--}}
                                {{--<a class="btn" href="{{ $n->url }}"><i class="fa fa-camera"></i>{{ trans('home.read_more') }}</a>--}}
                            {{--</figurecaption>--}}
                        {{--</figure>--}}
                        {{--<h3>{{ $n->{_trans('title')} }}</h3>--}}
                        {{--<hr>--}}
                        {{--<p>{{ $n->{_trans('description')} }}</p>--}}
                    {{--</article>--}}
                {{--@endforeach--}}
            </div>
            <div class="col-sm-12 morepostscontainer">
                <a href="{{ route('news') }}" class="btn moreposts">{{ trans('home.view_all_news') }}</a>
            </div>
        </div>
    </section>
    <section class="contact" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4>{!! nl2br(trans('home.contact_title')) !!}</h4>
                    <p>{!! nl2br(trans('home.contact_info')) !!}</p>
                </div>
                <div class="col-md-8">
                    <h4>{!! nl2br(trans('home.contact_form')) !!}</h4>

                    <form method="post" id="request_unit">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="{{ trans('home.name') }}">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="{{ trans('home.email') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="{{ trans('home.phone') }}">
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" id="selectSubject"  name="type" aria-invalid="false">
                                    <optgroup label="Elshrouk">
                                        <option value="Elshrouk- Duplex">Elshrouk- Duplex</option>
                                        <option value="Elshrouk- Apartment">Elshrouk- Apartment</option>
                                    </optgroup>
                                    <optgroup label="Heliopolis">
                                        <option value="Heliopolis - Twin House">Heliopolis - Twin House</option>
                                        <option value="Heliopolis - Town House">Heliopolis - Town House</option>
                                        <option value="Heliopolis - Duplex">Heliopolis - Duplex</option>
                                        <option value="Heliopolis - Apartment">Heliopolis - Apartment</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea class="form-control" placeholder="{{ trans('home.message') }}"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-lg-offset-9">
                                <button class="form-control" id="request_btn" type="submit">{{ trans('home.request_unit') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="map noMargin" id="map" data-lat="{{ option('map_lat') }}" data-lng="{{ option('map_lng') }}">
        <div id="map-canvas" style="height:550px; width:100%;"></div>
        <div class="infobox-wrapper">
            <div id="infobox">
                <h5>{!! nl2br(trans('home.map_title')) !!}</h5>
                {!! nl2br(trans('home.map_content')) !!}
            </div>
        </div>
    </section>

@endsection
