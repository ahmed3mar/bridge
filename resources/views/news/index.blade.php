@extends('layouts.app')

@section('body_class') blog @stop

@section('content')

    <section class=" container">
        <div class="row">
            <div class="col-lg-12 headline">
                <h1>{{ trans('news.last_news') }}</h1>
                <hr />
            </div>
        </div>

        <div class="row">

            <div class="col-md-9">
                @foreach($news as $n)
                <article class="row">
                    <aside class="col-xs-1">
                        <div class="box date">
                            <div class="day">
                                {{ $n->created_at->format('d') }}
                            </div>
                            <div class="rest">
                                {{ $n->created_at->format('M, Y') }}
                            </div>
                        </div>

                    </aside>
                    <div class="content col-xs-11">
                        <div class="col-xs-4">
                            <img src="{{ $n->image_url }}" >
                        </div>
                        <div class="col-xs-8">
                            <h3>{{ $n->{_trans('title')} }}</h3>
                            <p>{{ str_limit(strip_tags($n->{_trans('content')})) }}</p>
                            <a href="{{ $n->url }}" class="btn"><i class="fa fa-bars"></i>Read more</a>
                        </div>
                    </div>
                </article>
                @endforeach
            </div>

            <aside class="col-md-3">

                {{--<section class="popular">--}}
                    {{--<h4>Popular posts</h4>--}}
                    {{--@foreach($related_news as $related_new)--}}
                    {{--<article class="row">--}}
                        {{--<div class="col-md-4 col-xs-2">--}}
                            {{--<div class="photo"><img src="{{ $related_new->image_url }}" alt="thumbnail"></div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-8 col-xs-10">--}}
                            {{--<h5>{{ $related_new->{_trans('title')} }}</h5>--}}
                            {{--<aside>{{ $related_new->created_at->format('M d, Y') }}</aside>--}}
                            {{--<aside class="comments"><i class="fa fa-comments"></i> 23 comments</aside>--}}
                        {{--</div>--}}
                    {{--</article>--}}
                    {{--@endforeach--}}
                {{--</section>--}}

                {{--<section class="categories">--}}
                    {{--<h4>Categories</h4>--}}

                    {{--<div class="row">--}}
                        {{--<ul class="col-sm-12">--}}
                            {{--@foreach($categories as $category)--}}
                            {{--<li>--}}
                                {{--<a href="{{ $category->url }}">{{ $category->{_trans('title')} }} <span class="amount">({{ $category->count_news }})</span></a>--}}
                            {{--</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</section>--}}


            </aside>

        </div>


    </section>


@endsection
