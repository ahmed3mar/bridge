@extends('admin.layouts.app')

@section('after_scripts')
    {{--<!-- ChartJS -->--}}
    {{--<script src="{{ asset ("/backend/vendor/AdminLTE/plugins/chartjs/Chart.min.js") }}"></script>--}}

    {{--<script>--}}
        {{--//----------------}}
        {{--//- AREA CHART ---}}
        {{--//----------------}}

        {{--// Get context with jQuery - using jQuery's .get() method.--}}
        {{--var areaChartCanvas = $('#ordersChart').get(0).getContext('2d')--}}
        {{--// This will get the first returned node in the jQuery collection.--}}
        {{--var areaChart       = new Chart(areaChartCanvas)--}}

        {{--var areaChartData = {--}}
            {{--labels  : ['{!! implode("', '", array_keys($ordersChart)) !!}'], //'January', 'February', 'March', 'April', 'May', 'June', 'July'],--}}
            {{--datasets: [--}}
                {{--{--}}
                    {{--label               : 'Orders',--}}
                    {{--fillColor           : 'rgba(60,141,188,0.9)',--}}
                    {{--strokeColor         : 'rgba(60,141,188,0.8)',--}}
                    {{--pointColor          : '#3b8bba',--}}
                    {{--pointStrokeColor    : 'rgba(60,141,188,1)',--}}
                    {{--pointHighlightFill  : '#fff',--}}
                    {{--pointHighlightStroke: 'rgba(60,141,188,1)',--}}
                    {{--data                : [{!! implode(", ", array_values($ordersChart)) !!}]--}}
                {{--}--}}
            {{--]--}}
        {{--}--}}

        {{--var areaChartOptions = {--}}
            {{--//Boolean - If we should show the scale at all--}}
            {{--showScale               : true,--}}
            {{--//Boolean - Whether grid lines are shown across the chart--}}
            {{--scaleShowGridLines      : false,--}}
            {{--//String - Colour of the grid lines--}}
            {{--scaleGridLineColor      : 'rgba(0,0,0,.05)',--}}
            {{--//Number - Width of the grid lines--}}
            {{--scaleGridLineWidth      : 1,--}}
            {{--//Boolean - Whether to show horizontal lines (except X axis)--}}
            {{--scaleShowHorizontalLines: true,--}}
            {{--//Boolean - Whether to show vertical lines (except Y axis)--}}
            {{--scaleShowVerticalLines  : true,--}}
            {{--//Boolean - Whether the line is curved between points--}}
            {{--bezierCurve             : true,--}}
            {{--//Number - Tension of the bezier curve between points--}}
            {{--bezierCurveTension      : 0.3,--}}
            {{--//Boolean - Whether to show a dot for each point--}}
            {{--pointDot                : false,--}}
            {{--//Number - Radius of each point dot in pixels--}}
            {{--pointDotRadius          : 4,--}}
            {{--//Number - Pixel width of point dot stroke--}}
            {{--pointDotStrokeWidth     : 1,--}}
            {{--//Number - amount extra to add to the radius to cater for hit detection outside the drawn point--}}
            {{--pointHitDetectionRadius : 20,--}}
            {{--//Boolean - Whether to show a stroke for datasets--}}
            {{--datasetStroke           : true,--}}
            {{--//Number - Pixel width of dataset stroke--}}
            {{--datasetStrokeWidth      : 2,--}}
            {{--//Boolean - Whether to fill the dataset with a color--}}
            {{--datasetFill             : true,--}}
            {{--//String - A legend template--}}
            {{--legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',--}}
            {{--//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container--}}
            {{--maintainAspectRatio     : true,--}}
            {{--//Boolean - whether to make the chart responsive to window resizing--}}
            {{--responsive              : true--}}
        {{--}--}}

        {{--//Create the line chart--}}
        {{--areaChart.Line(areaChartData, areaChartOptions)--}}

    {{--</script>--}}
    {{--<script>--}}
        {{--//----------------}}
        {{--//- AREA CHART ---}}
        {{--//----------------}}

        {{--// Get context with jQuery - using jQuery's .get() method.--}}
        {{--var areaChartCanvas = $('#phoneChart').get(0).getContext('2d')--}}
        {{--// This will get the first returned node in the jQuery collection.--}}
        {{--var areaChart       = new Chart(areaChartCanvas)--}}

        {{--var areaChartData = {--}}
            {{--labels  : ['{!! implode("', '", array_keys($phoneClicks)) !!}'], //'January', 'February', 'March', 'April', 'May', 'June', 'July'],--}}
            {{--datasets: [--}}
                {{--{--}}
                    {{--label               : 'Orders',--}}
                    {{--fillColor           : 'rgba(60,141,188,0.9)',--}}
                    {{--strokeColor         : 'rgba(60,141,188,0.8)',--}}
                    {{--pointColor          : '#3b8bba',--}}
                    {{--pointStrokeColor    : 'rgba(60,141,188,1)',--}}
                    {{--pointHighlightFill  : '#fff',--}}
                    {{--pointHighlightStroke: 'rgba(60,141,188,1)',--}}
                    {{--data                : [{!! implode(", ", array_values($phoneClicks)) !!}]--}}
                {{--}--}}
            {{--]--}}
        {{--}--}}

        {{--var areaChartOptions = {--}}
            {{--//Boolean - If we should show the scale at all--}}
            {{--showScale               : true,--}}
            {{--//Boolean - Whether grid lines are shown across the chart--}}
            {{--scaleShowGridLines      : false,--}}
            {{--//String - Colour of the grid lines--}}
            {{--scaleGridLineColor      : 'rgba(0,0,0,.05)',--}}
            {{--//Number - Width of the grid lines--}}
            {{--scaleGridLineWidth      : 1,--}}
            {{--//Boolean - Whether to show horizontal lines (except X axis)--}}
            {{--scaleShowHorizontalLines: true,--}}
            {{--//Boolean - Whether to show vertical lines (except Y axis)--}}
            {{--scaleShowVerticalLines  : true,--}}
            {{--//Boolean - Whether the line is curved between points--}}
            {{--bezierCurve             : true,--}}
            {{--//Number - Tension of the bezier curve between points--}}
            {{--bezierCurveTension      : 0.3,--}}
            {{--//Boolean - Whether to show a dot for each point--}}
            {{--pointDot                : false,--}}
            {{--//Number - Radius of each point dot in pixels--}}
            {{--pointDotRadius          : 4,--}}
            {{--//Number - Pixel width of point dot stroke--}}
            {{--pointDotStrokeWidth     : 1,--}}
            {{--//Number - amount extra to add to the radius to cater for hit detection outside the drawn point--}}
            {{--pointHitDetectionRadius : 20,--}}
            {{--//Boolean - Whether to show a stroke for datasets--}}
            {{--datasetStroke           : true,--}}
            {{--//Number - Pixel width of dataset stroke--}}
            {{--datasetStrokeWidth      : 2,--}}
            {{--//Boolean - Whether to fill the dataset with a color--}}
            {{--datasetFill             : true,--}}
            {{--//String - A legend template--}}
            {{--legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',--}}
            {{--//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container--}}
            {{--maintainAspectRatio     : true,--}}
            {{--//Boolean - whether to make the chart responsive to window resizing--}}
            {{--responsive              : true--}}
        {{--}--}}

        {{--//Create the line chart--}}
        {{--areaChart.Line(areaChartData, areaChartOptions)--}}

    {{--</script>--}}

@endsection

@section('container')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    HOME

    <!-- Main content -->
    {{--<section class="content">--}}
        {{--<!-- Small boxes (Stat box) -->--}}
        {{--<div class="row">--}}
            {{--<div class="col-lg-4 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-aqua">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{ $count['orders']['pending'] }}<small style="font-size: 16px; color: white;"> of {{ $count['orders']['total'] }} total</small></h3>--}}
                        {{--<p>New Orders</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ route('adminorders.index') . '?status=pending' }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-4 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-green">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{ $count['ads']['pending'] }}<sup style="font-size: 20px"> of {{ $count['ads']['total'] }} total</sup></h3>--}}
                        {{--<p>Pending Ads</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-stats-bars"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ route('adminads.index') . '?status=pending' }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-4 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-yellow">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{ $count['users']['total'] }}</h3>--}}
                        {{--<p>User Registrations</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-person-add"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ route('adminusers.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.row -->--}}

        {{--<!-- Info boxes -->--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                {{--<div class="info-box">--}}
                    {{--<span class="info-box-icon bg-aqua"><i class="ion ion-eye"></i></span>--}}

                    {{--<div class="info-box-content">--}}
                        {{--<span class="info-box-text">Ads views</span>--}}
                        {{--<span class="info-box-number">{{ $count['views']['total'] }}</span>--}}
                    {{--</div>--}}
                    {{--<!-- /.info-box-content -->--}}
                {{--</div>--}}
                {{--<!-- /.info-box -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
            {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                {{--<div class="info-box">--}}
                    {{--<span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span>--}}

                    {{--<div class="info-box-content">--}}
                        {{--<span class="info-box-text">Total Reports</span>--}}
                        {{--<span class="info-box-number">{{ $count['reports']['total'] }}</span>--}}
                        {{--<span class="info-box-text"><strong>{{ $count['reports']['today'] }}</strong> added today</span>--}}
                    {{--</div>--}}
                    {{--<!-- /.info-box-content -->--}}
                {{--</div>--}}
                {{--<!-- /.info-box -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}

            {{--<!-- fix for small devices only -->--}}
            {{--<div class="clearfix visible-sm-block"></div>--}}

            {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                {{--<div class="info-box">--}}
                    {{--<span class="info-box-icon bg-green"><i class="ion ion-iphone"></i></span>--}}

                    {{--<div class="info-box-content">--}}
                        {{--<span class="info-box-text">Phone clicks</span>--}}
                        {{--<span class="info-box-number">{{ $count['phone']['total'] }}, and {{ $count['phone']['today'] }} today</span>--}}
                    {{--</div>--}}
                    {{--<!-- /.info-box-content -->--}}
                {{--</div>--}}
                {{--<!-- /.info-box -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}

            {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                {{--<div class="info-box">--}}
                    {{--<span class="info-box-icon bg-green"><i class="ion ion-iphone"></i></span>--}}

                    {{--<div class="info-box-content">--}}
                        {{--<span class="info-box-text">Add new ad page</span>--}}
                        {{--<span class="info-box-number">{{ option('create_ad') }}</span>--}}
                    {{--</div>--}}
                    {{--<!-- /.info-box-content -->--}}
                {{--</div>--}}
                {{--<!-- /.info-box -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
        {{--</div>--}}
        {{--<!-- /.row -->--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-6">--}}
                {{--<!-- AREA CHART -->--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Orders Chart [last year]</h3>--}}

                        {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="chart">--}}
                            {{--<canvas id="ordersChart" style="height:250px"></canvas>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                {{--</div>--}}
                {{--<!-- /.box -->--}}

            {{--</div>--}}
            {{--<div class="col-md-6">--}}
                {{--<!-- AREA CHART -->--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Phone Click Chart [last year]</h3>--}}

                        {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="chart">--}}
                            {{--<canvas id="phoneChart" style="height:250px"></canvas>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                {{--</div>--}}
                {{--<!-- /.box -->--}}

            {{--</div>--}}
            {{--<!-- /.col (LEFT) -->--}}
        {{--</div>--}}
        {{--<!-- /.row -->--}}

    {{--</section>--}}
    <!-- /.content -->

@endsection