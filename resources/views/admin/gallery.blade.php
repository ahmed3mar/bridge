@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'gallery',
            'ajax': '{!! route('admin.gallery.data', $type) !!}'
        }
    </script>
@endsection
@section('after_scripts')
    <!-- DataTables -->
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('container')

    <input type="hidden" id="type" name="type" value="{{ $type }}">

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Gallery</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Gallery</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="text" class="form-control" data-validation="required" id="title" name="title" placeholder="Gallery title">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Category</label>
                                    <select class="form-control" id="category_id" name="category_id">
                                        <option disabled selected>Select category</option>
                                        {!! drop_categories($type) !!}
                                        {{--@foreach(drop_categories($type) as $id => $name)--}}
                                            {{--<option value="{{ $id }}">{{ $name }}</option>--}}
                                        {{--@endforeach--}}
                                    </select>
                                </div>
                                @if($type == 'images')
                                <div class="form-group">
                                    <label for="file">Image</label>
                                    <input class="form-control" type="file" id="file" name="file">
                                </div>
                                @else
                                <div class="form-group">
                                    <label for="file">Video Url</label>
                                    <input class="form-control" type="text" id="url" name="url">
                                </div>
                                @endif
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $text }}</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{ $text }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-gallery'))
                            <div class="pull-right box-tools">
                                <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new gallery</a>
                            </div>
                    @endif
                    <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($gallery as $g)
                                <tr>
                                    <td>{{ $g->id }}</td>
                                    <td width="20%">{!! $g->image_url !!}</td>
                                    <td width="50%">{!! $g->title !!}</td>
                                    <td>{!! $g->options !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection