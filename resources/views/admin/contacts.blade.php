@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'contacts',
            'url': '{!! route('admin.contacts.data') !!}?',
            'ajax': '{!! route('admin.contacts.data') !!}{{ $ad_id ? '?ad=' . $ad_id : '' }}'
        }
    </script>
@endsection
@section('after_styles')
    <link rel="stylesheet" href="{{ asset('/backend/vendor/AdminLTE/plugins/daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css" />
@endsection
@section('after_scripts')
    <script type="text/javascript" src="{{ asset ("/backend/vendor/moment/min/moment.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset ("/backend/vendor/AdminLTE/plugins/daterangepicker/daterangepicker.js") }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script>
@endsection

@section('container')

    <input type="hidden" id="id" name="id">
    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="viewModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">View Contact</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bcontacted">
                        {{--<tr>--}}
                        {{--<td>Name</td>--}}
                        {{--<td data-id="name"></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                        {{--<td>E-Mail</td>--}}
                        {{--<td data-id="email"></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                        {{--<td>Phone</td>--}}
                        {{--<td data-id="phone"></td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                        {{--<td>Message</td>--}}
                        {{--<td data-id="message"></td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>Comment</td>
                            <td>
                                <textarea name="comment" id="comment" class="form-control"></textarea>
                                {{--<a href="#" data-id id="save-comment">Save</a>--}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-info accept-contact-btn" data-id="0">Accept</button>--}}
                    {{--<button type="button" class="btn btn-danger cancel-contact-btn" data-id="0">Cancel</button>--}}
                    <a href="#" class="btn btn-success"  data-id id="save-comment">Save</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="infoModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">View Contact</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bcontacted">
                        <tr>
                            <td>Name</td>
                            <td data-id="name"></td>
                        </tr>
                        <tr>
                            <td>E-Mail</td>
                            <td data-id="email"></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td data-id="phone"></td>
                        </tr>
                        <tr>
                            <td>Message</td>
                            <td data-id="message"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-info accept-contact-btn" data-id="0">Accept</button>--}}
                    {{--<button type="button" class="btn btn-danger cancel-contact-btn" data-id="0">Cancel</button>--}}
                    {{--<a href="#" class="btn btn-success"  data-id id="save-comment">Save</a>--}}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Contacts{{ $ad ? ' of [' . $ad->title . ']' : '' }}</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Contacts</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <div id="vue-components">
            <contact-filter></contact-filter>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <div class="form-inline">
                            <select name="type" id="type" class="form-control">
                                <option value="xls">XLS</option>
                                {{--<option value="xlsx">XLSX</option>--}}
                                <option value="csv">CSV</option>
                            </select>
                            <a href="#" id="exportIt" class="btn btn-success">Export</a>
                        </div>
                    </div>
                    <!-- /. tools -->
                    <br />
                </div>

                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bcontacted table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Comment</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->id }}</td>
                                <td width="20%">{!! $contact->name !!}</td>
                                <td width="10%">{!! $contact->phone !!}</td>
                                <td width="15%">{!! $contact->email !!}</td>
                                <td width="15%">{!! $contact->comment !!}</td>
                                <td width="15%">{!! $contact->message !!}</td>
                                <td width="10%">{!! $contact->created_at !!}</td>
                                <td>{!! $contact->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Comment</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection