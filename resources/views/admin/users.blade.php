@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'users',
            'ajax': '{!! route('admin.users.data') !!}'
        }
    </script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New User</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit User</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="form-group #add-only">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email address">
                        </div>
                        <div class="form-group #add-only">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                        {{--<div class="form-group #add-only">--}}
                            {{--<label for="phone">Phone Number</label>--}}
                            {{--<input type="tel" maxlength="25" class="form-control" id="phone" name="phone" placeholder="Phone number">--}}
                        {{--</div>--}}

                        <div class="form-group #add-only">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            <p class="small-text on-edit" style="display: none">Leave for no change.</p>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="status">Status</label>--}}
                            {{--<select class="form-control" name="status" id="status">--}}
                                {{--<option value="active">Active</option>--}}
                                {{--<option value="blocked">Blocked</option>--}}
                                {{--<option value="activation">Require Activation</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group add-only">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-8">--}}
                                    {{--<label for="name">Logo<span class="only-edit"> (leave for no change).</span></label>--}}
                                    {{--<input type="file" class="form-control" id="image_file" name="image_file" placeholder="Image">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4" id="image-preview"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label for="is_admin">Is Admin ?
                                <input type="checkbox" name="is_admin" value="1" id="is_admin">
                            </label>
                        </div>

                        <div class="form-group permissions-section" style="display: none;">
                            <label for="">Permissions</label>

                            <table class="table">
                                @foreach($permissionsGroups as $group => $permissions)
                                <tr>
                                    <td>{{ $group }}</td>
                                    <td>
                                        @foreach($permissions as $permission)
                                            <label for="permission-{{ $permission['name'] }}" style="width: 32%">
                                                <input type="checkbox" name="permissions[{{ $permission['name'] }}][{{ $permission['id'] }}]" id="permission-{{ $permission['name'] }}">
                                                {{ __('permissions.' . $permission['name']) }}
                                            </label>
                                        @endforeach
                                    </td>
                                </tr>
                                    @endforeach
                            </table>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Users</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Users</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box #box-danger #box-solid">

                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-user'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new user</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>E-Mail</th>
                                <th>Name</th>
                                <th>Role</th>
                                {{--<th>Phone</th>--}}
                                {{--<th>Status</th>--}}
                                <th>Register date</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td width="25%">{!! $user->email !!}</td>
                                    <td width="25%">{!! $user->name !!}</td>
                                    <td width="10%">{!! $user->is_admin !!}</td>
{{--                                    <td width="10%">{!! $user->phone !!}</td>--}}
{{--                                    <td width="10%">{!! $user->status !!}</td>--}}
                                    <td width="20%">{!! $user->created_at !!}</td>
                                    <td width="10%">{!! $user->options !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>E-Mail</th>
                                <th>Name</th>
                                <th>Role</th>
                                {{--<th>Phone</th>--}}
                                {{--<th>Status</th>--}}
                                <th>Register date</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
