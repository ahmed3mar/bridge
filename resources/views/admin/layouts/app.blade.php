<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name', 'BRIDGE') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/backend/vendor/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/backend/vendor/toastr/toastr.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/backend/vendor/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/backend/vendor/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    <link href="{{ mix('/backend/css/admin.css') }}" rel="stylesheet" type="text/css" />
    @yield('after_styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        window.config = {
            admin_url: '{{ route('admin.main') }}'
        }
        window.lang = {
            selected: "{{ ("selected") }}",
            validation: {
                required: "{{ ("This field is required") }}",
                email: "{{ ("Please enter valid email address") }}",
                isset: "{{ ("form_validation_isset") }}",
            }
        };
    </script>
</head>
<body class="skin-blue @if( Route::currentRouteName()=== 'admin.login' ) login-page @endif()">
<noscript>
    <div style="
    margin: auto;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.5);
    padding: 10px;
    position: fixed;
    z-index: 99999999;
">
        <div style="
    margin: auto;
    width: 40%;
    margin-top: 10%;
    border: 3px solid #f1ffff;
    border-radius: 5px;
    background: rgba(207,201,0,0.5);
    padding: 10px;
    font-size: 25px;
    color: white;
">Please enable javascript to use the admin panel.</div>
    </div>
</noscript>
@if( Route::currentRouteName()=== 'admin.login' )
    @yield('login')
@else
<div class="wrapper" id="app">

    <!-- Header -->
    @include('admin.common.header')

    <!-- Sidebar -->
    @include('admin.common.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @yield('container')

    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('admin.common.footer')

</div><!-- ./wrapper -->
@endif()

<!-- REQUIRED JS SCRIPTS -->

<!-- Scripts -->
@yield('before_scripts')


<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/backend/vendor/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/backend/vendor/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/backend/vendor/toastr/toastr.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/backend/vendor/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>

<script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>

<script src="{{ asset ("/backend/vendor/jquery-validation/dist/jquery.validate.min.js") }}"></script>
<script src="{{ asset ("/backend/vendor/bootstrap3-confirmation/bootstrap-confirmation.js") }}"></script>
<script src="{{ asset ("/backend/js/validation.js") }}"></script>

@yield('after_scripts')
<script src="{{ mix('/backend/js/admin.js') }}"></script>

</body>
</html>