@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'options',
            'ajax': '{!! route('admin.options.update') !!}'
        }
    </script>
@endsection

@section('container')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Options</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Options</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Global Options</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-global" class="form-horizontal">
                        <div class="box-body">
                            <input type="hidden" name="type" value="global">

                            <div class="form-group">
                                <label for="site_title" class="col-sm-2 control-label">Site title</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="site_title" id="site_title" placeholder="Site title (english)" value="{{ option('site_title') }}">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="site_title_ar" id="site_title_ar" placeholder="Site title (arabic)" value="{{ option('site_title_ar') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="home_title" class="col-sm-2 control-label">Home title</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="home_title" id="home_title" placeholder="Home title (english)" value="{{ option('home_title') }}">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="home_title_ar" id="home_title_ar" placeholder="Home title (arabic)" value="{{ option('home_title_ar') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_description" class="col-sm-2 control-label">Site description</label>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="site_description" id="site_description" placeholder="Site description (english)">{{ option('site_description') }}</textarea>
                                </div>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="site_description_ar" id="site_description_ar" placeholder="Site description (arabic)">{{ option('site_description_ar') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_keywords" class="col-sm-2 control-label">Site keywords</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="site_keywords" id="site_keywords" placeholder="Site keywords (english)" value="{{ option('site_keywords') }}">
                                    <p class="help-block keep">Seprate between words by comma ","</p>
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="site_keywords_ar" id="site_keywords_ar" placeholder="Site keywords (arabic)" value="{{ option('site_keywords_ar') }}">
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {{--<button type="submit" class="btn btn-default">Cancel</button>--}}
                            <button id="submit-global" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home Slider</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-slider" class="form-horizontal">
                        <div class="box-body">
                            <input type="hidden" name="type" value="slider">

                            <div class="form-group">
                                <label for="slider_top" class="col-sm-2 control-label">Slider Images</label>

                                <div class="col-sm-10">
                                    <table class="table table-bordered" id="sliderImages">
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Options</th>
                                        </tr>
                                        @foreach((object)json_decode(option('slider_links')) as $link)
                                            <tr>
                                                <td style="padding: 10px;">{{ $loop->index + 1 }}</td>
                                                <td>
                                                    <input type="hidden" name="slider_images[]" value="{{ $link->image }}">
                                                    <img style="width: 50px;" src="{{ slider_image($link->image) }}" alt=""></td>
                                                <td>
                                                    <input type="url" name="links[]" value="{{ $link->link }}">
                                                </td>
                                                <td><a href="#" class="deleteSliderImage"><i class="fa fa-trash text-danger"></i></a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <a href="#" class="btn btn-success addSliderImage">Add new image</a>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {{--<button type="submit" class="btn btn-default">Cancel</button>--}}
                            <button id="submit-slider" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">About section</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-about" class="form-horizontal">
                        <input type="hidden" name="type" value="about">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="about_title" class="col-sm-2 control-label">Title</label>

                                <div class="col-sm-5">
                                    <input type="text" data-validation="required" class="form-control" name="about_title" id="about_title" placeholder="Title (english)" value="{{ option('about_title') }}">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" data-validation="required" class="form-control" name="about_title_ar" id="about_title_ar" placeholder="Title (arabic)" value="{{ option('about_title_ar') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="about_content" class="col-sm-2 control-label">Content</label>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="about_content" id="about_content" placeholder="Content (english)">{{ option('about_content') }}</textarea>
                                </div>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="about_content_ar" id="about_content_ar" placeholder="Content (arabic)">{{ option('about_content_ar') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="about_file" class="col-sm-2 control-label">Image</label>

                                <div class="col-sm-5">
                                    <input type="file" name="about_file" id="about_file" class="form-control">
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="submit-about" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Social links</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-social" class="form-horizontal">
                        <div class="box-body">
                            <input type="hidden" name="type" value="social">


                            <div class="form-group">
                                <label for="facebook" class="col-sm-2 control-label">Facebook</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_facebook" id="facebook" placeholder="Facebook" value="{{ option('social_facebook') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="google_plus" class="col-sm-2 control-label">Google+</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_google_plus" id="google_plus" placeholder="google_plus" value="{{ option('social_google_plus') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="linked_in" class="col-sm-2 control-label">linked_in</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_linked_in" id="linked_in" placeholder="linked_in" value="{{ option('social_linked_in') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="twitter" class="col-sm-2 control-label">twitter</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_twitter" id="twitter" placeholder="twitter" value="{{ option('social_twitter') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="youtube" class="col-sm-2 control-label">youtube</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_youtube" id="youtube" placeholder="youtube" value="{{ option('social_youtube') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="instagram" class="col-sm-2 control-label">instagram</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="social_instagram" id="instagram" placeholder="instagram" value="{{ option('social_instagram') }}">
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="submit-social" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Map</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-map" class="form-horizontal">
                        <div class="box-body">
                            <input type="hidden" name="type" value="map">


                            <div class="form-group">
                                <label for="map_lat" class="col-sm-2 control-label">Lat</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="map_lat" id="map_lat" placeholder="map_lat" value="{{ option('map_lat') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="map_lng" class="col-sm-2 control-label">Lng</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="map_lng" id="map_lng" placeholder="map_lng" value="{{ option('map_lng') }}">
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="submit-map" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Site Close</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-close" class="form-horizontal">
                        <input type="hidden" name="type" value="close">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="site_title" class="col-sm-2 control-label">Site close?</label>

                                <div class="col-sm-10">
                                    <label class="radio-inline"><input type="radio" name="site_close" value="1" @if( option('site_close') ) checked @endif()>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="site_close" value="0" @if( !option('site_close') ) checked @endif()>No</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_description" class="col-sm-2 control-label">Close Message</label>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="site_close_message" id="site_close_message" placeholder="Site close message (english)">{{ option('site_close_message') }}</textarea>
                                </div>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="site_close_message_ar" id="site_close_message_ar" placeholder="Site close message (arabic)">{{ option('site_close_message_ar') }}</textarea>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {{--<button type="submit" class="btn btn-default">Cancel</button>--}}
                            <button id="submit-close" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection