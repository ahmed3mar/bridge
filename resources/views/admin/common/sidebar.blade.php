<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/backend/vendor/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">The Modules</li>
            <!-- Optionally, you can add icons to the links -->
            <li @if($current == 'home') class="active"@endif()><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @if(can(['add-user', 'edit-user', 'delete-user']))
                <li @if($current == 'users') class="active"@endif()><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
            @endif

            @if(can(['add-service', 'edit-service', 'delete-service']))
            <li @if($current == 'services') class="active"@endif()><a href="{{ route('admin.services') }}"><i class="fa fa-eercast"></i> <span>Services</span></a></li>
            @endif



            <li class="treeview @if($current === 'projects' || $current === 'projects-categories') active @endif()">
                <a href="#"><i class="fa fa-eercast"></i> <span>Projects</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(isset($sub) && $sub == 'projects-categories') class="active" @endif()><a href="{{ route('admin.categories', 'projects') }}"><i class="fa fa-info-circle"></i> Categories</a></li>
                    <li @if(isset($sub) && $sub == 'projects') class="active" @endif()><a href="{{ route('admin.projects') }}"><i class="fa fa-asterisk"></i> Projects</a></li>
                </ul>
            </li>

            <li class="treeview @if($current === 'images' || $current === 'images-categories') active @endif()">
                <a href="#"><i class="fa fa-eercast"></i> <span>Images</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(isset($sub) && $sub == 'images-categories') class="active" @endif()><a href="{{ route('admin.categories', 'images') }}"><i class="fa fa-info-circle"></i> Categories</a></li>
                    <li @if(isset($sub) && $sub == 'images') class="active" @endif()><a href="{{ route('admin.gallery', 'images') }}"><i class="fa fa-asterisk"></i> Images</a></li>
                </ul>
            </li>

            <li class="treeview @if($current === 'videos' || $current === 'videos-categories') active @endif()">
                <a href="#"><i class="fa fa-eercast"></i> <span>Videos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(isset($sub) && $sub == 'videos-categories') class="active" @endif()><a href="{{ route('admin.categories', 'videos') }}"><i class="fa fa-info-circle"></i> Categories</a></li>
                    <li @if(isset($sub) && $sub == 'videos') class="active" @endif()><a href="{{ route('admin.gallery', 'videos') }}"><i class="fa fa-asterisk"></i> Videos</a></li>
                </ul>
            </li>

            <li class="treeview @if($current === 'news' || $current === 'news-categories') active @endif()">
                <a href="#"><i class="fa fa-eercast"></i> <span>News</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(isset($sub) && $sub == 'news-categories') class="active" @endif()><a href="{{ route('admin.categories', 'news') }}"><i class="fa fa-info-circle"></i> Categories</a></li>
                    <li @if(isset($sub) && $sub == 'news') class="active" @endif()><a href="{{ route('admin.news') }}"><i class="fa fa-asterisk"></i> News</a></li>
                </ul>
            </li>

            @if(can(['add-translation', 'edit-translation', 'delete-translation']))
                <li @if($current == 'translations') class="active"@endif()><a href="{{ route('admin.translations') }}"><i class="fa fa-language"></i> <span>Translations</span></a></li>
            @endif

            @if(can(['add-page', 'edit-page', 'delete-page']))
                <li @if($current == 'pages') class="active"@endif()><a href="{{ route('admin.pages') }}"><i class="fa fa-file"></i> <span>Page</span></a></li>
            @endif

            @if(can(['view-order', 'delete-order']))
                <li @if($current == 'orders') class="active"@endif()><a href="{{ route('admin.orders') }}"><i class="fa fa-user-o"></i> <span>Orders</span></a></li>
            @endif
            @if(can(['view-contacts', 'edit-contact', 'delete-contact']))
                <li @if($current == 'contacts') class="active"@endif()><a href="{{ route('admincontacts.index') }}"><i class="fa fa-user-o"></i> <span>Contact us</span></a></li>
            @endif
            @if(can(['edit-options']))
                <li @if($current == 'options') class="active"@endif()><a href="{{ route('admin.options.index') }}"><i class="fa fa-cogs"></i> <span>Options</span></a></li>
            @endif
            <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>