<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="mailto:ahmed3mar@outlook.com">Ahmed M. Ammar</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright © {{ date('Y') }} <a href="#">BRIDGE</a>.</strong> All rights reserved.
</footer>