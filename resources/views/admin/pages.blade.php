@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'pages',
            'ajax': '{!! route('admin.pages.data') !!}'
        }
    </script>
@endsection

@section('after_styles')
    {{--    <link rel="stylesheet" href="{{ asset('/backend/vendor/AdminLTE/plugins/ckeditor/ckeditor.js') }}" />--}}
@endsection

@section('after_scripts')
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/ckeditor/ckeditor.js") }}"></script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Page</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Page</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">

                        <div class="row">
                            <div class="col-md-6">

                                <h4>PAGE INFO</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" data-validation="required" id="title" name="title" placeholder="Page title english">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="title_ar" name="title_ar" placeholder="Page title arabic">
                                </div>
                                <div class="form-group">
                                    <input type="text" data-validation="required" class="form-control" id="slug" name="slug" placeholder="Page slug (English)">
                                    <p class="help-info">This is unique value that will be in the link <kbd>ENGLISH</kbd>.</p>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" data-validation="required" class="form-control" id="slug_ar" name="slug_ar" placeholder="Page slug (Arabic)">--}}
                                    {{--<p class="help-info">This is unique value that will be in the link <kbd>ARABIC</kbd>.</p>--}}
                                {{--</div>--}}

                            </div>

                            <div class="col-md-6">

                                <h4>PAGE SEO</h4>
                                <div class="form-group">
                                    <label for="name">SEO Title (English)</label>
                                    <input type="text" class="form-control" data-validation="required" id="seo_title" name="seo_title" placeholder="SEO Title (English)">
                                </div>

                                <div class="form-group">
                                    <label for="name">SEO Title (Arabic)</label>
                                    <input type="text" class="form-control" data-validation="required" id="seo_title_ar" name="seo_title_ar" placeholder="SEO Title (Arabic)">
                                </div>

                                <div class="form-group">
                                    <label for="name">SEO Description (English)</label>
                                    <textarea class="form-control" data-validation="required" id="seo_description" name="seo_description" placeholder="SEO Description English"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="name">SEO Description (Arabic)</label>
                                    <textarea class="form-control" data-validation="required" id="seo_description_ar" name="seo_description_ar" placeholder="SEO Description Arabic"></textarea>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <textarea name="content" id="content" rows="3" class="form-control" placeholder="Content English"></textarea>
                                </div>

                                <div class="form-group">
                                    <textarea name="content" id="content_ar" rows="3" class="form-control" placeholder="Content Arabic"></textarea>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pages</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Pages</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box #box-danger #box-solid">

                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-page'))
                            <div class="pull-right box-tools">
                                <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new page</a>
                            </div>
                    @endif
                    <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Page</th>
                                <th>Arabic title</th>
                                <th>Slug</th>
                                {{--<th>Slug Ar</th>--}}
                                <th>Created At</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr>
                                    <td>{{ $page->id }}</td>
                                    <td width="20%">{!! $page->title !!}</td>
                                    <td width="20%">{!! $page->title_ar !!}</td>
                                    <td width="10%">{!! $page->slug !!}</td>
{{--                                    <td width="10%">{!! $page->slug_ar !!}</td>--}}
                                    <td width="20%">{!! $page->created_at !!}</td>
                                    <td>{!! $page->options !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Page</th>
                                <th>Arabic title</th>
                                <th>Slug</th>
                                {{--<th>Slug Ar</th>--}}
                                <th>Created At</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection