@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'categories',
            'ajax': '{!! route('admin.categories.data', $type) !!}'
        }
    </script>
@endsection
@section('after_scripts')

    <style type="text/css">
        .cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
        *:first-child+html .cf { zoom: 1; }
        a { color: #2996cc; }
        a:hover { text-decoration: none; }
        p { line-height: 1.5em; }
        .small { color: #666; font-size: 0.875em; }
        .large { font-size: 1.25em; }
        /**
         * Nestable
         */
        /*.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }*/
        .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
        .dd-list .dd-list { padding-left: 30px; }
        .dd-collapsed .dd-list { display: none; }
        .dd-item,
        .dd-empty,
        .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
        .dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
            background: #fafafa;
            background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
            background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
            background:         linear-gradient(top, #fafafa 0%, #eee 100%);
            -webkit-border-radius: 3px;
            border-radius: 3px;
            box-sizing: border-box; -moz-box-sizing: border-box;
        }
        .dd-handle:hover { color: #2ea8e5; background: #fff; }
        .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
        .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
        .dd-item > button[data-action="collapse"]:before { content: '-'; }
        .dd-placeholder,
        .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
        .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
            background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-size: 60px 60px;
            background-position: 0 0, 30px 30px;
        }
        .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
        .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
        .dd-dragel .dd-handle {
            -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
        }
        /**
         * Nestable Extras
         */
        .nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }
        #nestable-menu { padding: 0; margin: 20px 0; }
        #nestable-output,
        #nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }
        #nestable2 .dd-handle {
            color: #fff;
            border: 1px solid #999;
            background: #bbb;
            background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
            background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
            background:         linear-gradient(top, #bbb 0%, #999 100%);
        }
        #nestable2 .dd-handle:hover { background: #bbb; }
        #nestable2 .dd-item > button:before { color: #fff; }
        @media only screen and (min-width: 700px) {
            .dd { float: left; width: 98%; }
            .dd + .dd { margin-left: 2%; }
        }
        .dd-hover > .dd-handle { background: #2ea8e5 !important; }
        /**
         * Nestable Draggable Handles
         */
        .dd3-content {
            display: block; height: 30px;
            margin: 5px 0; padding: 5px 10px 5px 80px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
            background: #fafafa;
            background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
            background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
            background:         linear-gradient(top, #fafafa 0%, #eee 100%);
            -webkit-border-radius: 3px;
            border-radius: 3px;
            box-sizing: border-box; -moz-box-sizing: border-box;
        }
        .dd3-image {
            position: absolute;
            top: 0;
            left: 30px;
            border-left: 1px solid #DDD;
            padding: 3px;
        }
        .dd-image {
            width: 25px;
            height: 25px;
        }
        .dd3-options {
            position: absolute;
            top: 0;
            right: 0;
            border-left: 1px solid #DDD;
            padding: 5px 10px;
        }
        .dd3-content:hover { color: #2ea8e5; background: #fff; }
        .dd-dragel > .dd3-item > .dd3-content { margin: 0; }
        .dd3-item > button { margin-left: 30px; }
        .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
            border: 1px solid #aaa;
            background: #ddd;
            background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
            background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
            background:         linear-gradient(top, #ddd 0%, #bbb 100%);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
        .dd3-handle:hover { background: #ddd; }
        /**
         * Socialite
         */
        .socialite { display: block; float: left; height: 35px; }
    </style>


    <!-- DataTables -->
    <script src="{{ asset ("/backend/js/jquery.nestable.js") }}"></script>
{{--    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>--}}
{{--    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>--}}

@endsection

@section('container')

    <input type="hidden" id="type" name="type" value="{{ $type }}">

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Category</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" data-validation="required" id="title" name="title" placeholder="Category title">
                                </div>
                                <div class="form-group">
                                    <label for="title_ar">Title</label>
                                    <input type="text" class="form-control" data-validation="required" id="title_ar" name="title_ar" placeholder="Category title (arabic)">
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="name">Image<span class="only-edit"> (leave for no change).</span></label>
                                        <input type="file" class="form-control" id="file" name="file" placeholder="Image">
                                    </div>
                                    <div class="col-md-4" id="image-preview"></div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Categories</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Categories</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->

        <div class="row">
            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-'.$type.'-category'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new category</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                                <div class="dd">
                                    <ol class="dd-list">
                                        {{--@foreach($categories as $category)--}}
                                        {{--<li class="dd-item" data-id="1">--}}
                                            {{--<div class="dd-handle dd3-handle">Drag</div>--}}
                                            {{--<div class="dd3-content">{{ $category->title }}</div>--}}
                                            {{--<div class="dd3-options">--}}
                                                {{--<a href="#" data-id="{{ $category->id }}" class="text-success"><i class="fa fa-edit"></i></a>--}}
                                                {{--&nbsp;--}}
                                                {{--<a href="#" data-id="{{ $category->id }}" class="text-danger"><i class="fa fa-trash"></i></a>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--@endforeach--}}
                                        {{--<li class="dd-item" data-id="2">--}}
                                            {{--<div class="dd-handle">Item 2</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="dd-item" data-id="3">--}}
                                            {{--<div class="dd-handle">Item 3</div>--}}
                                            {{--<ol class="dd-list">--}}
                                                {{--<li class="dd-item" data-id="4">--}}
                                                    {{--<div class="dd-handle">Item 4</div>--}}
                                                {{--</li>--}}
                                                {{--<li class="dd-item" data-id="5">--}}
                                                    {{--<div class="dd-handle">Item 5</div>--}}
                                                {{--</li>--}}
                                            {{--</ol>--}}
                                        {{--</li>--}}
                                    </ol>
                                </div>

                        {{--<table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>#</th>--}}
                                {{--<th>Title</th>--}}
                                {{--<th>Options</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--@foreach($categories as $category)--}}
                            {{--<tr>--}}
                                {{--<td>{{ $category->id }}</td>--}}
                                {{--<td width="50%">{!! $category->title !!}</td>--}}
                                {{--<td>{!! $category->options !!}</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection