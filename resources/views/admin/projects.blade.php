@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'projects',
            'ajax': '{!! route('admin.projects.data') !!}'
        }
    </script>
@endsection
@section('after_scripts')
    <!-- DataTables -->
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
    <script src="https://cdn.ckeditor.com/4.9.1/basic/ckeditor.js"></script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Project</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Project</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" data-validation="required" id="title" name="title" placeholder="Project title">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Category</label>
                                    <select class="form-control" id="category_id" name="category_id">
                                        <option disabled selected>Select category</option>
                                        {!! drop_categories('projects') !!}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">Project Content</label>
                                    <div id="content-editor" class="show-only-text"></div>
                                    <textarea class="hidden" id="content" name="content"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title_ar">Title (Arabic)</label>
                                    <input type="text" class="form-control" data-validation="required" id="title_ar" name="title_ar" placeholder="Project title (Arabic)">
                                </div>
                                <div class="form-group">
                                    <label for="content_ar">Project Content (Arabic)</label>
                                    <div id="content_ar-editor" class="show-only-text"></div>
                                    <textarea class="hidden" id="content_ar" name="content_ar"></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody id="projectImages">
                                    @foreach((object)json_decode(option('slider_links')) as $link)
                                        <tr>
                                            <td style="padding: 10px;">{{ $loop->index + 1 }}</td>
                                            <td>
                                                <input type="hidden" name="project_images[]" value="{{ $link->image }}">
                                                <img style="width: 50px;" src="{{ slider_image($link->image) }}" alt="">
                                            </td>
                                            <td><a href="#" class="deleteProjectImage"><i class="fa fa-trash text-danger"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <a href="#" class="btn btn-success addProjectImage">Add new image</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Projects</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Projects</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-project'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new project</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->id }}</td>
                                <td width="10%">{!! $project->image_url !!}</td>
                                <td width="50%">{!! $project->title !!}</td>
                                <td>{!! $project->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection