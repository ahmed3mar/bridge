@extends('layouts.app')

@section('body_class')
    blog
@stop

@section('content')

    <style>
        section.blog article {
            margin-bottom: 0px;
            position: relative;
        }
        section.blog article h3{
            position: absolute;
            bottom: 0;
            background: #00000094;
            right: 15px;
            left: 15px;
            text-align: center;
            padding: 5px;
            color: white;
        }
    </style>

    <section class="blog container" style="margin-bottom: 0;">
        <div class="row">
            <div class="col-lg-12 headline">
                <h1>{{ $category->{_trans('title')} }}</h1>
                <hr />
            </div>
        </div>

        <div class="row">
            @foreach($category->subCategories as $subCategory)
            <article class="col-md-4" style="margin-bottom: 0px;">
                <figure>
                    <img src="{{ $subCategory->image_url }}"  alt="img" />
                    <figurecaption>
                        <a class="btn" href="{{ $subCategory->url }}"><i class="fa fa-camera"></i>Read more</a>
                    </figurecaption>
                </figure>
                <h3>{{ $subCategory->{_trans('title')} }}</h3>
            </article>
            @endforeach
        </div>

    </section>

    @if($category->projects and sizeof($category->projects) > 0)
    <section class="container">

        <div class="row headline">
            <h1>{{ trans('projects.projects') }}</h1>
            <hr>
        </div>

        @foreach($category->projects as $project)
            <article class="row" style="margin-bottom: 40px;">
                <div class="content col-xs-12">
                    <div class="col-xs-4">
                        <img src="{{ $project->image_url }}" >
                    </div>
                    <div class="col-xs-8">
                        <h3>{{ $project->{_trans('title')} }}</h3>
                        <p>{{ str_limit(strip_tags($project->{_trans('content')})) }}</p>
                        <a href="{{ $project->url }}" class="btn"><i class="fa fa-bars"></i>{{ trans('projects.read_more') }}</a>
                    </div>
                </div>
            </article>
        @endforeach
    </section>
    @endif


@endsection
