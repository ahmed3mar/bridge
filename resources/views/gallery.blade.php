@extends('layouts.app')
@section('body_class') blog @stop

@section('after_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
@stop

@section('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <script>
        $("a.fancy").fancybox({
            'titleShow'     : false,
            'transitionIn'	: 'elastic',
            'transitionOut'	: 'elastic',
            'easingIn'      : 'easeOutBack',
            'easingOut'     : 'easeInBack'
        });
    </script>
@stop

@section('content')

    <style>
        .g .col-md-3 {
            height: 150px;
            margin-bottom: 40px;
        }
        .g img {
            width: 100%;
            height: 100%;

            padding: 5px;
            background: white;
            border: 1px solid #BBB;

        }
    </style>

    <section class="" style="margin: 0">
    <div class="container">
        <div class="row headline">
            <h1>{{ trans('gallery.gallery') }}</h1>
            <hr />
        </div>
    </div>
    </section>

    <section class="g container">

        <div class="row">
            @foreach($gallery as $item)
            <div class="col-md-3">
                <a class="fancy" href="{{ $item->image_url }}">
                    <img src="{{ $item->image_url }}" />
                </a>
            </div>
            @endforeach
        </div>

        {{ $gallery->links() }}

    </section>

    <br>
    <br>
    <br>
    <br>

@endsection
