<figure data-path-hover="m 0 0 L 100 100 L 100 100 L 0 100 z" class="gallery-item">
    <div class="loading">
        LOADING
    </div>
    <img src="{{ $media->image_url }}" class="photo" alt="photo"/>
    <svg viewBox="0 0 100 100" preserveAspectRatio="none">
        <path class="background" d="M 0 75 L 100 75 L 100 100 L 0 100 z"/>
    </svg>
    <figcaption>
        <h4>{{ $media->title }}</h4>
        <ul class="tags">
            <li>{{ $media->category->{_trans('title')} }}</li>
        </ul>
        <a href="{{ route('gallery') }}" class="btn">See more</a>
    </figcaption>
</figure>