<footer class="s">
    <div class="container">
        <div class="goUp">
            <i class="fa fa-angle-up"></i>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <img src="{{ asset('img/bridge-logo.png') }}" alt="ONE" />
                </div>
            </div>
            <div class="col-md-3">
                <h3 class="footer-title">Useful links<span /></h3>
                <ul class="footer-links">
                    <li><a href="">Our fairs</a></li>
                    <li><a href=""> Finishing Request</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3 class="footer-title">Our Fairs<span /></h3>
                <ul class="footer-links">
                    <li><a href="">Home</a></li>
                    <li><a href="">Services</a></li>
                    <li><a href="">Projects</a></li>
                    <li><a href="">About Us</a></li>
                    <li><a href="">Contact Us</a></li>
                    <li style="color: #e2e2e2;font-size: 15px;">Gallery
                    </li><li style="margin-left: 15px;"><a href="">Photos</a></li>
                    <li style="margin-left: 15px;"><a href="">Videos</a></li>

                </ul>
            </div>
            <div class="col-md-3">
                <h3 class="footer-title">Follow us on Facebook<span /></h3>
                <div style="color:rgb(155, 155, 155);">
                    <div class="fb-like" data-href="https://www.facebook.com/bridgeeg" data-width="230" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                </div>
            </div>
        </div>

        

        {{--<div class="social">--}}
            {{--<h3>Follow us</h3>--}}

            {{--<div>--}}
                {{--@if(option('social_facebook')) <a href="{{ option('social_facebook') }}"><i class="fa fa-facebook"></i></a>@endif--}}
                {{--@if(option('social_twitter')) <a href="{{ option('social_twitter') }}"><i class="fa fa-twitter"></i></a>@endif--}}
                {{--@if(option('social_google_plus')) <a href="{{ option('social_google_plus') }}"><i class="fa fa-google-plus"></i></a>@endif--}}
                {{--@if(option('social_linked_in')) <a href="{{ option('social_linked_in') }}"><i class="fa fa-linkedin"></i></a>@endif--}}
                {{--@if(option('social_youtube')) <a href="{{ option('social_youtube') }}"><i class="fa fa-youtube"></i></a>@endif--}}
                {{--@if(option('social_instagram')) <a href="{{ option('social_instagram') }}"><i class="fa fa-instagram"></i></a>@endif--}}
            {{--</div>--}}

            {{--<hr />--}}
        {{--</div>--}}

        {{--<div class="copyright">--}}
            {{--<p>&copy; 2018 <strong>{{ option(_trans('site_title')) }}</strong>. All Rights Reserved. </p>--}}
        {{--</div>--}}
    </div>
    <div style="background: black">
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="padding-top: 5px;">
                    <span class="copyright">© Bridgeeg.com Designed by <a href="http://www.bridgeeg.com/" target="_blank">Bridge Group</a></span>
                </div>
                <div class="col-md-4">
                    <ul class="social-links">
                        @if(option('social_facebook'))
                            <li>
                                <a href="{{ option('social_facebook') }}"><i class="fa fa-facebook"></i></a>
                            </li>
                        @endif
                        @if(option('social_twitter'))
                            <li>
                                <a href="{{ option('social_twitter') }}"><i class="fa fa-twitter"></i></a>
                            </li>
                        @endif
                        @if(option('social_google_plus'))
                            <li><a href="{{ option('social_google_plus') }}"><i class="fa fa-google-plus"></i></a></li>
                            @endif
                        @if(option('social_linked_in'))
                            <li><a href="{{ option('social_linked_in') }}"><i class="fa fa-linkedin"></i></a></li>
                            @endif
                        @if(option('social_youtube'))
                            <li><a href="{{ option('social_youtube') }}"><i class="fa fa-youtube"></i></a></li>
                            @endif
                        @if(option('social_instagram'))
                            <li><a href="{{ option('social_instagram') }}"><i class="fa fa-instagram"></i></a></li>
                            @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>