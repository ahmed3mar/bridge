<header @if(isset($home)) id="home" @endif >

    @if(isset($home))
    <div class="swiper-container">
        <div class="swiper-wrapper">
            {{--<!--First Slide-->--}}
            {{--<div class="swiper-slide" style="background-image:url('img/slider/01.jpg')">--}}

            {{--</div>--}}
            <!--Second Slide-->
                @foreach((object)json_decode(option('slider_links')) as $link)
            <div class="swiper-slide"  style="background-image:url('image/slider/{{ $link->image }}')">
                <!-- Any HTML content of the second slide goes here -->

                <a class="btn red" style="margin-top:60px" href="{{ $link->link }}">Learn more</a>

            </div>
                @endforeach
            <!--Third Slide-->
            <div class="swiper-slide video">
                <video id="vid2" autoplay loop muted>
                    <source id="video-player" type="video/mp4">
                </video>
                {{--<a  style="margin-top:60px"  href="http://themeforest.net/item/one-html-template/7974440" class="btn">Buy it now</a>--}}
            </div>
            {{--<!--Etc..-->--}}
            {{--<div class="swiper-slide"  style="background-image:url('img/slider/04.jpg')">--}}
                {{--<!-- Any HTML content of the fourth slide goes here -->--}}
                {{--<a  style="margin-top:60px"  href="http://themeforest.net/item/one-html-template/7974440" class="btn alternate">View our work</a>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="swiper-controls">
        <div class="arrow left">
            <div class="prev_thumbnail"></div>
            <i class="fa fa-angle-left"></i>
        </div>
        <div class="arrow right">
            <div class="next_thumbnail"></div>
            <i class="fa fa-angle-right"></i>
        </div>
    </div>
    <div class="swiper-indicators">

    </div>
    @endif

    <style>
        /*@media only screen and (max-width: 768px) {*/
            .mobileMenu #menu {
                display: none;
            }
            .menu-icon {
                display: none;
            }
            .mobileMenu .menu-icon {
                display: block;
                position: absolute;
                top: 40px;
                left: 25px;
                color: #0c508a;
            }
            .mobileMenu #menu {
                position: fixed;
                top: 100px;
                left: 0;
                right: 0;
                bottom: 0;
                background: #326095;
            }
            .mobileMenu #menu li {
                opacity: 1 !important;
            }

        @media (min-width: 992px)
        {
            .col-md-pull-9 {
                right: 66%;
                left: auto;
            }
        }
        /*}*/
    </style>

    <div class="top-menu">

        <div id="header-shadow"></div>

        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-md-9  col-md-push-3 ">

                    <nav id="menu">
                        <ul>
                            {{--<li class="show-menu-li"><a href="{{ url('') }}#menu" class="show-menu" >{{ trans('header.MENU') }}</a></li>--}}
                            <li><a href="#home">{{ trans('header.home') }}</a></li>
                            <li><a href="#services">{{ trans('header.Services') }}</a></li>
                            <li><a href="#projects">{{ trans('header.Projects') }}</a></li>
                            <li><a href="#features">{{ trans('header.About Us') }}</a></li>
                            <li><a href="#gallery">{{ trans('header.Gallery') }}</a></li>
                            <li><a href="#contact">{{ trans('header.Contact') }}</a></li>
                            @if(auth()->guest())
                            <li><a href="{{ route('login') }}">{{ trans('header.Login') }}</a></li>
                            @else
                            <li><a href="{{ route('profile') }}">{{ trans('header.Profile') }}</a></li>
                            @endif
                        </ul>
                    </nav>

                    <a href="#" class="menu-icon">
                        <i class="fa fa-2x fa-list"></i>
                    </a>

                </div>

                <div class="col-xs-2 col-xs-push-3 col-md-2 col-md-pull-9 logo-container">
                    <a href="{{ route('home') }}"><img src="{{ asset('img/bridge-logo.png') }}" alt="BRIDGE" class="logo"></a>
                </div>
                {{--<div class="col-md-1 col-md-pull-9 info">--}}
                    {{--<div class="outer">--}}
                        {{--<div class="inner">--}}
                            {{--<img src="img/info.png" alt="">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="info-bubble">--}}
                        {{--<h4>Voice control</h4>--}}
                        {{--<p>--}}
                            {{--This Website makes use of voice control technology.--}}
                            {{--You can use the following commands to navigate:</p>--}}
                        {{--<p>- Next (next slide)</p>--}}
                        {{--<p>- Previous (previous slide)</p>--}}
                        {{--<p>- Down (slide down)</p>--}}

                    {{--</div>--}}

                {{--</div>--}}

            </div>
        </div>
    </div>

</header>
