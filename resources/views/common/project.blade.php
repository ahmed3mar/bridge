<figure data-path-hover="m 0 0 L 100 100 L 100 100 L 0 100 z">
    <div class="loading">
        LOADING
    </div>
    <img src="{{ $project->image_url }}" class="photo" alt="photo"/>
    <svg viewBox="0 0 100 100" preserveAspectRatio="none">
        <path class="background" d="M 0 75 L 100 75 L 100 100 L 0 100 z"/>
    </svg>
    <figcaption>
        <h4>{{ $project->{_trans('title')} }}</h4>
        <ul class="tags">
            <li>{{ $project->category->{_trans('title')} }}</li>
        </ul>
        <a href="{{ $project->url }}" class="btn">See more</a>
    </figcaption>
</figure>