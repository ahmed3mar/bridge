<div class="contact1">

    <div class="container-contact1">
        <div class="contact1-pic js-tilt" data-tilt>
            <div class="contact_info">
                <h1>Contact US</h1>
                <div class="contact_info_seprator"></div>
                <p style="color:#333333;"><i class="fa fa-map-marker" aria-hidden="true"></i>
                    OUR LOCATION</p>
                <p class="txt">Kuwait Opera House</p><br>
                <p style="color:#333333;"><i class="fa fa-building-o" aria-hidden="true"></i>
                    OUR OFFICE</p>
                <p class="txt">
                    Kuwait City-Kuwait<br><br>
                </p>
                <p style="color:#333333;"><i class="fa fa-mobile" aria-hidden="true"></i> Mobile: 99011183</p>
                <a href="mailto:info@dinnerinthesky.com.kw"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                    Email : info@dinnerinthesky.com.kw</a>
            </div>
        </div>

        <form class="contact1-form validate-form">
				<span class="contact1-form-title">
                    {{ trans('page.get_in_touch') }}
				</span>


            <div class="wrap-input1 validate-input" data-validate = "{{ trans('page.name_is_required') }}">
                <input class="input1" type="text" name="name" placeholder="{{ trans('page.name') }}">
                <span class="shadow-input1"></span>
            </div>

            <div class="wrap-input1 validate-input" data-validate = "{{ trans('page.email_is_required') }}">
                <input class="input1" type="text" name="email" placeholder="{{ trans('page.email') }}">
                <span class="shadow-input1"></span>
            </div>

            <div class="wrap-input1 validate-input"  data-validate = "{{ trans('page.phone_is_required') }}">
                <input class="input1" type="text" name="subject" placeholder="{{ trans('page.phone') }}">
                <span class="shadow-input1"></span>
            </div>

            <div class="wrap-input1 validate-input" data-validate = "{{ trans('page.comment_is_required') }}">
                <textarea class="input1" name="message" placeholder="{{ trans('page.your_comment') }}"></textarea>
                <span class="shadow-input1"></span>
            </div>

            <div class="container-contact1-form-btn">
                <button class="contact1-form-btn">
						<span>
							{{ trans('page.submit') }}
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
						</span>
                </button>
            </div>
        </form>
    </div>
</div>