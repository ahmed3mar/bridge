<footer class="footer-area single-section">
    <div class="container">
        <div class="row main-footer">
            <div class="col-12 col-md-3">
                <h5 class="column-title">Useful links</h5>
                <ul class="list-unstyled column-content">
                    <li><a href="">Our fairs</a></li>
                    <li><a href=""> Finishing Request</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <h5 class="column-title">Our Fairs</h5>
                <ul class="list-unstyled column-content">
                    <li><a href="">Home</a></li>
                    <li><a href="">Services</a></li>
                    <li><a href="">Projects</a></li>
                    <li><a href="">About Us</a></li>
                    <li><a href="">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <h5 class="column-title">Gallery</h5>
                <ul class="list-unstyled column-content">
                    <li><a href="#0">Photos</a></li>
                    <li><a href="#0">Videos</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <h2 class="brand-logo"><img src="{{ asset('img/bridge-logo.png') }}" alt="ONE" /></h2>
                <p class="brand-description">Follow us on Facebook</p>
                <div class="fb-like" data-href="https://www.facebook.com/bridgeeg" data-width="230" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
        </div>
        <div class="row mini-footer">
            <div class="col-12">
                <div class="social-medias">
                    @if(option('social_twitter'))
                        <a class="twitter" href="{{ option('social_twitter') }}">
                            <i class="fa fa-twitter"></i>
                        </a>
                    @endif
                    @if(option('social_instagram'))
                        <a class="instagram" href="{{ option('social_instagram') }}">
                            <i class="fa fa-instagram"></i>
                        </a>
                    @endif
                    @if(option('social_linked_in'))
                        <a class="linkedin" href="{{ option('social_linked_in') }}">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    @endif
                    @if(option('social_youtube'))
                        <a class="youtube" href="{{ option('social_youtube') }}">
                            <i class="fa fa-youtube"></i>
                        </a>
                    @endif
                    @if(option('social_google_plus'))
                        <a class="youtube" href="{{ option('social_google_plus') }}">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    @endif
                    @if(option('social_facebook'))
                        <a class="facebook" href="{{ option('social_facebook') }}">
                            <i class="fa fa-facebook"></i>
                        </a>
                    @endif
                </div>
                <p class="copyright-notice">© Bridgeeg.com Designed by <a href="http://www.bridgeeg.com/" target="_blank">Bridge Group</a>.</p>
            </div>
        </div>
    </div>
</footer>