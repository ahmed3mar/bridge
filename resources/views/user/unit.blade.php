@extends('layouts.app')

@section('content')

    <style>
        header {
            height: 160px;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" >

                <a href="{{ route('profile') }}" class="btn btn-default">
                    {{ __('Profile') }}
                </a>
                <a href="{{ route('units') }}" class="btn btn-success">
                    {{ __('Units') }}
                </a>
                <a href="{{ route('user.logout') }}" class="btn btn-danger">
                    {{ __('Logout') }}
                </a>
                <br>
                <br>

                <table class="adminlist">
                    <tr>
                        <td width=25%>
                            <b>{{ __( 'Place' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->province .", " .$unit->city . ", ". $unit->region; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Project name' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->Name; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><b>{{ __( 'Plot Number' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->PlotNumber; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Area' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->Area; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Market Price' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->UnitValue ; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Have Garage' ) }}</b>
                        </td>
                        <td>
                            <?php if ($unit->HaveGarage) echo __( 'yes' ); else echo __( "No" ); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Garage value' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->GarageValue; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Floor' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->floor; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>{{ __( 'Finishing Level' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->flevel; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b></b>
                        </td>
                        <td>
                            <?php //echo $unit->reserv; ?>
                        </td>
                    </tr>
                    <?php if(isset($_GET['res'])) : ?>
                    <tr>
                        <td>
                            <b>{{ __( 'Initial Delivery Date' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->IntialDeliveryDate; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>
                            <b>{{ __( 'Final Delivery Date' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->FinalDeliveryDate; ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign=top>
                            <b>{{ __( 'Category' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->cat . "<br />" . $unit->subcat; ?>
                        </td>
                    </tr>
                    <?php if(isset($_GET['res'])) : ?>
                    <tr>
                        <td valign=top>
                            <b>{{ __( 'Construction Phase' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->const. "<br />" . $unit->constdetails; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td valign=top>
                            <b>{{ __( 'Unit borders' ) }}</b>
                        </td>
                        <td>
                            <?php echo $unit->PlotBoundaries; ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign=top>
                            <b>{{ __( 'Image' ) }}</b>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php if (file_exists("administrator/components/com_customers/uploads/uimages/".$unit->uid.".jpg")) : ?>
                            <img src="/administrator/components/com_customers/uploads/uimages/<?php echo $unit->uid; ?>.jpg" width="400" height="400"/>
                            <?php else: ?>
                            <img src="/images/stories/no_photo_result.gif">
                            <?php endif ?>

                        </td>
                    </tr>

                    <?php $user = auth()->user();
                    if($user->id && isset($_GET['res'])): ?>
                    <tr>
                        <td valign=top>
                            <b><?php echo __( 'Project design' ); ?></b>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php
                            if (count($unit->pimages) > 0) {
                                for($i=0; $i < count($unit->pimages) ; $i++)
                                {
                                    echo '<img src="/administrator/components/com_customers/uploads/pimages/'.$unit->pimages[$i]->Image.'.jpg" width="400" height="400"/><br />';
                                }
                            }
                            else
                                echo '<img src="/images/stories/no_photo_result.gif"><br />';
                            ?>
                        </td>
                    </tr>
                    <?php endif; ?>

                </table>

            </div>
        </div>
    </div>
@stop
