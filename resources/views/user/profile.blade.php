@extends('layouts.app')

@section('content')

    <style>
        header {
            height: 160px;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 toppad" >


                <a href="{{ route('profile') }}" class="btn btn-default">
                    {{ __('Profile') }}
                </a>
                <a href="{{ route('units') }}" class="btn btn-success">
                    {{ __('Units') }}
                </a>
                <a href="{{ route('user.logout') }}" class="btn btn-danger">
                    {{ __('Logout') }}
                </a>
                <br>
                <br>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Profile</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {{--<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>--}}

                            <div class=" col-md-12 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>{{ __('profile.Name') }}:</td>
                                        <td>{{ $user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('profile.Username') }}:</td>
                                        <td>{{ $user->username }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('profile.Registered Date') }}:</td>
                                        <td>{{ $user->registerDate }}</td>
                                    </tr>

                                    <tr>
                                        <td>{{ __('profile.Last Visited Date') }}:</td>
                                        <td>{{ $user->lastvisitDate }}</td>
                                    </tr>

                                    </tbody>
                                </table>

                                <a href="{{ route('edit.profile') }}" class="btn btn-primary">{{ __('user.Edit profile') }}</a>
                                {{--<a href="#" class="btn btn-primary">Team Sales Performance</a>--}}
                            </div>
                        </div>
                    </div>
                    {{--<div class="panel-footer">--}}
                    {{--<a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>--}}
                    {{--<span class="pull-right">--}}
                    {{--<a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>--}}
                    {{--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                    {{--</span>--}}
                    {{--</div>--}}

                </div>
            </div>
        </div>
    </div>
@stop
