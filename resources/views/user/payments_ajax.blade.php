<table class="table table-bordered table-responsive">
    <tr>
        <th>
            <b><?php echo __( 'Total Amount' ); ?></b>
        </th>
        <th>
            <b><?php echo __( 'Paid Amount' ); ?></b>
        </th>
        <th>
            <b><?php echo __( 'Remaining Amount' ); ?></b>
        </th>
        <th>
            <b><?php echo __( 'Type' ); ?></b>
        </th>

        <th>
            <b><?php echo __( 'Date' ); ?></b>
        </th>
    </tr>
    @foreach($payments as $payment)
        <tr>
            <td>{{ $payment->Amount }}</td>
            <td>{{ $payment->pamount }}</td>
            <td>{{ $payment->Amount - $payment->pamount }}</td>
            <td>{{ $payment->Type }}</td>
            <td>{{ $payment->idate }}</td>
        </tr>
    @endforeach
</table>
