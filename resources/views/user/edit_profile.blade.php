@extends('layouts.app')

@section('content')

    <style>
        header {
            height: 160px;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 toppad" >


                <a href="{{ route('profile') }}" class="btn btn-default">
                    Profile
                </a>
                <a href="{{ route('units') }}" class="btn btn-success">
                    Units
                </a>
                <a href="{{ route('user.logout') }}" class="btn btn-danger">
                    Logout
                </a>
                <br>
                <br>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Profile</h3>
                    </div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="post" action="{{ route('save.profile') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>{{ __('profile.Username') }}</label>
                                <input type="text" class="form-control"value="{{ $user->Username }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('profile.Name') }}</label>
                                <input type="text" class="form-control" id="name" name="name" required value="{{ $user->name }}" />
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('profile.Email') }}</label>
                                <input type="email" class="form-control" id="email" name="email" required value="{{ $user->email }}" />
                            </div>
                            <div class="form-group">
                                <label for="password">{{ __('profile.Password') }}</label>
                                <input type="password" class="form-control" id="password" name="password"  />
                            </div>
                            <div class="form-group">
                                <label for="password_confirmed">{{ __('profile.Password confirmed') }}</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  />
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="description1">Descripcion</label>--}}
                                {{--<textarea class="form-control" id="description1" rows="4" name="description" placeholder="Descripcion" required></textarea>--}}
                            {{--</div>--}}


                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
