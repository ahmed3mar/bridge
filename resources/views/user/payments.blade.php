@extends('layouts.app')

@section('content')

    <style>
        header {
            height: 160px;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" >


                <a href="{{ route('profile') }}" class="btn btn-default">
                    {{ __('Profile') }}
                </a>
                <a href="{{ route('units') }}" class="btn btn-success">
                    {{ __('Units') }}
                </a>
                <a href="{{ route('user.logout') }}" class="btn btn-danger">
                    {{ __('Logout') }}
                </a>
                <br>
                <br>

                <table class="table table-bordered table-responsive">
                    <tr>
                        <th>
                            <b><?php echo __( 'Total Amount' ); ?></b>
                        </th>
                        <th>
                            <b><?php echo __( 'Paid Amount' ); ?></b>
                        </th>
                        <th>
                            <b><?php echo __( 'Remaining Amount' ); ?></b>
                        </th>
                        <th>
                            <b><?php echo __( 'Type' ); ?></b>
                        </th>

                        <th>
                            <b><?php echo __( 'Date' ); ?></b>
                        </th>
                    </tr>
                    @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->Amount }}</td>
                            <td>{{ $payment->pamount }}</td>
                            <td>{{ $payment->Amount - $payment->pamount }}</td>
                            <td>{{ $payment->Type }}</td>
                            <td>{{ $payment->idate }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
@stop
