@extends('layouts.app')

@section('after_scripts')
    <script>
        $('.show-installments').click(function () {

            $('#installments').modal('show');
            $('.modal-body').html('<div class="text-center"><i class="fa fa-spin fa-spinner fa-3x"></i></div>')

            $.ajax({
                url: '{{ route('payments', '') }}/' + $(this).data('id') + '?ajax=1',
                success: function (response) {
                    $('.modal-body').html(response);
                }
            })
            return false;
        })
    </script>
@stop

@section('content')

    <style>
        header {
            height: 160px;
        }
    </style>

    <div class="modal fade" style="    z-index: 9999;" id="installments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ __('Installments') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center"><i class="fa fa-spin fa-spinner fa-3x"></i></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" >

                <a href="{{ route('profile') }}" class="btn btn-default">
                    {{ __('Profile') }}
                </a>
                <a href="{{ route('units') }}" class="btn btn-success">
                    {{ __('Units') }}
                </a>
                <a href="{{ route('user.logout') }}" class="btn btn-danger">
                    {{ __('Logout') }}
                </a>
                <br>
                <br>


                <table class="table table-bordered table-responsive">
                    <tr>
                        <td>Project Name</td>
                        <td>Plot Number</td>
                        <td>Construction Phase</td>
                        <td>Price</td>
                        <td>Total Paid</td>
                        <td>Installments</td>
                    </tr>
                    @foreach($units as $unit)
                        <tr>
                            <td>{{ $unit->Name }}</td>
                            <td>{{ $unit->PlotNumber }} ( <a href="{{ route('unit', $unit->ID) }}?res=0">Plot Details</a> )</td>
                            <td>{{ $unit->ConstructionPhase }}</td>
                            <td>{{ $unit->TotalValue }}</td>
                            <td>{{ $unit->TotalValue }}</td>
                            <td>{{ $unit->inst }} Installment ( <a href="{{ route('payments', $unit->CID) }}" class="show-installments" data-id="{{ $unit->CID }}">View</a> )</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
@stop
