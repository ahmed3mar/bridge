<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @if(app()->getLocale() == 'ar') dir="rtl" @endif>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('css/idangerous.swiper.css') }}">
    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @if(app()->getLocale() == 'ar')
        <link href="{{ asset('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style-rtl.css') }}" rel="stylesheet">
    @endif
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <!--     <link rel="stylesheet" href="http://basehold.it/20/128/128/128"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var config = {
            current: '{{ isset($current) ? $current : '' }}',
            url: '{{ route('home') }}',
        };
    </script>

    <style>
        /*::-webkit-scrollbar-track*/
        /*{*/
            /*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);*/
            /*background-color: #F5F5F5;*/
        /*}*/

        /*::-webkit-scrollbar*/
        /*{*/
            /*width: 6px;*/
            /*background-color: #F5F5F5;*/
        /*}*/

        /*::-webkit-scrollbar-thumb*/
        /*{*/
            /*background-color: #000000;*/
        /*}*/
    </style>

    @yield('after_styles')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ option(_trans('site_title')) }}</title>

</head>
<body class=" @if(isset($home)) home @endif() @yield('body_class') ">
{{--<div id="loader">--}}
    {{--<div id="loading-bar">--}}
        {{--<div id="inner"></div>--}}
    {{--</div>--}}
{{--</div>--}}

    @include('common.header')
    @yield('content')
    @include('common.footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>
<script src="{{ asset('js/jquery.smoothwheel.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('js/idangerous.swiper.min.js') }}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/annyang/1.1.0/annyang.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwNw7naMEq6ZK-l4fMirYM8JglTVwCs6s"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="{{ asset('js/snap.svg-min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>

@yield('after_scripts')
</body>
</html>
